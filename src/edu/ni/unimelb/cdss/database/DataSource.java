package edu.ni.unimelb.cdss.database;

import java.util.ArrayList;
import java.util.List;

import edu.ni.unimelb.cdss.diagnosis.DiagnosisHistory;
import edu.ni.unimelb.cdss.patientInfo.PatientInfo;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Made in Eclipse 
 * @Created date 8-25-2013
 * @        time 6:30:42 p.m.
 * @author Patrick Zhao
 * @Data Provider, provide basic data access functions
 */
public class DataSource {
	private SQLiteDatabase database;
	private DBHelper dbHelper;
	
	
	/**
	 * @param context: access to specific class
	 */
	public DataSource(Context context) {
//		dbHelper = new DBHelper(context);
		dbHelper = DBHelper.getInstance(context);
	}
	
	/**
	 * @throws SQLException
	 */
	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	/**
	 * void
	 */
	public void close() {
		dbHelper.close();
	}
	
	/**
	 * @return ArrayList<PatientInfo>: a list of patients with their information in database
	 */
	public ArrayList<PatientInfo> queryPatientInfo(){
		ArrayList<PatientInfo> patientsout = new ArrayList<PatientInfo>();
		
		String columns[] = {DataStorageConstant.PATIENT_ID,DataStorageConstant.PATIENT_FNAME,DataStorageConstant.PATIENT_LNAME,DataStorageConstant.PATIETN_PHONE,
				DataStorageConstant.PATIETN_DOB,DataStorageConstant.PATIETN_AGE,DataStorageConstant.PATIENT_SEX,DataStorageConstant.PATIETN_ADDRESS,
				DataStorageConstant.LAST_UPDATE,DataStorageConstant.REGISTERED};
		
		Cursor cursor = database.query(DataStorageConstant.TABLE_NAME1, columns, null, null, null, null, null);
//		cursor.moveToFirst();
		
		while(cursor.moveToNext()){

			String patientIDDisplay = cursor.getString(cursor.getColumnIndex(DataStorageConstant.PATIENT_ID));
			String patientFNameDisplay = cursor.getString(cursor.getColumnIndex(DataStorageConstant.PATIENT_FNAME));
		    String patientLNameDisplay = cursor.getString(cursor.getColumnIndex(DataStorageConstant.PATIENT_LNAME));
			String patientPhoneNumberDisplay = cursor.getString(cursor.getColumnIndex(DataStorageConstant.PATIETN_PHONE));
			String patientDOBDisplay = cursor.getString(cursor.getColumnIndex(DataStorageConstant.PATIETN_DOB));
			String patientAgeDisplay = cursor.getString(cursor.getColumnIndex(DataStorageConstant.PATIETN_AGE));
			String patientSexDisplay = cursor.getString(cursor.getColumnIndex(DataStorageConstant.PATIENT_SEX));
			String patientAddressDisplay = cursor.getString(cursor.getColumnIndex(DataStorageConstant.PATIETN_ADDRESS));
			String lastUpdateDisplay = cursor.getString(cursor.getColumnIndex(DataStorageConstant.LAST_UPDATE));
			String RegisteredDisplay = cursor.getString(cursor.getColumnIndex(DataStorageConstant.REGISTERED));
			
			PatientInfo piout = new PatientInfo(patientIDDisplay,patientFNameDisplay,patientLNameDisplay,patientPhoneNumberDisplay,
					patientDOBDisplay,patientAgeDisplay,patientSexDisplay,patientAddressDisplay,lastUpdateDisplay,RegisteredDisplay);

			
			patientsout.add(piout);
			
		}
		return patientsout;
	}
	
	
	/**
	 * @param p_id the id of the patient you want to query
	 * @return a list of Diagnosis History of one patient under the ID
	 */
	public ArrayList<DiagnosisHistory> queryDiagnosisHistory(String p_id){
		ArrayList<DiagnosisHistory> DiagnosisHistoryout = new ArrayList<DiagnosisHistory>();
		
		String columns[] = {DataStorageConstant.PATIENT_ID,DataStorageConstant.TIME_STAMP,DataStorageConstant.PATIENT_TEMPERATURE,
				DataStorageConstant.FEATURES,DataStorageConstant.OUTCOME,DataStorageConstant.RECOMMENDATION};
		
		Cursor cursor = database.query(DataStorageConstant.TABLE_NAME2, columns, DataStorageConstant.PATIENT_ID + "='" + p_id + "'", null, null, null, null);
		cursor.moveToFirst();
		
//		while(cursor.moveToNext()){
		while (!cursor.isAfterLast()){
			String patientIDDisplay = cursor.getString(cursor.getColumnIndex(DataStorageConstant.PATIENT_ID));

			String timeStampDisplay = cursor.getString(cursor.getColumnIndex(DataStorageConstant.TIME_STAMP));
			String temperatureDisplay = cursor.getString(cursor.getColumnIndex(DataStorageConstant.PATIENT_TEMPERATURE));
			String featuresDisplay = cursor.getString(cursor.getColumnIndex(DataStorageConstant.FEATURES));
			String outcomeDisplay = cursor.getString(cursor.getColumnIndex(DataStorageConstant.OUTCOME));
			String recommendationDisplay = cursor.getString(cursor.getColumnIndex(DataStorageConstant.RECOMMENDATION));
		 
			DiagnosisHistory dhout = new DiagnosisHistory(patientIDDisplay, timeStampDisplay, temperatureDisplay, 
					featuresDisplay, outcomeDisplay, recommendationDisplay);
			
			DiagnosisHistoryout.add(dhout);
			
			cursor.moveToNext();
		}
		return DiagnosisHistoryout;
	}
	
	
	/**
	 * @param pi: single patient with all the information to insert
	 * This function insert the Diagnosis History of one patient under patient ID into the diagnosis history table
	 */
	public void insertDignosisHistory(DiagnosisHistory dh){

		String patientID = dh.getP_ID();
		String timeStamp = dh.getTimeStamp();
		String temperature = dh.getPatientTempreture();
		String features = dh.getFeatures();
		String outcomes = dh.getOutcomes();
		String recommendation = dh.getRecommendation();
			
		ContentValues cv = new ContentValues();
			
		cv.put(DataStorageConstant.PATIENT_ID, patientID);
		
		cv.put(DataStorageConstant.TIME_STAMP, timeStamp);
		cv.put(DataStorageConstant.PATIENT_TEMPERATURE, temperature);
		cv.put(DataStorageConstant.FEATURES, features);
		cv.put(DataStorageConstant.OUTCOME, outcomes);
		cv.put(DataStorageConstant.RECOMMENDATION, recommendation);
			
		database.insert(DataStorageConstant.TABLE_NAME2, null, cv);
	}
	
	/**
	 * @param pi: single patient with all the information to insert
	 */
	public void insertPatientInfo(PatientInfo pi){
		String patientID = pi.getP_ID();
		String patientFName = pi.getF_name();
		String patientLName = pi.getL_name();
		String patientPhoneNumber = pi.getPhone();
		String patientDOB = pi.getDate_of_birth();
		String patientAge = pi.getAge();
		String patientSex = pi.getSex();
		String patientAddress = pi.getAddress();
		String lastUpdate = pi.getLast_update();
		String registered = pi.isRegistered();
			
		ContentValues cv = new ContentValues();
			
		cv.put(DataStorageConstant.PATIENT_ID, patientID);
		cv.put(DataStorageConstant.PATIENT_FNAME, patientFName);
		cv.put(DataStorageConstant.PATIENT_LNAME, patientLName);
		cv.put(DataStorageConstant.PATIETN_PHONE, patientPhoneNumber);
		cv.put(DataStorageConstant.PATIETN_DOB, patientDOB);
		cv.put(DataStorageConstant.PATIETN_AGE, patientAge);
		cv.put(DataStorageConstant.PATIENT_SEX, patientSex);
		cv.put(DataStorageConstant.PATIETN_ADDRESS, patientAddress);
		cv.put(DataStorageConstant.LAST_UPDATE, lastUpdate);
		cv.put(DataStorageConstant.REGISTERED, registered);
			
		database.insert(DataStorageConstant.TABLE_NAME1, null, cv);
	}

	
	/**
	 * @param pi
	 * This function update the patient information table under the patient ID
	 */
	public void updatePatientInfo(PatientInfo pi) {
		// TODO Auto-generated method stub
		
		String patientID = pi.getP_ID();
		String patientFName = pi.getF_name();
		String patientLName = pi.getL_name();
		String patientPhoneNumber = pi.getPhone();
		String patientDOB = pi.getDate_of_birth();
		String patientAge = pi.getAge();
		String patientSex = pi.getSex();
		String patientAddress = pi.getAddress();
		String lastUpdate = pi.getLast_update();
		String registered = pi.isRegistered();
			
		ContentValues cv = new ContentValues();
			
		cv.put(DataStorageConstant.PATIENT_ID, patientID);
		cv.put(DataStorageConstant.PATIENT_FNAME, patientFName);
		cv.put(DataStorageConstant.PATIENT_LNAME, patientLName);
		cv.put(DataStorageConstant.PATIETN_PHONE, patientPhoneNumber);
		cv.put(DataStorageConstant.PATIETN_DOB, patientDOB);
		cv.put(DataStorageConstant.PATIETN_AGE, patientAge);
		cv.put(DataStorageConstant.PATIENT_SEX, patientSex);
		cv.put(DataStorageConstant.PATIETN_ADDRESS, patientAddress);
		cv.put(DataStorageConstant.LAST_UPDATE, lastUpdate);
		cv.put(DataStorageConstant.REGISTERED, registered);
		
		database.update(DataStorageConstant.TABLE_NAME1, cv,DataStorageConstant.PATIENT_ID + "='" + patientID + "'",null);
		
	}

	
	/**
	 * @param p_id
	 * @return
	 */
	public boolean existPatientInfo(String p_id){
		Cursor cursor = null;
		
		
//		if( cursor == null){
//			Log.d("DATA", "Cursor null");
//		}
		Log.d("DATA", "Checking patient: " + p_id);
		
		cursor = database.rawQuery("SELECT * FROM " + DataStorageConstant.TABLE_NAME1 + " WHERE " + DataStorageConstant.PATIENT_ID + "= '" + p_id + "'", null); 
		
		if(cursor.getCount() < 1){
			Log.d("DATA", "Not exist Patient");
			return false;
		} else {
			Log.d("DATA", "Exist Patient");
			return true;
		}
	
	}
	

	/**
	 * @param p_id
	 * @param timeStamp
	 * @return
	 */
	public boolean existDiagnosisHistory(String p_id, String timeStamp){
		Cursor cursor = database.rawQuery("SELECT * FROM " + DataStorageConstant.TABLE_NAME2 + " WHERE " + DataStorageConstant.PATIENT_ID + "= '" + p_id + "' and "+ DataStorageConstant.TIME_STAMP +" ='" + timeStamp+ "'", null); 
		if( cursor == null){
			return false;
		} else {
			return true;
		}
	}
} 