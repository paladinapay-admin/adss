/**
 * 
 */
package edu.ni.unimelb.cdss.database;

import android.provider.BaseColumns;

/**
 * Made in Eclipse
 * 
 * @Created date 16-8-2013 
 * @ time 3:33:36 p.m 
 * @ author Patrick Zhao
 * @version 1.0
 * @Data Base Entity Constants
 * 
 */
public class DataStorageConstant implements BaseColumns {
	public static final String DATABASE_NAME = "CDSS_data";

	public static final String TABLE_NAME1 = "CDSS_patient_information_table";
	public static final String TABLE_NAME2 = "CDSS_patient_diagnose_table";

	public static final String PATIENT_ID = "P_ID";
	public static final String PATIENT_FNAME = "F_name";
	public static final String PATIENT_LNAME = "L_name";
	public static final String PATIETN_PHONE = "phone";
	public static final String PATIETN_DOB = "data_of_birth";
	public static final String PATIETN_AGE = "age";
	public static final String PATIENT_SEX = "sex";
	public static final String PATIETN_ADDRESS = "address";
	public static final String LAST_UPDATE = "last_update";
	public static final String REGISTERED = "registered";

	public static final String Diagnosis_ID = "diagnosisID";
	public static final String TIME_STAMP = "time_stamp";
	public static final String PATIENT_TEMPERATURE = "temperature";
	public static final String FEATURES = "features";
	public static final String OUTCOME = "outcome";
	public static final String RECOMMENDATION = "recommendation";
}
