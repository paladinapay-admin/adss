package edu.ni.unimelb.cdss.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Made in Eclipse
 * 
 * @Created date 16-8-2013 
 * @ time 3:41:02 p.m 
 * @ author Patrick Zhao
 * @version 1.0
 * @Provide database create and upgrade function
 * 
 */
public class DBHelper extends SQLiteOpenHelper {
	private static DBHelper sInstance = null;
	
	public static DBHelper getInstance(Context context){
		if (sInstance == null){
			sInstance = new DBHelper(context.getApplicationContext());
		}
		return sInstance;
	}
	
	
	public static final int VERSION = 1;
	// As SQLite only supports NULL, INTEGER, REAL, TEXT, BLOB and no Boolean,
	// all data is stored as text.

	private final String createDB_PatientTable = "create table if not exists "
			+ DataStorageConstant.TABLE_NAME1 + " ( "
			+ DataStorageConstant.PATIENT_ID + " text, "
			+ DataStorageConstant.PATIENT_FNAME + " text, "
			+ DataStorageConstant.PATIENT_LNAME + " text, "
			+ DataStorageConstant.PATIETN_PHONE + " text, "
			+ DataStorageConstant.PATIETN_DOB + " text, "
			+ DataStorageConstant.PATIETN_AGE + " text, "
			+ DataStorageConstant.PATIENT_SEX + " text, "
			+ DataStorageConstant.PATIETN_ADDRESS + " text, "
			+ DataStorageConstant.LAST_UPDATE + " text, "
			+ DataStorageConstant.REGISTERED + " text); ";

	private final String createDB_DiagnoseTable = "create table if not exists "
			+ DataStorageConstant.TABLE_NAME2 + " ( "
			+ DataStorageConstant.PATIENT_ID + " text, "
			+ DataStorageConstant.Diagnosis_ID + " text, "
			+ DataStorageConstant.TIME_STAMP + " text, "
			+ DataStorageConstant.PATIENT_TEMPERATURE + " text, "
			+ DataStorageConstant.FEATURES + " text, "
			+ DataStorageConstant.OUTCOME + " text, "
			+ DataStorageConstant.RECOMMENDATION + " text); ";

	/**
	 * @param context
	 *            : access to specific classes
	 */
//	public DBHelper(Context context) {
//		super(context, DataStorageConstant.DATABASE_NAME, null, VERSION);
//
//	}

	private DBHelper(Context context) {
		super(context, DataStorageConstant.DATABASE_NAME, null, VERSION);

	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.database.sqlite.SQLiteOpenHelper#onCreate(android.database.sqlite
	 * .SQLiteDatabase)
	 */
	// This function is used to create the DB
	public void onCreate(SQLiteDatabase cdssDB) {
		cdssDB.execSQL(createDB_PatientTable);
		cdssDB.execSQL(createDB_DiagnoseTable);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.database.sqlite.SQLiteOpenHelper#onUpgrade(android.database.sqlite
	 * .SQLiteDatabase, int, int)
	 */
	public void onUpgrade(SQLiteDatabase cdssDB, int oldversion, int newversion) {
		cdssDB.execSQL("drop table " + DataStorageConstant.TABLE_NAME1);
		cdssDB.execSQL("drop table " + DataStorageConstant.TABLE_NAME2);
	}

}
