/*
 * Copyright 2008 The Android Open Source Project
 * Copyright 2011-2012 Michael Novak <michael.novakjr@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ni.unimelb.cdss.numberPicker;

import java.text.NumberFormat;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import edu.ni.unimelb.cdss.R;

public class NumberPicker extends LinearLayout {

	private final long REPEAT_DELAY = 50;

	private final int ELEMENT_HEIGHT = 35;
	private final int ELEMENT_WIDTH = 35; // you're all squares, yo

	private double MINIMUM = 30.0;
	private double MAXIMUM = 45.0;

	private final int TEXT_SIZE = 20;

	public double value;

	Button decrement;
	Button increment;
	public EditText valueText;

	private Handler repeatUpdateHandler = new Handler();

	private boolean autoIncrement = false;
	private boolean autoDecrement = false;

	class RepetetiveUpdater implements Runnable {
		public void run() {
			if (autoIncrement) {
				increment();
				repeatUpdateHandler.postDelayed(new RepetetiveUpdater(), REPEAT_DELAY);
			} else if (autoDecrement) {
				decrement();
				repeatUpdateHandler.postDelayed(new RepetetiveUpdater(), REPEAT_DELAY);
			}
		}
	}

	public NumberPicker(Context context, AttributeSet attributeSet) {
		super(context, attributeSet);

		this.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		LayoutParams elementParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		LayoutParams valuetext = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		// init the individual elements
		
		initDecrementButton(context);
		initValueEditText(context);
		initIncrementButton(context);

		/**
		 * @author ChiWang
		 */
		valueText.setFocusableInTouchMode(false);
		valueText.setBackgroundColor(Color.LTGRAY);
//		valueText.setHint("Please use the + and - button to adjust temperature");
		
		
		// Can be configured to be vertical or horizontal
		// Thanks for the help, LinearLayout!
		if (getOrientation() == VERTICAL) {
			addView(increment, elementParams);
			elementParams.setMargins(10, 0, 10, 0);
			addView(valueText, valuetext);
			addView(decrement, elementParams);
		} else {
			addView(decrement, elementParams);
			elementParams.setMargins(10, 0, 10, 0);
			addView(valueText, valuetext);
			addView(increment, elementParams);
		}
	}

	private void initIncrementButton(Context context) {
		increment = new Button(context);
		increment.setBackgroundResource(R.drawable.button);
		increment.setTextSize(TEXT_SIZE);
		increment.setText(" + ");
		increment.setTextColor(Color.WHITE);

		// Increment once for a click
		increment.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				increment();
			}
		});

		// Auto increment for a long click
		increment.setOnLongClickListener(new View.OnLongClickListener() {
			public boolean onLongClick(View arg0) {
				autoIncrement = true;
				repeatUpdateHandler.post(new RepetetiveUpdater());
				return false;
			}
		});

		// When the button is released, if we're auto incrementing, stop
		increment.setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_UP && autoIncrement) {
					autoIncrement = false;
				}
				return false;
			}
		});
	}

	private void initValueEditText(Context context) {

		value = 0.0;

		valueText = new EditText(context);
		valueText.setTextSize(TEXT_SIZE);

		// Since we're a number that gets affected by the button, we need to be
		// ready to change the numeric value with a simple ++/--, so whenever
		// the value is changed with a keyboard, convert that text value to a
		// number. We can set the text area to only allow numeric input, but
		// even so, a carriage return can get hacked through. To prevent this
		// little quirk from causing a crash, store the value of the internal
		// number before attempting to parse the changed value in the text area
		// so we can revert to that in case the text change causes an invalid
		// number
		valueText.setOnKeyListener(new View.OnKeyListener() {
			public boolean onKey(View v, int arg1, KeyEvent event) {
				double backupValue = value;
				try {
					value = Double.parseDouble(((EditText) v).getText().toString()); // Integer.parseInt(
																						// ((EditText)v).getText().toString()
																						// );
				} catch (NumberFormatException nfe) {
					value = backupValue;
				}
				return false;
			}
		});

		// Highlight the number when we get focus
		valueText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					((EditText) v).selectAll();
				}
			}
		});
		valueText.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
		valueText.setText("");
		valueText.setEms(4);
		valueText.setInputType(InputType.TYPE_CLASS_NUMBER);

	}

	private void initDecrementButton(Context context) {
		decrement = new Button(context);
		decrement.setBackgroundResource(R.drawable.button);
		decrement.setTextColor(Color.WHITE);
		decrement.setTextSize(TEXT_SIZE);
		decrement.setText(" - ");

		// Decrement once for a click
		decrement.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				decrement();
			}
		});

		// Auto Decrement for a long click
		decrement.setOnLongClickListener(new View.OnLongClickListener() {
			public boolean onLongClick(View arg0) {
				autoDecrement = true;
				repeatUpdateHandler.post(new RepetetiveUpdater());
				return false;
			}
		});

		// When the button is released, if we're auto decrementing, stop
		decrement.setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_UP && autoDecrement) {
					autoDecrement = false;
				}
				return false;
			}
		});
	}

	public void increment() {
		if (value == 0.0)
			value = 37;
		else {
			if (value < MAXIMUM) {
				value = value + 0.1;
				NumberFormat formatter = NumberFormat.getNumberInstance();
				formatter.setMinimumFractionDigits(2);
				formatter.setMaximumFractionDigits(2);
				String output = formatter.format(value);
				valueText.setText(output);
			}
		}
	}

	public void decrement() {
		if (value == 0.0)
			value = 37;
		else {
			if (value > MINIMUM) {
				value = value - 0.1;
				NumberFormat formatter = NumberFormat.getNumberInstance();
				formatter.setMinimumFractionDigits(2);
				formatter.setMaximumFractionDigits(2);
				String output = formatter.format(value);
				valueText.setText(output);

			}
		}
	}

	public String getValue() {

		String v = "";
		if (value == 0.0)
			v = "";
		else
			v = String.valueOf(value);
		return v;
	}

	public void setValue(double value) {

		if (value > MAXIMUM)
			value = MAXIMUM;
		if (value >= MINIMUM) {
			this.value = value;
			valueText.setText(String.valueOf(value));
		}
	}

	public EditText getValueText() {
		return valueText;
	}

}
