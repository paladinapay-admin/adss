package edu.ni.unimelb.cdss.synchronization;

import java.io.Serializable;
import java.util.ArrayList;

import android.util.Log;
import edu.ni.unimelb.cdss.database.DataSource;
import edu.ni.unimelb.cdss.diagnosis.DiagnosisHistory;
import edu.ni.unimelb.cdss.patientInfo.PatientInfo;

/**
 * This class wraps patient information and diagnosis history as ONE object, in
 * order to encode/decode them
 * 
 * @author Hanlin Chen
 * @version 0.3
 */
public class DataWrapper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private transient DataSource db;
	private ArrayList<PatientInfo> patientInfos;
	private ArrayList<ArrayList<DiagnosisHistory>> allHistories;
	private boolean lastPiece;

	public DataWrapper(DataSource db, Boolean last) {
		this.db = db;
		// this.db.close();
		this.db.open();
		this.lastPiece = last;
		this.patientInfos = db.queryPatientInfo();
		this.allHistories = new ArrayList<ArrayList<DiagnosisHistory>>();
		for (PatientInfo p : this.patientInfos) {
			ArrayList<DiagnosisHistory> history = new ArrayList<DiagnosisHistory>();
			try {
				history = db.queryDiagnosisHistory(p.getP_ID());
			} catch (Exception e) {
				Log.d("DW", "No such table");
				e.printStackTrace();
			}
			allHistories.add(history);
		}
	}

	/**
	 * Insert all non existing data into local database
	 * 
	 * @param newd
	 *            The incoming DataWrapper class to decode and insert
	 * @author Hanlin Chen
	 */
	public boolean insertAllData(Object newd) {
		this.db.close();
		this.db.open();
		DataWrapper newdw = (DataWrapper) newd;
		ArrayList<PatientInfo> inputAllPatients = newdw.getAllPatients();
		ArrayList<ArrayList<DiagnosisHistory>> inputAllHistories = newdw.getAllHistories();

		if (inputAllPatients == null || inputAllHistories == null) {
			return newdw.lastPiece;
		}

		for (PatientInfo p : inputAllPatients) {
			// check whether have this patient
			if (this.db.existPatientInfo(p.getP_ID()))
				continue;

			Log.d("DW", "Insert patient: " + p.getP_ID());
			this.db.insertPatientInfo(p);

		}

		for (ArrayList<DiagnosisHistory> histories : inputAllHistories) {
			for (DiagnosisHistory dh : histories) {
				for (PatientInfo p : inputAllPatients) {
					if (!this.db.existDiagnosisHistory(p.getP_ID(), dh.getTimeStamp())) {
						this.db.insertDignosisHistory(dh);
					}
				}
			}
		}
		this.db.close();
		return newdw.lastPiece;
	}

	public ArrayList<PatientInfo> getAllPatients() {
		return this.patientInfos;
	}

	public ArrayList<ArrayList<DiagnosisHistory>> getAllHistories() {
		return this.allHistories;
	}

	/**
	 * Check whether local database is empty
	 * 
	 * @author Hanlin Chen
	 */
	public boolean containNothing() {
		this.db.close();
		this.db.open();
		return this.patientInfos.isEmpty();
	}

	// public void refreshDb(){
	// this.db.close();
	// this.db.open();
	// this.patientInfos = db.queryPatientInfo();
	// this.allHistories = new ArrayList<ArrayList<DiagnosisHistory>>();
	// for (PatientInfo p : this.patientInfos){
	// ArrayList<DiagnosisHistory> history = new ArrayList<DiagnosisHistory>();
	// try {
	// history = db.queryDiagnosisHistory(p.getP_ID());
	// } catch (Exception e) {
	// Log.d("DW", "No such table");
	// e.printStackTrace();
	// }
	// allHistories.add(history);
	// }
	// }
}
