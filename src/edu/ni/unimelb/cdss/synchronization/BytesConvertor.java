/**
 * This class provides a converter to convert a serializeble object to bytes and
 * from bytes back to object.
 * @author Hanlin Chen
 * @version 0.2
 */
package edu.ni.unimelb.cdss.synchronization;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;

public class BytesConvertor {

	/**
	 * Convert a serializeble object to bytes
	 * 
	 * @param object
	 *            Object to serialize
	 * @author Hanlin Chen
	 */
	public static byte[] objectToBytes(Object object) throws IOException {
		ByteArrayOutputStream b = new ByteArrayOutputStream();
		ObjectOutputStream o = new ObjectOutputStream(b);
		o.writeObject(object);
		return b.toByteArray();
	}

	/**
	 * Convert bytes back to its object
	 * 
	 * @param bytes
	 *            Bytes to deserialize
	 * @author Hanlin Chen
	 */
	public static Object deserialize(byte[] bytes)
			throws OptionalDataException, ClassNotFoundException, IOException {
		ByteArrayInputStream b = new ByteArrayInputStream(bytes);
		ObjectInputStream o = new ObjectInputStream(b);
		return o.readObject();
	}
}