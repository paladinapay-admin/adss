/**
 * This class is used for data synchronization.
 * @author Hanlin Chen, Lacy Zhang
 **/

package edu.ni.unimelb.cdss.synchronization;

import java.io.IOException;
import java.util.Set;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;
import edu.ni.unimelb.cdss.database.DataSource;

/**
 * This class does all the work for setting up and managing Bluetooth
 * connections with other devices. It has a thread for connecting with paired
 * devices,
 * 
 * @author Hanlin Chen, Lacy Zhang
 * @version 0.3
 */
public class SynchronizationService extends Service {
	private static final String TAG = "SYNC";

	// Wether we want to activate data sync function
	private static final boolean pSync = true;

	private final IBinder mBinder = new MyBinder();

	// Local Bluetooth adapter
	private BluetoothAdapter mBluetoothAdapter = null;
	// Member object for the chat services
	private AutoConnection connection = null;
	private DataSource datasource;
	private DataWrapper dw;

	@Override
	public void onCreate() {
		super.onCreate();

		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		// ensure bluetooth available and ON
		if (bluetoothCheck()) {
			if (!pSync)
				return;
			datasource = new DataSource(getBaseContext());
			datasource.open();
			dw = new DataWrapper(datasource, true);
			setupConnection();
		}

	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		datasource = new DataSource(getBaseContext());

		if (mBluetoothAdapter == null) {
			mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		}

		if (bluetoothCheck()) {
			if (!pSync)
				return Service.START_NOT_STICKY;
			datasource = new DataSource(getBaseContext());
			datasource.open();
			dw = new DataWrapper(datasource, true);
			// setupConnection();
			try {
				sendPatients();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return Service.START_NOT_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

	public class MyBinder extends Binder {
		public SynchronizationService getService() {
			return SynchronizationService.this;
		}
	}

	/**
	 * Send local database to all paired devices
	 * 
	 * @author Hanlin Chen
	 */
	public void sendPatients() throws IOException {

		byte[] out = new byte[16384];
		if (connection.getState() == AutoConnection.STATE_CONNECTED) {
			Log.d(TAG, "Connected");
			if (!(new DataWrapper(new DataSource(getBaseContext()), true)).containNothing()) {
				Log.d(TAG, "Converting to bytes");
				DataSource tmpDB = new DataSource(getBaseContext());
				DataWrapper tmpDW = new DataWrapper(tmpDB, true);
				out = BytesConvertor.objectToBytes(tmpDW);

				Log.d(TAG, "Writting to other device");

				connection.write(out);
				return;
			}
		}
		// Get a set of currently paired devices
		Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
		// If there are paired devices, add each one to the ArrayAdapter
		if (pairedDevices.size() > 0) {
			// Put while loop here maybe
			for (BluetoothDevice device : pairedDevices) {
				Toast.makeText(this, "Connecting to " + device.getName(), Toast.LENGTH_SHORT).show();

				connection.connect(device);
				// Wait while connecting
				while (connection.getState() == AutoConnection.STATE_CONNECTING)
					;
				// If successfully connected send db; otherwise loop to next
				// device
				if (connection.getState() == AutoConnection.STATE_CONNECTED) {
					Log.d(TAG, "Connected");
					if (!(new DataWrapper(new DataSource(getBaseContext()), true)).containNothing()) {
						Log.d(TAG, "Converting to bytes");
						DataSource tmpDB = new DataSource(getBaseContext());
						DataWrapper tmpDW = new DataWrapper(tmpDB, true);
						out = BytesConvertor.objectToBytes(tmpDW);

						Log.d(TAG, "Writting to other device:");
						Log.d(TAG, out.toString());

						connection.write(out);
					}
				}
				// setupConnection();
			}
		}
	}

	/**
	 * Stop current running connection and establish new connection
	 * 
	 * @author Hanlin Chen, Lacy Zhang
	 */
	private void setupConnection() {
		// Stop the Bluetooth services
		if (connection != null)
			connection.stop();
		// Initialize the BluetoothChatService to perform bluetooth connections
		connection = new AutoConnection(this, dw);
		// Toast.makeText(this, "Bluetooth connection starts"
		// , Toast.LENGTH_SHORT).show();
		connection.start();
	}

	/**
	 * Ensure the device has bluetooth and it's been turned on, Otherwise stop
	 * the service
	 * 
	 * @author Hanlin Chen, Lacy Zhang
	 */
	public boolean bluetoothCheck() {
		// If the adapter is null, then Bluetooth is not supported
		if (mBluetoothAdapter == null) {
			Toast.makeText(this, "Bluetooth is not available, unable to sync", Toast.LENGTH_SHORT).show();
			return false;
		}
		if (!mBluetoothAdapter.isEnabled()) {
			
			Toast.makeText(this, "IMCI+: Bluetooth is not on, please turn it on.", Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;
	}

}
