package edu.ni.unimelb.cdss.qrcode;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;

/**
 * The QR Code generator and decoder. The generator is not used by system yet.
 * Just leave it here for testing purpose and further usage.
 * 
 * @author ChiWang
 * @version 0.3
 */
public class QRCode {
	private static final int BLACK = 0xff000000;
	private static final int WHITE = 0xFFFFFFFF;
	private final int inforLength = 8;

	private static String result;

	/**
	 * @param contents
	 * @return Bitmap QR Code generator.
	 */
	public Bitmap encode(String contents) {
		try {
			BitMatrix bitMatrix = new MultiFormatWriter().encode(contents, BarcodeFormat.QR_CODE, 200, 200);

			return toBufferedImage(bitMatrix);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;

	}

	/**
	 * @param matrix
	 * @return Bitmap Used by QR Code generator
	 */
	public Bitmap toBufferedImage(BitMatrix matrix) {
		int width = matrix.getWidth();
		int height = matrix.getHeight();
		Bitmap image = Bitmap.createBitmap(width, height, Config.ARGB_4444);
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				image.setPixel(x, y, matrix.get(x, y) == true ? BLACK : WHITE);
			}
		}
		return image;
	}

	/**
	 * Dessemble a decoded string to useful information
	 * 
	 * @param in
	 * @return Array of patient's information
	 */
	public String[] Dessemble(String in) {

		String[] result = new String[inforLength];
		if (in.contains("CDSSQRCODE")) {
			in = in.substring("CDSSQRCODE".length());

			for (int i = 0; i < inforLength; i++) {
				String[] temp = in.split(" ");
				try {
					Integer.parseInt(temp[0]);
				} catch (NumberFormatException e) {
					for (int j = 0; j < inforLength; j++) {
						result[j] = "CDSSQRCODEINFORMATIONERROR";
					}
					return result;
				}
				int count = Integer.parseInt(temp[0]);
				String temp1 = in.substring(temp[0].length() + 1);
				result[i] = temp1.substring(0, count);
				in = temp1.substring(count);
				System.out.println(result[i]);

			}
		} else {

			for (int i = 0; i < inforLength; i++) {
				result[i] = "CDSSQRCODEINFORMATIONERROR";
			}

		}
		return result;

	}

	/**
	 * Format an array of string to a string with particular format
	 * 
	 * @param in
	 * @return
	 */
	public String Assemble(String[] in) {

		result = "CDSSQRCODE";
		for (int i = 0; i < in.length; i++) {

			result = result + String.valueOf(in[i].length()) + " " + in[i];

		}
		return result;

	}

}