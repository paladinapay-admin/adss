package edu.ni.unimelb.cdss.qrcode;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import edu.ni.unimelb.cdss.R;
import edu.ni.unimelb.cdss.control.HomeActivity;

/**
 * 
 * @author ChiWang QR Code generator will use it to preview the generated QR
 *         Code.
 * @version 0.3
 */
public class QRCodePreviewActivity extends Activity {

	private Button back;

	private String mIDEditText;
	private String mFNameEditText;
	private String mLNameEditText;
	private String mPhoneEditText;
	private String mSelectDate;
	private String mAge;
	private String mAddress;
	private String mSex;

	private QRCode qRCode;

	private Bitmap qRCodeImage;

	private ImageView qRCodeDisplay;

	private Bundle bundle;
	private Intent intent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.qrcode_preview);

		back = (Button) findViewById(R.id.back);
		back.setOnClickListener(new BackOnClickListener());

		qRCodeDisplay = (ImageView) findViewById(R.id.camera_image_view);

		intent = this.getIntent();
		bundle = intent.getExtras();

		qRCode = new QRCode();

		mIDEditText = bundle.getString("mIDEditText");
		mFNameEditText = bundle.getString("mFNameEditText");
		mLNameEditText = bundle.getString("mLNameEditText");
		mPhoneEditText = bundle.getString("mPhoneEditText");
		mSelectDate = bundle.getString("mSelectDate");
		mAge = bundle.getString("mAge");
		mAddress = bundle.getString("mAddress");
		mSex = bundle.getString("mSex");

		String[] infor = { mIDEditText, mFNameEditText, mLNameEditText, mPhoneEditText, mSelectDate, mAge, mAddress,
				mSex };

		Toast.makeText(QRCodePreviewActivity.this, infor[0], Toast.LENGTH_SHORT).show();
		qRCodeImage = qRCode.encode(qRCode.Assemble(infor));

		qRCodeDisplay.setImageBitmap(qRCodeImage);

	}

	class BackOnClickListener implements View.OnClickListener {

		@Override
		public void onClick(View v) {

			Toast.makeText(QRCodePreviewActivity.this, R.string.save_toast, Toast.LENGTH_SHORT).show();

			QRCodePreviewActivity.this.finish();

		}
	}

	private int group1Id = 1;

	int exit = 1;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		menu.add(group1Id, exit, exit, "").setTitle("Exit");

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case 1:
			Intent exit = new Intent(this, HomeActivity.class);
			exit.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			exit.putExtra("EXIT", true);
			startActivity(exit);
			return true;

		default:
			break;

		}
		return super.onOptionsItemSelected(item);
	}

}
