package edu.ni.unimelb.cdss.qrcode;

import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import edu.ni.unimelb.cdss.R;
import edu.ni.unimelb.cdss.control.ApplicationController;
import edu.ni.unimelb.cdss.control.HomeActivity;
import edu.ni.unimelb.cdss.patientInfo.PatientInfo;
import edu.ni.unimelb.cdss.patientInfo.PatientInfoFormActivity;
import edu.ni.unimelb.cdss.patientInfo.PatientInfoPreviewActivity;
import edu.ni.unimelb.cdss.patientInfo.PatientSearchActivity;
import edu.ni.unimelb.cdss.patientInfo.PatientSearchResultActivity;

/* Import ZBar Class files */

/**
 * @author ChiWang
 * 
 *         Camera Activity. Open the camera and try to pick up the QRCode and
 *         decode it.
 * 
 * @version 0.3
 */
public class CameraActivity extends Activity {
	private Camera mCamera;
	private CameraPreview mPreview;
	private Handler autoFocusHandler;

	TextView scanText;
	// Button scanButton;
	Button mNextbtn;
	ApplicationController app;
	PatientInfo p;
	ImageScanner scanner;

	private QRCode qRCode;

	private boolean barcodeScanned = false;
	private boolean previewing = true;

	private String scanResult;

	private boolean barcoeValid = false;

	static {
		System.loadLibrary("iconv");
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.camera_activity);

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		app = ((ApplicationController) getApplicationContext());

		p = app.getPatient();
		autoFocusHandler = new Handler();
		mCamera = getCameraInstance();

		/* Instance barcode scanner */
		scanner = new ImageScanner();
		scanner.setConfig(0, Config.X_DENSITY, 3);
		scanner.setConfig(0, Config.Y_DENSITY, 3);

		qRCode = new QRCode();

		mPreview = new CameraPreview(this, mCamera, previewCb, autoFocusCB);
		FrameLayout preview = (FrameLayout) findViewById(R.id.cameraPreview);
		preview.addView(mPreview);

		scanText = (TextView) findViewById(R.id.scanText);

		// scanButton = (Button) findViewById(R.id.ScanButton);
		mNextbtn = (Button) findViewById(R.id.NextButtonCammera);
		mNextbtn.setOnClickListener(new OnClickListener() {
			/**
			 * When the start button is clicked, move to the patient screen.
			 */
			public void onClick(View v) {

				// initialize patient every time for the new patient

				Intent to_patientSer = new Intent(CameraActivity.this, PatientSearchActivity.class);
				startActivity(to_patientSer);
			}
		});

	}

	@Override
	protected void onPause() {
		super.onPause();

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onStop() {
		super.onStop();
		releaseCamera();
		Log.d("QR Camera", "on stop");
	}

	@Override
	protected void onResume() {

		super.onResume();

		barcoeValid = false;
		barcodeScanned = false;
		previewing = true;
		try {
			if (mCamera == null) {

				// setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
				// autoFocusHandler = new Handler();
				mCamera = getCameraInstance();
				mCamera.setPreviewDisplay(mPreview.getHolder());
				this.getWindowManager().getDefaultDisplay().getRotation();

				scanner = new ImageScanner();
				scanner.setConfig(0, Config.X_DENSITY, 3);
				scanner.setConfig(0, Config.Y_DENSITY, 3);

				mPreview = new CameraPreview(this, mCamera, previewCb, autoFocusCB);
				FrameLayout preview = (FrameLayout) findViewById(R.id.cameraPreview);
				preview.addView(mPreview);
				Log.d("QR Camera", "resume");
			}
		} catch (Exception e) {
			Log.d("QR Camera", "resume failed");

		}

	}

	/**
	 * A safe way to get an instance of the Camera object.
	 */
	public static Camera getCameraInstance() {
		Camera c = null;
		try {
			c = Camera.open();
		} catch (Exception e) {
		}
		return c;
	}

	/**
	 * Release Camera
	 */
	private void releaseCamera() {
		if (mCamera != null) {
			previewing = false;
			mCamera.stopPreview();
			mCamera.setPreviewCallback(null);
			mCamera.release();
			mPreview.getHolder().removeCallback(mPreview);
			mCamera = null;

		}
	}

	private Runnable doAutoFocus = new Runnable() {
		public void run() {
			if (previewing)
				mCamera.autoFocus(autoFocusCB);
		}
	};

	PreviewCallback previewCb = new PreviewCallback() {
		public void onPreviewFrame(byte[] data, Camera camera) {

			Camera.Parameters parameters = camera.getParameters();
			Size size = parameters.getPreviewSize();

			Image barcode = new Image(size.width, size.height, "Y800");
			barcode.setData(data);

			int result = scanner.scanImage(barcode);

			if (result != 0) {

				Log.d("QR Camera", String.valueOf(result));

				SymbolSet syms = scanner.getResults();
				for (Symbol sym : syms) {
					String[] display = qRCode.Dessemble(sym.getData());

					if (!display[0].equals("CDSSQRCODEINFORMATIONERROR")) {
						scanText.setText("ID: " + display[1] + display[3] + "\nFirst Name: " + display[1]
								+ "\nLast Name: " + display[2] + "\nPhone Number: " + display[3] + "\nBirthday: "
								+ display[4] + "\nAge: " + display[5] + "\nAddress: " + display[6] + "\nSex: "
								+ display[7]);

						barcoeValid = true;
						barcodeScanned = true;
						previewing = false;
						mCamera.setPreviewCallback(null);
						mCamera.stopPreview();
						releaseCamera();

						final ToneGenerator tg = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
						tg.startTone(ToneGenerator.TONE_PROP_BEEP);

						Vibrator vi = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

						// Vibrate for 500 milliseconds
						vi.vibrate(500);
					} else {
						scanText.setText("Invalid\n\n\n\n\n\n\n\n");
					}

					scanResult = sym.getData();

				}

				/*
				 * send it to the next screen
				 */
				Intent i = new Intent(CameraActivity.this, PatientInfoFormActivity.class);

				Bundle bl = new Bundle();
				if (barcodeScanned && barcoeValid) {

					String[] read = qRCode.Dessemble(scanResult);

					p.setP_ID(read[0]);
					if (p.retrievePatients(getApplicationContext(), p).size() != 0) {
						if (p.retrievePatients(getApplicationContext(), p).size() == 1) {

							/*
							 * Save this patient in ApplicationController
							 */
							app.setPatient(p.retrievePatients(getApplicationContext(), p).get(0));
							Intent toOne = new Intent(CameraActivity.this, PatientInfoPreviewActivity.class);
							startActivity(toOne);
						} else {
							Intent toresult = new Intent(CameraActivity.this, PatientSearchResultActivity.class);
							startActivity(toresult);
						}

					} else {
						bl.putString("mIDEditText", read[0]);
						bl.putString("mFNameEditText", read[1]);
						bl.putString("mLNameEditText", read[2]);
						bl.putString("mPhoneEditText", read[3]);
						bl.putString("mSelectDate", read[4]);
						bl.putString("mAge", read[5]);
						bl.putString("mAddress", read[6]);
						bl.putString("mSex", read[7]);

						i.putExtras(bl);
						startActivityForResult(i, 0);
					}

				} else {

				}

			}
		}
	};

	// Mimic continuous auto-focusing
	AutoFocusCallback autoFocusCB = new AutoFocusCallback() {
		public void onAutoFocus(boolean success, Camera camera) {
			autoFocusHandler.postDelayed(doAutoFocus, 100);

			Log.d("QR Camera", "focusing");
		}
	};

	private int group1Id = 1;

	int exit = 1;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		menu.add(group1Id, exit, exit, "").setTitle("Exit");

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case 1:
			Intent exit = new Intent(this, HomeActivity.class);
			exit.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			exit.putExtra("EXIT", true);
			startActivity(exit);
			return true;

		default:
			break;

		}
		return super.onOptionsItemSelected(item);
	}
}
