package edu.ni.unimelb.cdss.diagnosis.stc;

import jason.asSyntax.Structure;
import edu.ni.unimelb.cdss.diagnosis.env.CDSSEnvironment;
import edu.ni.unimelb.cdss.diagnosis.listener.ActivityListener;
import edu.ni.unimelb.cdss.diagnosis.question.Question;


/**
 * 
 * A class for holdering static references to singleton resources. 
 * 
 * It also contains of constant variables used by dianosis module.
 * 
 * @author ilhamyputra
 *
 */
public class Static {
	
	public static String DANGER_SIGNS          = "edu.ni.unimelb.cdss.diagnosis.DangerSignsActivity";
	public static String KEY_SYMPTOMS          = "edu.ni.unimelb.cdss.diagnosis.KeySymptomsActivity";
	public static String QUESTIONS             = "edu.ni.unimelb.cdss.diagnosis.QuestionsActivity";
	public static String RECOMMENDATION        = "edu.ni.unimelb.cdss.diagnosis.RecommendationsActivity";
	public static String RECOMMENDATION_DIALOG = "edu.ni.unimelb.cdss.diagnosis.RecommendationsDialog";
	public static String FAST_BREATHING        = "edu.ni.unimelb.cdss.diagnosis.FastBreathingActivity";
	
	public static String AGE = "age";
	public static String IMCI = "imci";
	public static int questionActivityCode = 111;
	public static String FAST_BREATHING_CODE = "kf_breathing_problems_fast_breathing";
	
	public static ActivityListener currentActivity;
	public static CDSSEnvironment environment;
	public static Question currentQuestion;
	public static Structure recommendation;
	public static int idRecommendationItem;
	
	/*public static int getAgentEnvironment(String module, String criteria, String param){
		if(module.equals(IMCI)){
			if(criteria.equals(AGE)){
				return R.string.mas2j;
			}
		}
		return 0;
	}*/
}
