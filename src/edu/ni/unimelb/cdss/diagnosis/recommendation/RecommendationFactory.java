package edu.ni.unimelb.cdss.diagnosis.recommendation;

import edu.ni.unimelb.cdss.diagnosis.recommendation.Recommendation.Urgency;
import edu.ni.unimelb.cdss.diagnosis.utils.Duration;
import edu.ni.unimelb.cdss.diagnosis.utils.JasonUtils;
import jason.asSyntax.Atom;
import jason.asSyntax.StringTerm;
import jason.asSyntax.Structure;


/**
 * A factory class to generate recommendation items based on Jason structure
 * contents. Used by the recommendation screen activity to generate its
 * contents.
 * 
 * - TREATMENTS = treatments([TREATMENT])
 * 
 * - TREATMENT = treatment(TREATMENT_TYPE, TREATMENT_PARAMETERS,
 * TREATMENT_CONDITIONS, SEVERITY, TREATMENT_INFO)
 */
public class RecommendationFactory {
	/**
	 * The single access point to convert a treatment Jason structure into a
	 * Recommendation object.
	 * 
	 * @param treatmentStructure
	 *            The Jason structure to convert. Must be of the form
	 * @return
	 */
	public static Recommendation createTreatmentRecommendation(Structure treatmentStructure) {
		// treatment(TREATMENT_TYPE, TREATMENT_PARAMETERS, TREATMENT_CONDITIONS,
		// SEVERITY, TREATMENT_INFO)
		String treatmentType = ((Atom) treatmentStructure.getTerm(0)).getFunctor();
		Structure params = (Structure) treatmentStructure.getTerm(1);
		String conditions = ((StringTerm) treatmentStructure.getTerm(2)).getString();
		Urgency urgency = parseUrgency(((Atom) treatmentStructure.getTerm(3)).getFunctor());
		String info = ((StringTerm) treatmentStructure.getTerm(4)).getString();

		Recommendation rec = null;
		// Create the recommendation with a function depending on its type
		if (treatmentType.equals("referral")) {
			rec = parseReferral(params, conditions);
		} else if (treatmentType.equals("prescribed_drug")) {
			rec = parseDrug(params, conditions);
		} else if (treatmentType.equals("prescribed_supplement")) {
			rec = parseSupplement(params, conditions);
		} else if (treatmentType.equals("advice")) {
			rec = parseAdvice(params, conditions);
		} else if (treatmentType.equals("follow_up")) {
			rec = parseFollowUp(params, conditions);
		} else {
			rec = parseUnknown(params, conditions);
		}

		// Set the urgency
		rec.setUrgency(urgency);

		// Set the filename details if they exist
		if (!info.equals("")) {
			rec.setDetailsFilename(info);
		}
		return rec;
	}

	/**
	 * Convert a severity string into an urgency enum value.
	 */
	private static Urgency parseUrgency(String urgency) {
		return Urgency.valueOf(urgency.toUpperCase());
	}

	/**
	 * Create a referral recommendation.
	 * 
	 * referral_parameters(REFER_TO, REFER_WHEN, REFER_WHY, REFER_URGENT)
	 */
	private static Recommendation parseReferral(Structure params, String conditions) {
		String referTo = ((StringTerm) params.getTerm(0)).getString();
		Duration referWhen = JasonUtils.parseDuration(((Structure) params.getTerm(1)));
		String referWhy = ((StringTerm) params.getTerm(2)).getString();
		Boolean referUrgent = ((Atom) params.getTerm(3)).getFunctor().equals("true");
		return new ReferralRecommendation(referTo, referWhen, referWhy, referUrgent, conditions);
	}

	/**
	 * Create a drug recommendation.
	 * 
	 * prescribed_drug_parameters(DRUG_NAME, DRUG_DOSE, DRUG_DURATION)
	 */
	private static Recommendation parseDrug(Structure params, String conditions) {
		String name = ((StringTerm) params.getTerm(0)).getString();
		String dose = ((StringTerm) params.getTerm(1)).getString();
		Duration duration = JasonUtils.parseDuration(((Structure) params.getTerm(2)));
		return new DrugRecommendation(name, dose, duration, conditions);
	}

	/**
	 * Create a supplement recommendation.
	 * 
	 * prescribed_supplement_parameters(SUPP_NAME, SUPP_DOSE, SUPP_DURATION)
	 */
	private static Recommendation parseSupplement(Structure params, String conditions) {
		String name = ((StringTerm) params.getTerm(0)).getString();
		String dose = ((StringTerm) params.getTerm(1)).getString();
		Duration duration = JasonUtils.parseDuration(((Structure) params.getTerm(2)));
		return new SupplementRecommendation(name, dose, duration, conditions);
	}

	/**
	 * Create an advice recommendation.
	 * 
	 * advice_parameters(ADVICE_DESCRIPTION)
	 */
	private static Recommendation parseAdvice(Structure params, String conditions) {
		String advice = ((StringTerm) params.getTerm(0)).getString();
		return new AdviceRecommendation(advice, conditions);
	}

	/**
	 * Create a follow-up recommendation.
	 * 
	 * follow_up_parameters(FOLLOW_UP_WHEN)
	 */
	private static Recommendation parseFollowUp(Structure params, String conditions) {
		Duration duration = JasonUtils.parseDuration(((Structure) params.getTerm(0)));
		return new FollowUpRecommendation(duration, conditions);
	}

	/**
	 * Create an unknown recommendation, just displaying whatever information we
	 * have so that hopefully the user can figure it out. This should never
	 * happen if the agent data is well formed.
	 */
	private static Recommendation parseUnknown(Structure params, String conditions) {
		return new UnknownRecommendation(params.toString());
	}

}
