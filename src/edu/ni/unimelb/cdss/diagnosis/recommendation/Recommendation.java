package edu.ni.unimelb.cdss.diagnosis.recommendation;

import edu.ni.unimelb.cdss.R;
import edu.ni.unimelb.cdss.diagnosis.stc.Static;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;


/**
 * Template for Recommendation objects, be they outcomes or treatments.
 */
public abstract class Recommendation {
	/**
	 * Represents the set of possible relative urgencies of treatments (or
	 * equivalently severities of outcomes).
	 */
	public enum Urgency {
		HIGH, MEDIUM, LOW
	};

	/** The label to display in the recommendations list. */
	protected String label;
	/** The filename of the image to display details. */
	protected String detailsFilename;
	/** The severity of the outcome, or urgency of the treatment. */
	protected Urgency urgency = Urgency.HIGH;

	/** The recommendation view instance. */
	private View view;
	/** The left drawable indicating the urgency. */
	private Drawable urgencyDrawable;
	/** The right drawable indicating whether information is available. */
	private Drawable infoDrawable;

	/**
	 * Default constructor. Label should be set using setLabel in subclass
	 * constructor.
	 */
	public Recommendation() {
	}

	/**
	 * Constructor with additional setting of label.
	 */
	public Recommendation(String label) {
		this.label = label;
	}

	/**
	 * Create the view and set its label.
	 */
	public View createView(Context context) {
		// Inflate a new recommendation item
		LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = layoutInflater.inflate(Static.idRecommendationItem, null);

		// Set the background to look like a list
		view.setBackgroundResource(android.R.drawable.list_selector_background);

		// Draw the label
		TextView labelView = (TextView) view.findViewById(R.id.textRecLabel);
		labelView.setText(this.label);

		// Generate the drawables (urgency left and info right)
		urgencyDrawable = createUrgencyDrawable(context);
		// Only generate an info drawable if info is available
		if (getDetailsFilename() != null) {
			infoDrawable = context.getResources().getDrawable(R.drawable.question);
		}
		labelView.setCompoundDrawablesWithIntrinsicBounds(urgencyDrawable, null, infoDrawable, null);
		return view;
	}

	/**
	 * Create the appropriate urgency indicator drawable to place on the left of
	 * the recommendation list item, based on the severity of the
	 * recommendation.
	 */
	protected Drawable createUrgencyDrawable(Context context) {
		if (getUrgency() == Urgency.HIGH) {
			return context.getResources().getDrawable(R.drawable.urgency_red);
		} else if (getUrgency() == Urgency.MEDIUM) {
			return context.getResources().getDrawable(R.drawable.urgency_yellow);
		}
		if (getUrgency() == Urgency.LOW) {
			return context.getResources().getDrawable(R.drawable.urgency_green);
		} else {
			// Unknown, assume severe
			return context.getResources().getDrawable(R.drawable.urgency_red);
		}
	}

	/**
	 * Get this recommendation's view, and create it if it doesn't exist yet.
	 */
	public View getView(Context context) {
		if (view == null) {
			view = createView(context);
		}
		return view;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getDetailsFilename() {
		return detailsFilename;
	}

	public void setDetailsFilename(String detailsFilename) {
		this.detailsFilename = detailsFilename;
	}

	public Urgency getUrgency() {
		return urgency;
	}

	public void setUrgency(Urgency urgency) {
		this.urgency = urgency;
	}
}