package edu.ni.unimelb.cdss.diagnosis.recommendation;

import jason.asSyntax.Structure;

/**
 * Provides a callback to a listener class when a new recommendation is
 * available.
 */
public interface NewRecommendationListener {
	public void onNewRecommendation(Structure recommendations);
}