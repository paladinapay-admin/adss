package edu.ni.unimelb.cdss.diagnosis.recommendation;

import java.util.Comparator;

import edu.ni.unimelb.cdss.diagnosis.recommendation.Recommendation.Urgency;



/**
 * Sorts recommendations based on their urgency values, and sorts
 * recommendations of equal urgency by name.
 */
public class RecommendationComparator implements Comparator<Recommendation> {
	/**
	 * Compare the two questions.
	 * 
	 * @return -1 if question 1 should come first, 1 if second, and 0 if they
	 *         are effectively equal and order is unimportant.
	 */
	public int compare(Recommendation rec1, Recommendation rec2) {
		// Sort by urgency (decreasing)
		Urgency sev1 = rec1.getUrgency();
		Urgency sev2 = rec2.getUrgency();

		// Assume they are the same by default
		int cmp = 0;
		// If sev1 is more urgent
		if (sev1 == Urgency.HIGH || sev1 == Urgency.MEDIUM && sev2 == Urgency.LOW)
			cmp = -1;
		else if (sev1 != sev2)
			// If they're not in fact equal, the sev2 is more urgent
			cmp = 1;

		// If equal, sort alphabetically
		if (cmp == 0) {
			cmp = rec1.getLabel().compareTo(rec2.getLabel());
		}
		return cmp;
	}
}
