package edu.ni.unimelb.cdss.diagnosis.recommendation;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

/**
 * A simple adapter backed by an array list with many of the same behaviours
 * available.
 */
public class RecommendationListAdapter extends BaseAdapter {
	/** Context instance. */
	private final Context context;
	private List<Recommendation> recList;

	/**
	 * Constructor.
	 * 
	 * @param context
	 *            context instance.
	 * @param recList
	 *            question list.
	 */
	public RecommendationListAdapter(Context context) {
		this.context = context;
		recList = new ArrayList<Recommendation>();
	}

	/**
	 * Gets the number of elements in question list.
	 * 
	 * @see BaseAdapter#getCount()
	 */
	public int getCount() {
		return recList.size();
	}

	/**
	 * Gets the question item at given position.
	 * 
	 * @param poisition
	 *            item position
	 * @see BaseAdapter#getItem(int)
	 */
	public Recommendation getItem(int position) {
		return recList.get(position);
	}

	/**
	 * Gets the question id at given position.
	 * 
	 * @param position
	 *            item position
	 * @return question id
	 * @see BaseAdapter#getItemId(int)
	 */
	public long getItemId(int position) {
		return position;
	}

	/**
	 * Get the position of the given item.
	 */
	public int getPosition(Recommendation item) {
		return recList.indexOf(item);
	}
	
	/**
	 * Insert the given item at the given index.
	 */
	public void insert(int index, Recommendation item) {
		recList.add(index, item);
	}
	public void insert(Recommendation item) {
		recList.add(item);
	}
	
	/**
	 * Remove the given item from the list.
	 */
	public boolean remove(Recommendation item) {
		return recList.remove(item);
	}
	
	/**
	 * Remove the given item from the list.
	 */
	public Recommendation remove(int index) {
		return recList.remove(index);
	}

	/**
	 * Gets the item view for given position.
	 * 
	 * @param position
	 *            item position.
	 * @param convertView
	 *            existing view to use.
	 * @param parent
	 *            parent view.
	 */
	public View getView(int position, View convertView, ViewGroup parent) {
		return recList.get(position).getView(context);
	}

	public void updateRecommendationList(List<Recommendation> recList, Activity activity) {
		this.recList = recList;
		activity.runOnUiThread(new Runnable() {
			public void run() {
				notifyDataSetChanged();
			}
		});
	}

}
