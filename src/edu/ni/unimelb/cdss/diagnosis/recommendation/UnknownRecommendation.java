package edu.ni.unimelb.cdss.diagnosis.recommendation;

/**
 * Subclass of Recommendation that is instantiated if a recommendation of an
 * unknown type is provided.
 */
public class UnknownRecommendation extends Recommendation {
	public UnknownRecommendation(String name) {
		super(name);
	}
}
