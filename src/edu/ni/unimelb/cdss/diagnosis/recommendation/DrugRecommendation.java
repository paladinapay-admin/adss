package edu.ni.unimelb.cdss.diagnosis.recommendation;

import edu.ni.unimelb.cdss.diagnosis.utils.Duration;


/**
 * Subclass of Recommendation recommending the prescription of a drug.
 */
public class DrugRecommendation extends Recommendation {
	public DrugRecommendation(String name, String dose, Duration howLong, String conditions) {
		super();
		
		String label= !conditions.equals("") ?  conditions : "\n" ;
		label += "Prescribe " + name;
		label += !dose.equals("") ? " (" + dose.toLowerCase() + ")" : "";
		label += !howLong.isEmpty() ? " for " + howLong : "";
		
		setLabel(label);
	}
}
