package edu.ni.unimelb.cdss.diagnosis.recommendation;

import jason.asSyntax.ListTerm;
import jason.asSyntax.StringTerm;

import java.util.Arrays;

/**
 * Special class of recommendation item to display reported outcomes alongside
 * treatments.
 */
public class OutcomeRecommendation extends Recommendation {
	/**
	 * This is a hack to provide some way of determining the urgency (severity)
	 * of the outcome, given the agents cannot easily provide it in the current
	 * recommendation structure.
	 */
	/*private static String[] mediums = new String[] { "Anaemia", "Some Dehydration", "Persistent Diarrhoea",
			"Dysentery", "Malaria", "Measles with Eye or Mouth Complications", "Acute Ear Infection",
			"Chronic Ear Infection" };
	private static String[] lows = new String[] { "Cough or Cold", "No Dehydration", "Fever (Malaria Unlikely)",
			"Measles", "Not Very Low Weight", "No Ear Infection", "Very Low Weight", "No Anaemia" };
	*/
	public OutcomeRecommendation(ListTerm outcome) {
		super(((StringTerm) outcome.get(0)).getString());
		String urgency = ((StringTerm) outcome.get(1)).getString();
		// Set the severity of the outcome
		if (urgency.equals("MEDIUM"))
			setUrgency(Urgency.MEDIUM);
		else if (urgency.equals("LOW"))
			setUrgency(Urgency.LOW);
		else
			setUrgency(Urgency.HIGH);
	}
}
