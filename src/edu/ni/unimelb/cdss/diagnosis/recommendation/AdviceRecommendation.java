package edu.ni.unimelb.cdss.diagnosis.recommendation;

/**
 * Subclass of Recommendation providing the child's mother with advice.
 */
public class AdviceRecommendation extends Recommendation {
	public AdviceRecommendation(String advice, String conditions) {
		super((!conditions.equals("") ? conditions + "\n" : "") + advice);
	}
}
