package edu.ni.unimelb.cdss.diagnosis.utils;

import static jason.asSyntax.ASSyntax.createAtom;
import static jason.asSyntax.ASSyntax.createList;
import jason.asSyntax.Atom;
import jason.asSyntax.ListTerm;
import jason.asSyntax.Structure;
import jason.asSyntax.Term;

import java.util.ArrayList;
import java.util.List;

import android.widget.CheckBox;
import edu.ni.unimelb.cdss.guiutils.Switch;

/**
 * A set of utility functions for working with Jason data structures.
 * 
 * @author Nisix Team
 * @author reused and refined by ilhamyputra for converting checkbox answer
 * @version 0.3
 */
public class JasonUtils {
	/**
	 * Convert an array of strings into a path list.
	 * 
	 * @param path
	 *            An array of string to convert.
	 * @return A list of the given strings.
	 */
	public static ListTerm pathToList(final String[] path) {
		List<Term> terms = new ArrayList<Term>();
		for (String step : path) {
			terms.add(createAtom(step));
		}
		return createList(terms);
	}

	/**
	 * Convert a node path structure into a list of node strings.
	 */
	public static List<String> nodePathToStrings(Structure nodePath) {
		List<String> nodes = new ArrayList<String>();
		for (Term node : (ListTerm) nodePath.getTerm(0)) {
			nodes.add(((Atom) node).getFunctor());
		}
		return nodes;
	}

	/**
	 * Convert a node path structure into a list of node strings.
	 */
	public static String nodePathToString(List<String> nodePath) {
		String path = "";
		for (String node : nodePath) {
			path += node + "/";
		}
		return path.substring(0, path.length() - 1);
	}

	/**
	 * Checks whether a duration structure is empty (all zeroes).
	 */
	public static boolean isDurationEmpty(Structure duration) {
		if (duration == null)
			return true;
		return duration.getTerm(0).toString().equals("0") && duration.getTerm(1).toString().equals("0")
				&& duration.getTerm(2).toString().equals("0") && duration.getTerm(3).toString().equals("0")
				&& duration.getTerm(4).toString().equals("0") && duration.getTerm(5).toString().equals("0");
	}

	/**
	 * Convert a Jason duration into a Date object.
	 * 
	 * @param duration
	 *            A Jason structure representing a duration of the form:
	 *            DURATION = duration(YEARS, MONTHS, DAYS, HOURS, MINUTES,
	 *            SECONDS)
	 * @return The resulting Duration object.
	 */
	public static Duration parseDuration(Structure duration) {
		Duration output = new Duration();
		output.setYears(Integer.valueOf(duration.getTerm(0).toString()));
		output.setMonths(Integer.valueOf(duration.getTerm(1).toString()));
		output.setDays(Integer.valueOf(duration.getTerm(2).toString()));
		output.setHours(Integer.valueOf(duration.getTerm(3).toString()));
		output.setMinutes(Integer.valueOf(duration.getTerm(4).toString()));
		output.setSeconds(Integer.valueOf(duration.getTerm(5).toString()));
		return output;
	}

	/**
	 * Return the truthiness of the given checkbox's state as a String, just as
	 * Jason likes it.
	 */
	public static String isChecked(final CheckBox box) {
		if (Boolean.valueOf(box.isChecked())) {
			return "yes";
		} else {
			return "no";
		}
	}

	/**
	 * Return the truthiness of the given switch's state as a String, just as
	 * Jason likes it. Custom MySwitch Version.
	 */
	public static String isChecked(final Switch swi) {
		if (Boolean.valueOf(swi.isChecked())) {
			return "yes";
		} else {
			return "no";
		}
	}

}
