package edu.ni.unimelb.cdss.diagnosis.env;

import jason.asSyntax.ASSyntax;
import jason.asSyntax.Literal;
import jason.asSyntax.Structure;
import jason.environment.Environment;

import java.util.List;

import edu.ni.unimelb.cdss.diagnosis.stc.Static;

/**
 * Agents environment.
 * 
 * Handle belief and communication between agents.
 * 
 * Handle action from agent.
 * 
 * @author ilhamyputra
 * @version 0.3
 * 
 */
public class CDSSEnvironment extends Environment {

	/*
	 * (non-Javadoc)
	 * 
	 * @see jason.environment.Environment#init(java.lang.String[])
	 */
	@Override
	public void init(String[] args) {
		Static.environment = this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jason.environment.Environment#executeAction(java.lang.String,
	 * jason.asSyntax.Structure)
	 */
	public boolean executeAction(String ag, Structure action) {
		if (action.getFunctor().equals("registerSymptom")) {
			addPercept(ASSyntax.createLiteral(Literal.LPos, "keysymptom",
					ASSyntax.createAtom(action.getTerm(0).toString()), ASSyntax.createAtom("yes")));
		}
		if (action.getFunctor().equals("removeAsk")) {
			removePercept(Literal.parseLiteral("ask"));
		}
		if (action.getFunctor().equals("checkOutcome")) {
			addPercept(Literal.parseLiteral("checkOutcome"));
		}
		/*
		 * if(action.getFunctor().equals("prepareOutcome")){
		 * addPercept(Literal.parseLiteral("prepareOutcome")); }
		 */
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jason.environment.Environment#addPercept(jason.asSyntax.Literal)
	 */
	@Override
	public void addPercept(Literal per) {
		super.addPercept(per);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jason.environment.Environment#getPercepts(java.lang.String)
	 */
	@Override
	public List<Literal> getPercepts(String agName) {
		return super.getPercepts(agName);
	}
}
