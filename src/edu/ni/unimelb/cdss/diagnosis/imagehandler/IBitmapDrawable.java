package edu.ni.unimelb.cdss.diagnosis.imagehandler;

import android.graphics.Bitmap;

/**
 * Base interface used in the {@link ImageViewTouchBase} view
 * @author alessandro
 *
 */
public interface IBitmapDrawable {

	Bitmap getBitmap();
}
