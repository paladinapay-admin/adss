package edu.ni.unimelb.cdss.diagnosis.imagehandler;

public interface IDisposable {

	void dispose();
}
