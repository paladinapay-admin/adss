package edu.ni.unimelb.cdss.diagnosis.question;

/**
 * 
 * Question represents the contents of a required feature question to the user,
 * including details of the question itself such as key symptom related to this
 * question, feature code and string of the question.
 * 
 * @author ilhamyputra
 * @version 0.3
 */

public class Question {

	/**
	 * key symptom reference
	 */
	private String keySymptom;

	/**
	 * feature code reference
	 */
	private String featureCode;

	/**
	 * question string reference
	 */
	private String questionString;

	/**
	 * question help string reference
	 */
	private String questionHelp;

	/**
	 * question help image reference
	 */
	private String questionHelpImage;

	/**
	 * @return key symptom
	 */
	public String getKeySymptom() {
		return keySymptom;
	}

	/**
	 * @param keySymptom
	 */
	public void setKeySymptom(String keySymptom) {
		this.keySymptom = keySymptom;
	}

	/**
	 * @return feature code
	 */
	public String getFeatureCode() {
		return featureCode;
	}

	/**
	 * @param featureCode
	 */
	public void setFeatureCode(String featureCode) {
		this.featureCode = featureCode;
	}

	/**
	 * @return question string
	 */
	public String getQuestionString() {
		return questionString;
	}

	/**
	 * @param questionString
	 */
	public void setQuestionString(String questionString) {
		this.questionString = questionString;
	}

	/**
	 * @return question help string
	 */
	public String getQuestionHelp() {
		return questionHelp;
	}

	/**
	 * @param questionHelp
	 */
	public void setQuestionHelp(String questionHelp) {
		this.questionHelp = questionHelp;
	}

	/**
	 * @return question help image
	 */
	public String getQuestionHelpImage() {
		return questionHelpImage;
	}

	/**
	 * @param questionHelpImage
	 */
	public void setQuestionHelpImage(String questionHelpImage) {
		this.questionHelpImage = questionHelpImage;
	}
}
