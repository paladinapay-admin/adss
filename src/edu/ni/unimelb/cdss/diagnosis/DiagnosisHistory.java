package edu.ni.unimelb.cdss.diagnosis;

import java.io.Serializable;
import java.util.ArrayList;

import android.content.Context;
import edu.ni.unimelb.cdss.database.DataSource;
import edu.ni.unimelb.cdss.patientInfo.PatientInfo;

//import edu.ni.unimelb.cdss.database.DataProvider;

/**
 * @author Team A, Eman K
 * 
 *         This represent the DiagnosisHistory Model, which contains Medical
 *         diagnosis history of one patient. This object will get PatientInfo
 *         and PatientTempreture values form ApplicationController once it
 *         Initializes in DangerSignsActivity
 * 
 * @version 0.3
 */
public class DiagnosisHistory implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * DiagnosisHistory object values
	 */
	private String P_ID;
	private String timeStamp;
	private String features;
	private String outcomes;
	private String recommendation;
	private String patientTemprature;

	// Declared by patrick

	private transient DataSource datasource;

	public DiagnosisHistory() {
		this.setP_ID("");
		this.setTimeStamp("");
		this.setPatientTempreture("");
		this.setFeatures("");
		this.setOutcomes("");
		this.setRecommendation("");
	}

	public DiagnosisHistory(String P_ID, String timeStamp, String patientTemperature, String features, String outcome,
			String recommendation) {
		// TODO Auto-generated constructor stub

		this.setP_ID(P_ID);
		this.setTimeStamp(timeStamp);
		this.setPatientTempreture(patientTemprature);
		this.setFeatures(features);
		this.setOutcomes(outcomes);
		this.setRecommendation(recommendation);
	}

	public String getP_ID() {
		return P_ID;
	}

	public void setP_ID(String p_ID) {
		P_ID = p_ID;
	}

	public String getTimeStamp() {

		return timeStamp;
	}

	private void setTimeStamp(String timeStamp) {
		// TODO Auto-generated method stub
		this.timeStamp = timeStamp;

	}

	/**
	 * @return patientTemprature Use this function to get the current
	 *         patientTemprature value
	 */
	public String getPatientTempreture() {

		return patientTemprature;
	}

	/**
	 * This function will be set patientTemprature from Application Class
	 */
	public void setPatientTempreture(String temp) {

		this.patientTemprature = temp;
	}

	/**
	 * @return recommendation
	 */
	public String getRecommendation() {
		return recommendation;
	}

	/**
	 * @param recommendation
	 */
	public void setRecommendation(String recommendation) {
		this.recommendation = recommendation;
	}

	/**
	 * @return features
	 */
	public String getFeatures() {
		return features;
	}

	/**
	 * @param features
	 */
	public void setFeatures(String features) {
		this.features = features;
	}

	/**
	 * @return outcomes
	 */
	public String getOutcomes() {
		return outcomes;
	}

	/**
	 * @param outcomes
	 */
	public void setOutcomes(String outcomes) {
		this.outcomes = outcomes;
	}

	/**
	 * @param context
	 * @param dh
	 * @return
	 */
	public boolean insert(Context context, DiagnosisHistory dh) {
		datasource = new DataSource(context);
		datasource.open();

		try {
			datasource.insertDignosisHistory(dh);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * @author Patrick, Eman K please add here
	 * @param context
	 * @param pi
	 * @return
	 */
	public ArrayList<DiagnosisHistory> retrievePatientDiagnosis(Context context, String p_id, PatientInfo pi) {
		datasource = new DataSource(context);
		datasource.open();

		ArrayList<DiagnosisHistory> list = new ArrayList<DiagnosisHistory>();

		list = datasource.queryDiagnosisHistory(p_id);

		return list;
	}

}
