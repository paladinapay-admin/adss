package edu.ni.unimelb.cdss.diagnosis.agent;

import jason.architecture.AgArch;
import jason.asSemantics.ActionExec;
import jason.asSyntax.Structure;

import java.util.List;

import edu.ni.unimelb.cdss.R;
import edu.ni.unimelb.cdss.diagnosis.question.Question;
import edu.ni.unimelb.cdss.diagnosis.stc.Static;

/**
 * Handle interaction between GUI and agent layer.
 * 
 * @author ilhamyputra
 * @version 0.3
 * 
 */
public class UserInteraction extends AgArch {

	/*
	 * (non-Javadoc)
	 * 
	 * @see jason.architecture.AgArch#act(jason.asSemantics.ActionExec,
	 * java.util.List)
	 */
	@Override
	public void act(ActionExec action, List<ActionExec> feedback) {
		String functor = action.getActionTerm().getFunctor();
		if (functor.equals("agentsIntialised")) {
			action.setResult(true);
			feedback.add(action);
			Static.currentActivity.next(Static.DANGER_SIGNS);
		} else if (functor.equals("gotokeysymptoms")) {
			action.setResult(true);
			feedback.add(action);
			Static.currentActivity.next(Static.KEY_SYMPTOMS);
		} else if (functor.equals("askGUI")) {
			action.setResult(true);
			feedback.add(action);
			Static.currentQuestion = new Question();
			Static.currentQuestion.setKeySymptom(action.getActionTerm().getTerm(0).toString());
			Static.currentQuestion.setFeatureCode(action.getActionTerm().getTerm(1).toString());
			Static.currentQuestion.setQuestionString(action.getActionTerm().getTerm(2).toString().replaceAll("\"", ""));
			Static.currentQuestion.setQuestionHelp(action.getActionTerm().getTerm(3).toString().replaceAll("\"", ""));
			Static.currentQuestion.setQuestionHelpImage(action.getActionTerm().getTerm(4).toString()
					.replaceAll("\"", ""));
			if (Static.currentQuestion.getFeatureCode().equals(Static.FAST_BREATHING_CODE)) {
				Static.currentActivity.next(Static.FAST_BREATHING);
			} else {
				Static.currentActivity.next(Static.QUESTIONS);
			}
		} else if (functor.equals("sendRecommendation")) {
			Static.recommendation = (Structure) action.getActionTerm().getTerm(0);
			Static.idRecommendationItem = R.layout.recommendation_item;
			Static.currentActivity.next(Static.RECOMMENDATION);
		} else if (functor.equals("sendAlertRecommendation")) {
			Static.recommendation = (Structure) action.getActionTerm().getTerm(0);
			Static.idRecommendationItem = R.layout.recommendation_dialog_item;
			System.out.println(Static.currentActivity.getClass());
			Static.currentActivity.next(Static.RECOMMENDATION_DIALOG);
		} else {
			// calls the default implementation
			super.act(action, feedback);
		}
	}
}