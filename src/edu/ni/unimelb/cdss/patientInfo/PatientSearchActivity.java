package edu.ni.unimelb.cdss.patientInfo;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import edu.ni.unimelb.cdss.R;
import edu.ni.unimelb.cdss.allData.AllDataActivity;
import edu.ni.unimelb.cdss.control.ApplicationController;
import edu.ni.unimelb.cdss.control.HomeActivity;

/**
 * This Activity the first one in the Application, to search for the patients
 * then redirect the user to the suitable screens
 * 
 * @author Team A, Eman K
 * @version 0.3
 * 
 */
public class PatientSearchActivity extends Activity {
	/**
	 * Search screen views elements
	 */
	private EditText mPhoneEditTextSer;
	private EditText mFNameEditTextSer;
	private EditText mLNameEditTextSer;
	private Button mSearchButton;
	ImageButton mHomeButton;
	ImageButton mBackbutton;
	/**
	 * QR reader button
	 */
	// private Button mQRReaderButton;

	private ApplicationController app;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.patient_search_view);
		// get the context of application .
		app = ((ApplicationController) getApplicationContext());

		// Refer to initialize PatientInfo.
		// app.initPatientInfo();

		mSearchButton = (Button) findViewById(R.id.search_button);
		mSearchButton.setOnClickListener(new SearchButtonListener());

		// mQRReaderButton = (Button) findViewById(R.id.qr_code);
		// mQRReaderButton.setOnClickListener(new QRReaderButtonListener());

		mPhoneEditTextSer = (EditText) findViewById(R.id.phone_search);
		mFNameEditTextSer = (EditText) findViewById(R.id.edit_fname1);
		mLNameEditTextSer = (EditText) findViewById(R.id.edit_lname1);

		mHomeButton = (ImageButton) findViewById(R.id.homeButtonSerach);
		mHomeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			// On click function
			public void onClick(View view) {
				// Create the intent to start another activity
				Intent intent = new Intent(view.getContext(), HomeActivity.class);
				startActivity(intent);
			}
		});

		mBackbutton = (ImageButton) findViewById(R.id.backButtonSearch);
		mBackbutton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				finish();
			}
		});

	}

	// /**
	// * @author Lucas should add QR.
	// */
	// class QRReaderButtonListener implements View.OnClickListener {
	//
	// @Override
	// public void onClick(View v) {
	//
	// Intent i = new Intent(PatientSearchActivity.this,
	// CameraActivity.class);
	// startActivity(i);
	//
	// Toast.makeText(PatientSearchActivity.this, "Scanning",
	// Toast.LENGTH_LONG).show();
	// }
	//
	// }

	class SearchButtonListener implements View.OnClickListener {

		@Override
		public void onClick(View v) {

			Toast toast = null;
			/**
			 * Check on syncho. TODO
			 */
			if (!app.isMyServiceRunning()) {
				toast = Toast.makeText(PatientSearchActivity.this, R.string.service_is_running, Toast.LENGTH_LONG);
				toast.getView().setBackgroundColor(Color.YELLOW);
				toast.show();
			} else {
				PatientInfo p = new PatientInfo();
				/**
				 * collect the data from the edit texts
				 */
				if (collectData(p))
					FindPatient(app.getApplicationContext(), p);
				// else {
				// toast = Toast.makeText(PatientSearchActivity.this,
				// R.string.patientsearch, Toast.LENGTH_LONG);
				// toast.getView().setBackgroundColor(Color.RED);
				// toast.show();
				// }
			}
		}
	}

	/**
	 * @author Team A, Eman K SearchButtonListener to listen to Search button
	 *         and do the search functions
	 * @return boolean if the fields are enough to search
	 */
	public boolean collectData(PatientInfo patient) {
		/*
		 * if ALL fields are empty, display all data
		 */

		// PatientInfo patient = app.getPatient();
		if (mPhoneEditTextSer.getText().length() == 0 && mFNameEditTextSer.getText().length() == 0
				&& mLNameEditTextSer.getText().length() == 0) {

			// mPhoneEditTextSer.setError("Please enter Patient phone number");
			// mFNameEditTextSer.setError("Please enter Patient first name");
			// mLNameEditTextSer.setError("Please enter Patient last name");

			Intent toAllData = new Intent(PatientSearchActivity.this, AllDataActivity.class);
			startActivity(toAllData);

			return false;

		} else {

			if (mPhoneEditTextSer.getText().length() != 0) {
				patient.setPhone(mPhoneEditTextSer.getText().toString());
			}

			if (mFNameEditTextSer.getText().length() != 0) {
				patient.setF_name(mFNameEditTextSer.getText().toString());
			}

			if (mLNameEditTextSer.getText().length() != 0) {
				patient.setL_name(mLNameEditTextSer.getText().toString());
			}

			return true;

		}

		// /*
		// * if All fields are available, Search for patients.
		// */
		// if (mPhoneEditTextSer.getText().length() != 0 &&
		// mFNameEditTextSer.getText().length() != 0
		// && mLNameEditTextSer.getText().length() != 0) {
		// patient.setPhone(mPhoneEditTextSer.getText().toString());
		// patient.setF_name(mFNameEditTextSer.getText().toString());
		// patient.setL_name(mLNameEditTextSer.getText().toString());
		// return true;
		// }
		// /*
		// * if first and last names are available, search for patients
		// */
		// if (mFNameEditTextSer.getText().length() != 0 &&
		// mLNameEditTextSer.getText().length() != 0
		// && mPhoneEditTextSer.getText().length() == 0) {
		// patient.setF_name(mFNameEditTextSer.getText().toString());
		// patient.setL_name(mLNameEditTextSer.getText().toString());
		//
		// return true;
		// }
		// /*
		// * if phone is available, search for patients
		// */
		// if (mPhoneEditTextSer.getText().length() != 0
		// && (mFNameEditTextSer.getText().length() == 0 ||
		// mLNameEditTextSer.getText().length() == 0)) {
		// patient.setPhone(mPhoneEditTextSer.getText().toString());
		//
		// return true;
		//
		// }
		// /*
		// * if NO phone and one of names not available, set errors
		// */
		// if (mPhoneEditTextSer.getText().length() == 0
		// && (mFNameEditTextSer.getText().length() == 0 ||
		// mLNameEditTextSer.getText().length() == 0)) {
		//
		// mFNameEditTextSer.setError("Please enter Patient first name");
		// mLNameEditTextSer.setError("Please enter Patient last name");
		// return false;
		// }
		// return false;

	}

	/**
	 * @author Eman K
	 * @param pi
	 *            PatientInfo This function will search in Database and redirect
	 *            the result to other activities. By saving in
	 *            ApplicationController.
	 */
	public void FindPatient(Context context, PatientInfo pi) {

		ArrayList<PatientInfo> results = new ArrayList<PatientInfo>();
		results.clear();
		results = pi.retrievePatients(context, pi);
		if (results.size() == 0)

		{// no patients
			app.setPatient(pi);
			Toast.makeText(PatientSearchActivity.this, R.string.patientIsNew, Toast.LENGTH_LONG).show();
			Intent toform = new Intent(PatientSearchActivity.this, PatientInfoFormActivity.class);
			startActivity(toform);
		} else if (results.size() == 1) {
			// There exact match
			/*
			 * Save this patient in ApplicationController
			 */
			app.setPatient(results.get(0));
			Intent toOne = new Intent(PatientSearchActivity.this, PatientInfoPreviewActivity.class);
			startActivity(toOne);
		} else if (results.size() > 1) {
			app.setPatient(pi);
			// There are many patients
			Intent toresult = new Intent(PatientSearchActivity.this, PatientSearchResultActivity.class);
			startActivity(toresult);
		}
	}

	private int group1Id = 1;

	int exit = 1;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		menu.add(group1Id, exit, exit, "").setTitle("Exit");

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case 1:
			Intent exit = new Intent(this, HomeActivity.class);
			exit.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			exit.putExtra("EXIT", true);
			startActivity(exit);
			return true;

		default:
			break;

		}
		return super.onOptionsItemSelected(item);
	}

}
