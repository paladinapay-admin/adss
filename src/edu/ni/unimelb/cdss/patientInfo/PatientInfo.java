package edu.ni.unimelb.cdss.patientInfo;

import java.io.Serializable;
import java.util.ArrayList;

import android.content.Context;
import edu.ni.unimelb.cdss.database.DataSource;

// will contain the patient information
/**
 * 
 * This class represent the Patient Information model or object that holds
 * patient personal information This class will be responsible to interact with
 * data base layer to insert and retrieve patients.
 * 
 * @author Team A, Eman K
 * @version 0.3
 */
public class PatientInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * PatientInfo object variables. ID, first name, last name, phone number
	 * age, data of birth, sex, registed or not
	 * 
	 */
	private String P_ID;
	private String F_name;
	private String L_name;
	private String phone;
	private String date_of_birth;
	private String age;
	private String sex;
	private String address;
	private String last_update;
	private String registered;

	/*
	 * @author: Patrick add a declaration of database
	 */
	private transient DataSource datasource;

	/*
	 * Empty constructor
	 */
	public PatientInfo() {
		phone = "";
		F_name = "";
		L_name = "";
	}

	/*
	 * @author: Patrick constructor needed by data source layer
	 */
	public PatientInfo(String P_ID, String F_name, String L_name, String phone, String date_of_birth, String age,
			String sex, String address, String last_update, String registered) {
		this.P_ID = P_ID;
		this.F_name = F_name;
		this.L_name = L_name;
		this.phone = phone;
		this.date_of_birth = date_of_birth;
		this.age = age;
		this.sex = sex;
		this.address = address;
		this.last_update = last_update;
		this.registered = registered;
	}

	/**
	 * @return patient ID.
	 */
	public String getP_ID() {
		return P_ID;
	}

	/**
	 * @param p_ID
	 *            ID
	 */
	public void setP_ID(String p_ID) {
		P_ID = p_ID;
	}

	/**
	 * @return patient's first name
	 */
	public String getF_name() {
		return F_name;
	}

	/**
	 * @param f_name
	 *            's first name
	 */
	public void setF_name(String f_name) {
		F_name = f_name;
	}

	/**
	 * @return patient's last name
	 */
	public String getL_name() {
		return L_name;
	}

	/**
	 * @param l_name
	 *            's last name
	 */
	public void setL_name(String l_name) {
		L_name = l_name;
	}

	/**
	 * @return phone number
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone
	 *            number
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return date of birth
	 */
	public String getDate_of_birth() {
		return date_of_birth;
	}

	/**
	 * @param date_of_birth
	 */
	public void setDate_of_birth(String date_of_birth) {
		this.date_of_birth = date_of_birth;
	}

	/**
	 * @return age
	 */
	public String getAge() {
		return age;
	}

	/**
	 * @param age
	 */
	public void setAge(String age) {
		this.age = age;
	}

	/**
	 * @return date of last update
	 */
	public String getLast_update() {
		return last_update;
	}

	/**
	 * @param last_update
	 *            of last update
	 */
	public void setLatst_update(String last_update) {
		this.last_update = last_update;
	}

	/**
	 * @return address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return sex
	 */
	public String getSex() {
		return sex;
	}

	/**
	 * @param sex
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}

	/**
	 * @return if patient already Registered or not, 0 NOT Registered, 1 is
	 *         Registered
	 */
	public String isRegistered() {
		return registered;
	}

	/**
	 * @param registered
	 */
	public void setRegistered(String registered) {
		this.registered = registered;
	}

	/*
	 * :::::::::::::::::::::::::DataBase:::::::::::::::::::::::::::::::::::::
	 */

	/**
	 * @author Patrick Zhao
	 * @param context
	 * @param pi
	 *            :single patient with information to insert
	 * @return boolean if insert correctly.
	 */
	public boolean insert(Context context, PatientInfo pi) {
		datasource = new DataSource(context);
		datasource.open();

		try {
			datasource.insertPatientInfo(pi);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * @author Eman K
	 * @param patient
	 * @return boolean if insert correctly.
	 */
	public boolean insertPatient(Context context, PatientInfo patient) {
		if (insert(context, patient))
			return true;
		else
			return false;
	}

	/**
	 * @author Eman K, Patrick Zhao, Chi Wang(case insensitive)
	 * @param pi
	 *            is the patient with the 3 key values.
	 * @return results of all patients who have the same information of pi
	 */
	public ArrayList<PatientInfo> retrievePatients(Context context, PatientInfo pi) {
		datasource = new DataSource(context);
		datasource.open();
		ArrayList<PatientInfo> list = new ArrayList<PatientInfo>();
		ArrayList<PatientInfo> results = new ArrayList<PatientInfo>();

		list = datasource.queryPatientInfo();
		results.clear();
		// Any 1 of 3 key values right could get the patient
		for (int i = 0; i < list.size(); i++) {
			if ((pi.getPhone().length() != 0 && list.get(i).getPhone().toLowerCase()
					.contains(pi.getPhone().toLowerCase()))
					|| (pi.getL_name().length() != 0 && list.get(i).getL_name().toLowerCase()
							.contains(pi.getL_name().toLowerCase()))
					|| (pi.getF_name().length() != 0 && list.get(i).getF_name().toLowerCase()
							.contains(pi.getF_name().toLowerCase()))) {
				results.add(list.get(i));
			}
		}
		return results;
	}

	/**
	 * @author Patrick
	 * @param patient
	 * @return boolean if insert correctly.
	 */
	public boolean updatePatient(Context context, PatientInfo patient) {
		if (update(context, patient))
			return true;
		else
			return false;
	}

	private boolean update(Context context, PatientInfo patient) {
		datasource = new DataSource(context);
		datasource.open();

		try {
			datasource.updatePatientInfo(patient);
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	/**
	 * @author Patrick
	 * @param context
	 * @return
	 * 
	 *         Retrieve patient information when necessary
	 */
	public ArrayList<PatientInfo> retrieveAllPatients(Context context) {
		datasource = new DataSource(context);
		datasource.open();
		return datasource.queryPatientInfo();
	}

}
