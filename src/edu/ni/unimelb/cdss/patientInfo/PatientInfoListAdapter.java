package edu.ni.unimelb.cdss.patientInfo;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import edu.ni.unimelb.cdss.R;

/**
 * this is custom ArrayAdapter to show list of patients files in ListView.
 * 
 * @author Team A, Eman K
 * @version 0.3
 */
public class PatientInfoListAdapter extends ArrayAdapter<PatientInfo> {

	private Context context;

	public PatientInfoListAdapter(Context context, int textViewResourceId, ArrayList<PatientInfo> items) {
		super(context, textViewResourceId, items);
		this.context = context;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.patient_raw_view, null);
		}

		PatientInfo item = getItem(position);
		if (item != null) {
			TextView itemView = (TextView) view.findViewById(R.id.label);
			if (itemView != null) {
				// how to dispaly the information in one raw of patient
				itemView.setText(String.format("ID:%s,   Name:%s %s,   Phone:%s,   Date of birth:%s  ", item.getP_ID(),
						item.getF_name(), item.getL_name(), item.getPhone(), item.getDate_of_birth()));

			}
		}

		return view;
	}
}