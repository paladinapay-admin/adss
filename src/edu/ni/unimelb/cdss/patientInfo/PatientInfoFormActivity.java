package edu.ni.unimelb.cdss.patientInfo;

import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import edu.ni.unimelb.cdss.R;
import edu.ni.unimelb.cdss.control.ApplicationController;
import edu.ni.unimelb.cdss.control.HomeActivity;
import edu.ni.unimelb.cdss.diagnosis.DiagnosisHistory;

/**
 * This class represent the Patient Information form for adding new patient.
 * 
 * @author Team A, Eman K
 * @version 0.3
 */
public class PatientInfoFormActivity extends Activity {

	/**
	 * These PatientInfo details form to be filled by user.
	 */
	private PatientInfo patient;
	private EditText mFNameEditText;
	private EditText mLNameEditText;
	private EditText mPhoneEditText;
	private EditText mAddress;
	private RadioGroup mRadioSexGroup;
	private RadioButton mRadioSex;
	private ImageButton mDateOfBirthImageButton;
	private TextView mDateOfBirthText;
	private TextView mAgeText;
	private TextView mYearsText;
	private TextView mMonthsText;
	private EditText mAgeYears;
	private EditText mAgeMonths;
	private TextView mDateOfbirthEditText;
	private String Pid;

	ImageButton mHomeButton;
	ImageButton mBackbutton;
	/**
	 * @author ChiWang
	 */
	private RadioButton mMale;
	private RadioButton mFemale;

	/**
	 * Add button for next step (insert) and for search
	 */
	private Button mAddButton;
	/**
	 * To get the date and the current date from Calendar object
	 */
	private int birth_year, birth_month, birth_day, age;
	private int mYear, mMonth, mDay;
	static final int DATE_DIALOG_ID = 0;
	/**
	 * @author ChiWang
	 */
	// private Button scanButton;
	private Intent intent;
	private Bundle bundle;

	/**
	 * ApplicationController instance to record current patient info in
	 * ApplicationController
	 */
	private ApplicationController app;
	private DiagnosisHistory history;

	public PatientInfoFormActivity() {
		// Assign current Date
		final Calendar c = Calendar.getInstance();
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);
		Pid = "";
	}

	/*
	 * @see android.app.Activity#onCreate(android.os.Bundle) onCreate will be
	 * called when this Activity is created, to initialize its content
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.patient_info_form_view);

		// get the context of application .
		app = ((ApplicationController) getApplicationContext());
		patient = app.getPatient();
		history = app.getHistory();
		/*
		 * For all "Buttons","textEdits", "TextView", "CheckBox", "RadioButton":
		 * get reference to inflated View object set listener on these objects
		 * to response user action and get information from them.
		 */
		mAddButton = (Button) findViewById(R.id.add_button);
		mAddButton.setOnClickListener(new AddButtonListener(this));

		mHomeButton = (ImageButton) findViewById(R.id.homeButtonForm);
		mHomeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			// On click function
			public void onClick(View view) {
				// Create the intent to start another activity
				Intent intent = new Intent(view.getContext(), HomeActivity.class);
				startActivity(intent);
			}
		});

		mBackbutton = (ImageButton) findViewById(R.id.backButtonForm);
		mBackbutton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				finish();
			}
		});

		mFNameEditText = (EditText) findViewById(R.id.edit_fname);
		mLNameEditText = (EditText) findViewById(R.id.edit_lname);
		mPhoneEditText = (EditText) findViewById(R.id.phone);

		if (patient.getF_name() != null)
			mFNameEditText.setText(patient.getF_name().toString());
		if (patient.getPhone() != null)
			mPhoneEditText.setText(patient.getPhone().toString());
		if (patient.getL_name() != null)
			mLNameEditText.setText(patient.getL_name().toString());

		mAddress = (EditText) findViewById(R.id.addressEditText);
		mRadioSexGroup = (RadioGroup) findViewById(R.id.sexGroup);
		mAgeText = (TextView) findViewById(R.id.age_textform);
		mYearsText = (TextView) findViewById(R.id.years_textform);
		mMonthsText = (TextView) findViewById(R.id.months_textform);
		mAgeYears = (EditText) findViewById(R.id.patientAge_form);
		mAgeYears.setText("0");
		mAgeMonths = (EditText) findViewById(R.id.editTextAgeMonths);
		mAgeMonths.setText("0");
		mAgeMonths.setFilters(new InputFilter[] { new InputFilterMinMax("0", "11") });
		mDateOfBirthText = (TextView) findViewById(R.id.date_text);
		mDateOfBirthImageButton = (ImageButton) findViewById(R.id.date_of_birth_button);
		mDateOfbirthEditText = (TextView) findViewById(R.id.birth_date_form);
		// if the date of birth and age are got before do not show these fields
		// again
		if (app.getpDateOfBirth().length() == 0 || app.getpAge_years().length() == 0) {
			mLNameEditText.setNextFocusDownId(R.id.date_of_birth_button);
			mDateOfBirthImageButton.setOnClickListener(new View.OnClickListener() {

				public void onClick(View v) {
					// Show the DatePickerDialog

					showDialog(DATE_DIALOG_ID);
				}
			});
		} else {
			mAgeYears.setEnabled(false);
			mAgeYears.setVisibility(View.GONE);
			mAgeMonths.setEnabled(false);
			mAgeMonths.setVisibility(View.GONE);
			mAgeText.setEnabled(false);
			mAgeText.setVisibility(View.GONE);
			mYearsText.setEnabled(false);
			mYearsText.setVisibility(View.GONE);
			mMonthsText.setEnabled(false);
			mMonthsText.setVisibility(View.GONE);
			mDateOfBirthText.setEnabled(false);
			mDateOfBirthText.setVisibility(View.GONE);
			mDateOfBirthImageButton.setEnabled(false);
			mDateOfBirthImageButton.setVisibility(View.GONE);
			mDateOfbirthEditText.setEnabled(false);
			mDateOfbirthEditText.setVisibility(View.GONE);
			patient.setAge(app.getpAge_years());
			patient.setDate_of_birth(app.getpDateOfBirth());
		}
		/**
		 * @author ChiWang
		 */
		mMale = (RadioButton) findViewById(R.id.male);
		mFemale = (RadioButton) findViewById(R.id.female);

		/**
		 * @author ChiWang
		 */
		intent = this.getIntent();
		bundle = intent.getExtras();

		if (bundle != null && !bundle.getString("mIDEditText").equals("CDSSQRCODEINFORMATIONERROR")) {
			Pid = bundle.getString("mIDEditText");
			mFNameEditText.setText(bundle.getString("mFNameEditText"));
			mLNameEditText.setText(bundle.getString("mLNameEditText"));
			mPhoneEditText.setText(bundle.getString("mPhoneEditText"));
			mDateOfbirthEditText.setText(bundle.getString("mSelectDate"));
			String[] token = bundle.getString("mSelectDate").split("/");
			birth_year = Integer.parseInt(token[2]);
			birth_month = Integer.parseInt(token[1]);
			birth_day = Integer.parseInt(token[0]);
			int currenMonth = mMonth + 1;
			age = 0;
			int day = 0;
			age = mYear - birth_year;
			int months = currenMonth - birth_month;
			// if month difference is in negative then reduce years by one and
			// calculate the number of months.
			if (months < 0) {
				age--;
				months = 12 - birth_month + currenMonth;

				if (mDay < birth_day)
					months--;

			} else if (months == 0 && mDay < birth_day) {
				age--;
				months = 11;
			}
			day = mDay - birth_day;

			if (mDay == birth_day) {
				day = 0;
				if (months == 12) {
					age++;
					months = 0;
				}
			}
			if (age < 0)
				age = 0;
			mAgeYears.setText(String.valueOf(age));
			mAgeMonths.setText(String.valueOf(months));

			mAddress.setText(bundle.getString("mAddress"));
			if (bundle.getString("mSex").equals(mMale.getText().toString())) {
				mRadioSexGroup.check(mMale.getId());
			} else {
				mRadioSexGroup.check(mFemale.getId());
			}

		}

	}

	/**
	 * Register DatePickerDialog listener
	 */
	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
		// the callback received when the user "sets" the Date in the
		// DatePickerDialog
		public void onDateSet(DatePicker view, int yearSelected, int monthOfYear, int dayOfMonth) {
			birth_year = yearSelected;
			birth_month = monthOfYear + 1;
			birth_day = dayOfMonth;
			int currenMonth = mMonth + 1;
			// Set the Selected Date in Select date EditText
			if (mYear < birth_year || (mYear == birth_year && currenMonth < birth_month)) {
				Toast.makeText(PatientInfoFormActivity.this, "The Data Of birth is invalid", Toast.LENGTH_SHORT).show();
			} else {

				mDateOfbirthEditText.setText(birth_day + " - " + birth_month + " - " + birth_year);
				age = 0;
				int day = 0;
				age = mYear - birth_year;
				int months = currenMonth - birth_month;
				// if month difference is in negative then reduce years by one
				// and
				// calculate the number of months.
				if (months < 0) {
					age--;
					months = 12 - birth_month + currenMonth;

					if (mDay < birth_day)
						months--;

				} else if (months == 0 && mDay < birth_day) {
					age--;
					months = 11;
				}
				day = mDay - birth_day;

				if (mDay == birth_day) {
					day = 0;
					if (months == 12) {
						age++;
						months = 0;
					}
				}
				if (age < 0)
					age = 0;
				mAgeYears.setText(String.valueOf(age));

				mAgeMonths.setText(String.valueOf(months));
			}

		}
	};

	/*
	 * @author Team A, Eman K
	 * 
	 * @see android.app.Activity#onCreateDialog(int) Method automatically gets
	 * Called when you call showDialog() method
	 */
	@Override
	protected Dialog onCreateDialog(int id) {

		return new DatePickerDialog(this, mDateSetListener, mYear, mMonth, mDay);

	}

	/**
	 * For the Next Button: set listener on this object to response user action.
	 * This function will record the current patient information into
	 * ApplicationController and also send it to database.
	 **/
	class AddButtonListener implements View.OnClickListener {
		PatientInfoFormActivity PF;

		public AddButtonListener(PatientInfoFormActivity patientInfoFormActivity) {
			PF = patientInfoFormActivity;

		}

		@Override
		public void onClick(View v) {

			/*
			 * Collect Patient information from the form and call insert
			 * function.
			 */
			Toast toast = null;
			if (!app.isMyServiceRunning()) {
				toast = Toast.makeText(PatientInfoFormActivity.this, R.string.service_is_running, Toast.LENGTH_LONG);
				toast.getView().setBackgroundColor(Color.YELLOW);
				toast.show();
			} else {

				if (collectPatientInfo()) {
					/*
					 * Toast to show the
					 * message"patient information has been saved correctly"
					 */
					Toast.makeText(PatientInfoFormActivity.this, R.string.save_toast, Toast.LENGTH_SHORT).show();
					/*
					 * Go to the next screen
					 */
					if (app.isEnd()) {
						history.insert(app, app.getHistory());
						Toast.makeText(PatientInfoFormActivity.this, R.string.save_history_toast, Toast.LENGTH_SHORT)
								.show();
						Intent to_home = new Intent(PatientInfoFormActivity.this, HomeActivity.class);
						startActivity(to_home);
					} else {
						if (Integer.valueOf(mAgeYears.getText().toString()) > 5) {
							new AlertDialog.Builder(PF)
									.setIcon(android.R.drawable.ic_dialog_alert)
									.setTitle("Patient Age")
									.setMessage(
											"Sorry, this application can only diagnose children up to 5 years of age. Do you want to skip the diagnosis process?")
									.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

										@Override
										public void onClick(DialogInterface dialog, int id) {
											Intent intent = new Intent(PatientInfoFormActivity.this, HomeActivity.class);
											intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
											startActivity(intent);

										}

									}).setNegativeButton("No", null).show();
						} else {
							Intent to_temp = new Intent(PatientInfoFormActivity.this, PTempPAgeActivity.class);
							startActivity(to_temp);

						}
					}
				} else {
					/*
					 * Toast to show the message"patient information need to be
					 * completed
					 */
					Toast t = Toast.makeText(PatientInfoFormActivity.this, R.string.patienterorr, Toast.LENGTH_SHORT);
					t.getView().setBackgroundColor(Color.RED);
					t.show();

				}
			}
		}
	}

	/**
	 * @author Team A, Eman K
	 * @return true if the patient insert correctly, false if something not
	 *         complete. This function responsible for collect data from form
	 *         and record it into ApplicationController Then send this object to
	 *         data base to be inserted
	 */
	@SuppressLint("ResourceAsColor")
	public boolean collectPatientInfo() {

		if (Pid.length() == 0)
			patient.setP_ID(mFNameEditText.getText().toString() + mPhoneEditText.getText().toString());
		else
			patient.setP_ID(Pid);

		/*
		 * Get first and last names (required)
		 */

		if (mFNameEditText.getText().length() == 0) {
			mFNameEditText.setError("Please enter First Name ");
			return false;
		} else {
			patient.setF_name(mFNameEditText.getText().toString());

		}
		if (mLNameEditText.getText().length() == 0) {
			mLNameEditText.setError("Please enter Last Name ");
			return false;
		} else {
			patient.setL_name(mLNameEditText.getText().toString());

		}
		/*
		 * Get phone number (required)
		 */

		if (mPhoneEditText.getText().length() == 0) {
			mPhoneEditText.setError("Please enter Phone Number ");
			return false;
		} else {
			patient.setPhone(mPhoneEditText.getText().toString());

		}

		/*
		 * Get date of birth (required)
		 */
		if (app.getpAge_years().length() == 0) {
			if ((mDateOfbirthEditText.getText().length() == 0 && mAgeYears.getText().length() == 0)) {
				mDateOfbirthEditText.setError("Please enter date of birth ");
				return false;
			}
		}
		patient.setAge(mAgeYears.getText().toString() + " years " + mAgeMonths.getText().toString() + " months ");
		patient.setDate_of_birth(mDateOfbirthEditText.getText().toString());

		/*
		 * Get address and Age
		 */
		patient.setAddress(mAddress.getText().toString());

		/*
		 * Get sex
		 */

		// find the mRadioSex by returned id

		if (mRadioSexGroup.getCheckedRadioButtonId() == -1) {
			mRadioSexGroup.setBackgroundColor(R.color.imci_plus);
			mRadioSexGroup.setFocusable(true);
			// Toast.makeText(PatientInfoFormActivity.this,
			// "Please specify patient gender", Toast.LENGTH_SHORT).show();

			return false;
		} else {
			mRadioSex = (RadioButton) findViewById(mRadioSexGroup.getCheckedRadioButtonId());
			patient.setSex(mRadioSex.getText().toString());
		}
		/*
		 * get the current date
		 */
		patient.setLatst_update(mDay + "-" + mMonth + "-" + mYear);

		if (!patient.insertPatient(app, patient)) {
			return false;
		}

		app.setRecording(true);
		return true;

	}

	private int group1Id = 1;

	int exit = 1;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		menu.add(group1Id, exit, exit, "").setTitle("Exit");

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case 1:
			Intent exit = new Intent(this, HomeActivity.class);
			exit.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			exit.putExtra("EXIT", true);
			startActivity(exit);
			return true;

		default:
			break;

		}
		return super.onOptionsItemSelected(item);
	}

}
