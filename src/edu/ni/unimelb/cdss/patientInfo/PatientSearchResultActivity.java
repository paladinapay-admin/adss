package edu.ni.unimelb.cdss.patientInfo;

import java.util.ArrayList;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;
import edu.ni.unimelb.cdss.R;
import edu.ni.unimelb.cdss.control.ApplicationController;
import edu.ni.unimelb.cdss.control.HomeActivity;

/**
 * This Activity to show list of search results
 * 
 * @author Team A, Eman K
 * @version 0.3
 */
public class PatientSearchResultActivity extends ListActivity {
	private PatientInfo patient;
	private ArrayList<PatientInfo> results;
	private ApplicationController app;

	ImageButton mHomeButton;
	ImageButton mBackbutton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.patient_srearch_result_view);
		app = ((ApplicationController) getApplicationContext());

		results = new ArrayList<PatientInfo>();

		patient = app.getPatient();
		// From Database
		/*
		 * Changed by Patrick
		 */

		mHomeButton = (ImageButton) findViewById(R.id.homeButtonResult);
		mHomeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			// On click function
			public void onClick(View view) {
				// Create the intent to start another activity
				Intent intent = new Intent(view.getContext(), HomeActivity.class);
				startActivity(intent);
			}
		});

		mBackbutton = (ImageButton) findViewById(R.id.backButtonResult);
		mBackbutton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				finish();
			}
		});

		results = patient.retrievePatients(app, patient);

		PatientInfoListAdapter adapter = new PatientInfoListAdapter(this, R.layout.patient_raw_view, results);
		setListAdapter(adapter);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.ListActivity#onListItemClick(android.widget.ListView,
	 * android.view.View, int, long)
	 */
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		PatientInfo item = (PatientInfo) getListAdapter().getItem(position);
		Toast.makeText(this, item.getF_name() + "  " + item.getL_name() + " \nselected", Toast.LENGTH_LONG).show();
		app.setPatient(item);
		Intent i = new Intent(this, PatientInfoPreviewActivity.class);
		startActivity(i);
	}

	private int group1Id = 1;

	int exit = 1;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		menu.add(group1Id, exit, exit, "").setTitle("Exit");

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case 1:
			Intent exit = new Intent(this, HomeActivity.class);
			exit.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			exit.putExtra("EXIT", true);
			startActivity(exit);
			return true;

		default:
			break;

		}
		return super.onOptionsItemSelected(item);
	}

}
