package edu.ni.unimelb.cdss.patientInfo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import edu.ni.unimelb.cdss.R;
import edu.ni.unimelb.cdss.control.ApplicationController;
import edu.ni.unimelb.cdss.control.HomeActivity;
import edu.ni.unimelb.cdss.diagnosis.DiagnosisHistory;

/**
 * 
 * This class represent the Patient Information form for viewing the patient.
 * 
 * @author Team A, Eman K
 * @version 0.3
 */
public class PatientInfoPreviewActivity extends Activity {

	/**
	 * These PatientInfo details form Text view.
	 */
	private PatientInfo patient;
	private TextView mFNameText;
	private TextView mLNameText;
	private TextView mPhoneText;
	private TextView mDate;
	private TextView mAge;
	private TextView mAddress;
	private TextView mSex;

	private Button mConfirmButton;
	private Button mCancelButton;
	private Button mEditButton;
	private DiagnosisHistory history;

	ImageButton mHomeButton;
	ImageButton mBackbutton;
	private int age;
	private ApplicationController app;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.patient_info_preview_view);

		// get the context of application .
		app = ((ApplicationController) getApplicationContext());

		patient = app.getPatient();
		history = app.getHistory();
		/*
		 * For all "Buttons" "TextView": get reference to inflated View object
		 * set listener on these objects to response user action and get
		 * information from them.
		 */
		mConfirmButton = (Button) findViewById(R.id.confirm_button);
		mConfirmButton.setOnClickListener(new ConfirmButtonListener(this));

		mCancelButton = (Button) findViewById(R.id.cancel_button);
		mCancelButton.setOnClickListener(new CancelButtonListener());

		mEditButton = (Button) findViewById(R.id.editBT);
		mEditButton.setOnClickListener(new EditButtonListener());

		mFNameText = (TextView) findViewById(R.id.edit_fname1);
		mFNameText.setText(patient.getF_name());
		mLNameText = (TextView) findViewById(R.id.edit_lname1);
		mLNameText.setText(patient.getL_name());
		mPhoneText = (TextView) findViewById(R.id.phone1);
		mPhoneText.setText(patient.getPhone());
		mDate = (TextView) findViewById(R.id.DOB);
		mDate.setText(patient.getDate_of_birth());
		mAge = (TextView) findViewById(R.id.age1);
		mAge.setText(patient.getAge());
		String[] token = patient.getAge().split(" ");
		age = Integer.valueOf(token[0]);
		mAddress = (TextView) findViewById(R.id.addressEditText1);
		mAddress.setText(patient.getAddress());
		mSex = (TextView) findViewById(R.id.sexField);
		mSex.setText(patient.getSex());

		mHomeButton = (ImageButton) findViewById(R.id.homeButtonPrev);
		mHomeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			// On click function
			public void onClick(View view) {
				// Create the intent to start another activity
				Intent intent = new Intent(view.getContext(), HomeActivity.class);
				startActivity(intent);
			}
		});

		mBackbutton = (ImageButton) findViewById(R.id.backButtonPrev);
		mBackbutton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				finish();
			}
		});

	}

	class CancelButtonListener implements View.OnClickListener {

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View v) {
			Intent toForm = new Intent(PatientInfoPreviewActivity.this, PatientInfoFormActivity.class);
			startActivity(toForm);
		}

	}

	class EditButtonListener implements View.OnClickListener {

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View v) {
			Intent toUpdate = new Intent(PatientInfoPreviewActivity.this, PatientUpdateActivity.class);
			startActivity(toUpdate);
		}

	}

	class ConfirmButtonListener implements View.OnClickListener {
		PatientInfoPreviewActivity PP;

		public ConfirmButtonListener(PatientInfoPreviewActivity patientInfoPreviewActivity) {
			PP = patientInfoPreviewActivity;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View v) {
			app.setRecording(true);

			if (app.isEnd()) {
				history.insert(app, app.getHistory());
				Toast.makeText(PatientInfoPreviewActivity.this, R.string.save_history_toast, Toast.LENGTH_SHORT).show();
				Intent to_home = new Intent(PatientInfoPreviewActivity.this, HomeActivity.class);
				startActivity(to_home);
			} else {
				if (age > 5) {
					new AlertDialog.Builder(PP)
							.setIcon(android.R.drawable.ic_dialog_alert)
							.setTitle("Patient Age")
							.setMessage(
									"Sorry, this application can only diagnose children up to 5 years of age. Do you want to skip the diagnosis process?")
							.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int id) {
									Intent intent = new Intent(PatientInfoPreviewActivity.this, HomeActivity.class);
									intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
									startActivity(intent);

								}

							}).setNegativeButton("No", null).show();
				} else {
					Intent to_temp = new Intent(PatientInfoPreviewActivity.this, PTempPAgeActivity.class);
					startActivity(to_temp);
				}

			}

		}
	}

	private int group1Id = 1;

	int exit = 1;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		menu.add(group1Id, exit, exit, "").setTitle("Exit");

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case 1:
			Intent exit = new Intent(this, HomeActivity.class);
			exit.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			exit.putExtra("EXIT", true);
			startActivity(exit);
			return true;

		default:
			break;

		}
		return super.onOptionsItemSelected(item);
	}

}
