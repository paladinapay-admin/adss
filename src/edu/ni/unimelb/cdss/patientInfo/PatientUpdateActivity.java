package edu.ni.unimelb.cdss.patientInfo;

import java.util.Calendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import edu.ni.unimelb.cdss.R;
import edu.ni.unimelb.cdss.control.ApplicationController;
import edu.ni.unimelb.cdss.control.HomeActivity;
import edu.ni.unimelb.cdss.diagnosis.DiagnosisHistory;

public class PatientUpdateActivity extends Activity {

	private PatientInfo patient;
	private EditText mFNameText;
	private EditText mLNameText;
	private EditText mPhoneText;
	private TextView mDate;
	private EditText mAge;
	private EditText mAddress;
	private RadioButton mMale;
	private RadioButton mFemale;
	private RadioGroup mRadioSexGroup;
	private RadioButton mRadioSex;
	private ImageButton mDateOfBirthImageButton;
	private DiagnosisHistory history;
	private EditText mAgeMonths;
	ImageButton mHomeButton;
	ImageButton mBackbutton;

	private Button mUpdateButton;
	/*
	 * To get the date and the current date from Calendar object
	 */
	private int birth_year, birth_month, birth_day, age;
	private int mYear, mMonth, mDay;
	static final int DATE_DIALOG_ID = 0;

	private ApplicationController app;

	/*
	 * PatientInformationActivity constructor to initialize the date.
	 */
	public PatientUpdateActivity() {
		// Assign current Date
		final Calendar c = Calendar.getInstance();
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.patient_update_view);

		app = ((ApplicationController) getApplicationContext());
		patient = app.getPatient();
		history = app.getHistory();
		mUpdateButton = (Button) findViewById(R.id.update_button);
		mUpdateButton.setOnClickListener(new UpdateButtonListener(this));

		mFNameText = (EditText) findViewById(R.id.edit_fname2);
		mFNameText.setText(patient.getF_name());
		mLNameText = (EditText) findViewById(R.id.edit_lname2);
		mLNameText.setText(patient.getL_name());
		mPhoneText = (EditText) findViewById(R.id.phone2);
		mPhoneText.setText(patient.getPhone());
		mDateOfBirthImageButton = (ImageButton) findViewById(R.id.date_of_birth_button_update);
		mDate = (TextView) findViewById(R.id.birth_date_update);
		mDate.setText(patient.getDate_of_birth());
		mAge = (EditText) findViewById(R.id.patientAge_update);
		mAgeMonths = (EditText) findViewById(R.id.editTextAgeMonths_update);
		mAgeMonths.setFilters(new InputFilter[] { new InputFilterMinMax("0", "11") });
		String[] token = patient.getAge().split(" ");
		mAge.setText(token[0]);
		mAgeMonths.setText(token[2]);
		mAddress = (EditText) findViewById(R.id.addressEditText2);
		mAddress.setText(patient.getAddress());
		mRadioSexGroup = (RadioGroup) findViewById(R.id.sexGroup2);
		mMale = (RadioButton) findViewById(R.id.male2);
		mFemale = (RadioButton) findViewById(R.id.female2);
		if (patient.getSex() == "Male") {
			mRadioSexGroup.check(mMale.getId());
		} else {
			mRadioSexGroup.check(mFemale.getId());
		}
		/*
		 * Show the DatePickerDialog
		 */
		mDateOfBirthImageButton.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				// Show the DatePickerDialog
				showDialog(DATE_DIALOG_ID);
			}
		});

		mHomeButton = (ImageButton) findViewById(R.id.homeButtonUpdate);
		mHomeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			// On click function
			public void onClick(View view) {
				// Create the intent to start another activity
				Intent intent = new Intent(view.getContext(), HomeActivity.class);
				startActivity(intent);
			}
		});

		mBackbutton = (ImageButton) findViewById(R.id.backButtonUpdate);
		mBackbutton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				finish();
			}
		});

	}

	/*
	 * Register DatePickerDialog listener
	 */
	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
		// the callback received when the user "sets" the Date in the
		// DatePickerDialog
		public void onDateSet(DatePicker view, int yearSelected, int monthOfYear, int dayOfMonth) {
			birth_year = yearSelected;
			birth_month = monthOfYear + 1;
			birth_day = dayOfMonth;
			int currenMonth = mMonth + 1;
			// Set the Selected Date in Select date EditText
			if (mYear < birth_year || (mYear == birth_year && currenMonth < birth_month)) {
				Toast.makeText(PatientUpdateActivity.this, "The Data Of birth is invalid", Toast.LENGTH_SHORT).show();
			} else {

				mDate.setText(birth_day + " - " + birth_month + " - " + birth_year);
				age = 0;
				int day = 0;
				age = mYear - birth_year;
				int months = currenMonth - birth_month;
				// if month difference is in negative then reduce years by one
				// and
				// calculate the number of months.
				if (months < 0) {
					age--;
					months = 12 - birth_month + currenMonth;

					if (mDay < birth_day)
						months--;

				} else if (months == 0 && mDay < birth_day) {
					age--;
					months = 11;
				}
				day = mDay - birth_day;

				if (mDay == birth_day) {
					day = 0;
					if (months == 12) {
						age++;
						months = 0;
					}
				}
				if (age < 0)
					age = 0;
				mAge.setText(String.valueOf(age));

				mAgeMonths.setText(String.valueOf(months));
			}

		}
	};

	/*
	 * @author Team A, Eman K
	 * 
	 * @see android.app.Activity#onCreateDialog(int) Method automatically gets
	 * Called when you call showDialog() method
	 */
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			// create a new DatePickerDialog with values you want to show
			return new DatePickerDialog(this, mDateSetListener, mYear, mMonth, mDay);
		}
		return null;
	}

	class UpdateButtonListener implements View.OnClickListener {
		PatientUpdateActivity PU;

		public UpdateButtonListener(PatientUpdateActivity patientUpdateActivity) {
			PU = patientUpdateActivity;
		}

		@Override
		public void onClick(View v) {

			/*
			 * Collect Patient information from the form and call insert
			 * function.
			 */
			Toast toast = null;
			if (!app.isMyServiceRunning()) {
				toast = Toast.makeText(PatientUpdateActivity.this, R.string.service_is_running, Toast.LENGTH_LONG);
				toast.getView().setBackgroundColor(Color.YELLOW);
				toast.show();
			} else {
				if (updatePateintInfo()) {
					/*
					 * Toast to show the
					 * message"patient information has been updated correctly"
					 */
					Toast.makeText(PatientUpdateActivity.this, R.string.update_toast, Toast.LENGTH_SHORT).show();
					/*
					 * Go to the next screen
					 */
					if (app.isEnd()) {
						history.insert(app, app.getHistory());
						Toast.makeText(PatientUpdateActivity.this, R.string.save_history_toast, Toast.LENGTH_SHORT)
								.show();
						Intent to_home = new Intent(PatientUpdateActivity.this, HomeActivity.class);
						startActivity(to_home);

					} else {

						if (Integer.valueOf(mAge.getText().toString()) > 5) {
							new AlertDialog.Builder(PU)
									.setIcon(android.R.drawable.ic_dialog_alert)
									.setTitle("Patient Age")
									.setMessage(
											"Sorry, this application can only diagnose children up to 5 years of age. Do you want to skip the diagnosis process?")
									.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

										@Override
										public void onClick(DialogInterface dialog, int id) {
											Intent intent = new Intent(PatientUpdateActivity.this, HomeActivity.class);
											intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
											startActivity(intent);

										}

									}).setNegativeButton("No", null).show();
						} else {

							Intent to_temp = new Intent(PatientUpdateActivity.this, PTempPAgeActivity.class);
							startActivity(to_temp);

						}

					}
				} else {
					/*
					 * Toast to show the message"patient information need to be
					 * completed
					 */
					Toast t = Toast.makeText(PatientUpdateActivity.this, R.string.patienterorr, Toast.LENGTH_SHORT);
					t.getView().setBackgroundColor(Color.RED);
					t.show();
				}
			}
		}
	}

	public boolean updatePateintInfo() {

		/*
		 * Get first and last names (required)
		 */

		if (mFNameText.getText().length() == 0) {
			mFNameText.setError("Please enter First Name ");
			return false;
		} else {
			patient.setF_name(mFNameText.getText().toString());

		}
		if (mLNameText.getText().length() == 0) {
			mLNameText.setError("Please enter Last Name ");
			return false;
		} else {
			patient.setL_name(mLNameText.getText().toString());

		}
		/*
		 * Get phone number (required)
		 */

		if (mPhoneText.getText().length() == 0) {
			mPhoneText.setError("Please enter Phone Number ");
			return false;
		} else {
			patient.setPhone(mPhoneText.getText().toString());

		}

		/*
		 * Get date of birth (required)
		 */

		if (mDate.getText().length() == 0) {
			mDate.setError("Please enter date of birth ");
			return false;
		} else {
			patient.setDate_of_birth(mDate.getText().toString());
		}
		/*
		 * Get address and Age
		 */
		patient.setAddress(mAddress.getText().toString());

		patient.setAge(mAge.getText().toString() + " years " + mAgeMonths.getText().toString() + " months ");
		/*
		 * Get sex
		 */

		// find the mRadioSex by returned id

		mRadioSex = (RadioButton) findViewById(mRadioSexGroup.getCheckedRadioButtonId());
		patient.setSex(mRadioSex.getText().toString());
		/*
		 * get the current date
		 */
		patient.setLatst_update(mDay + "/" + mMonth + "/" + mYear);

		if (!patient.updatePatient(app, patient))
			return false;
		app.setRecording(true);
		return true;

	}

	// }

	private int group1Id = 1;

	int exit = 1;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		menu.add(group1Id, exit, exit, "").setTitle("Exit");

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case 1:
			Intent exit = new Intent(this, HomeActivity.class);
			exit.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			exit.putExtra("EXIT", true);
			startActivity(exit);
			return true;

		default:
			break;

		}
		return super.onOptionsItemSelected(item);
	}

}
