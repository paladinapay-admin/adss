package edu.ni.unimelb.cdss.control;

import java.util.Calendar;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import edu.ni.unimelb.cdss.R;
import edu.ni.unimelb.cdss.allData.AllDataActivity;
import edu.ni.unimelb.cdss.patientInfo.PTempPAgeActivity;
import edu.ni.unimelb.cdss.qrcode.CameraActivity;
import edu.ni.unimelb.cdss.synchronization.SynchronizationService;

/**
 * this activity represent the home screen of app which has only one button to
 * start the app procesure
 * 
 * @version 0.3
 * @author Team A, Eman K
 */
public class HomeActivity extends Activity {
	private int syncFreq = 600;

	private Button mbtnStartRecording;
	private Button mbtnStartDiag;
	// private Button mbtnAllData;

	private ApplicationController app;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		// check if user wants to exit
		if (getIntent().getBooleanExtra("EXIT", false)) {
			finish();
		}

		super.onCreate(savedInstanceState);
		setContentView(R.layout.home_view);
		app = (ApplicationController) getApplicationContext();

		mbtnStartDiag = (Button) findViewById(R.id.btnStartDia);
		mbtnStartDiag.setOnClickListener(new OnClickListener() {
			/**
			 * When the start button is clicked, move to the patient screen.
			 */
			public void onClick(View v) {

				// initialize patient every time for the new patient
				app.initPatientInfo();
				app.initPatientPreInfo();
				app.initPatientHisroty();
				app.setpAge_years("");
				app.setpAge_months("");
				app.setpDateOfBirth("");
				// set recording to flase
				app.setRecording(false);
				Intent to_diagnosis = new Intent(HomeActivity.this, PTempPAgeActivity.class);
				startActivity(to_diagnosis);
			}
		});

		mbtnStartRecording = (Button) findViewById(R.id.btnStartPatientInfo);
		mbtnStartRecording.setOnClickListener(new OnClickListener() {
			/**
			 * When the start button is clicked, move to the patient screen.
			 */
			public void onClick(View v) {

				// initialize patient every time for the new patient
				app.initPatientInfo();
				app.initPatientPreInfo();
				app.initPatientHisroty();
				app.setpAge_years("");
				app.setpAge_months("");
				app.setpDateOfBirth("");
				app.setEnd(false);
				Intent to_patientRecord = new Intent(HomeActivity.this, CameraActivity.class);
				startActivity(to_patientRecord);
			}
		});

		// BluetoothAdapter mBluetoothAdapter =
		// BluetoothAdapter.getDefaultAdapter();
		// if (!mBluetoothAdapter.isEnabled()) {
		// Intent enableBtIntent = new
		// Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
		// startActivityForResult(enableBtIntent, 1);

		if (app.isMyServiceRunning() == false) {
			// Set timer of data sync, and restarts service frequently
			Calendar cal = Calendar.getInstance();

			Intent intent = new Intent(this, SynchronizationService.class);
			PendingIntent pintent = PendingIntent.getService(this, 0, intent, 0);

			AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

			alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), syncFreq * 1000, pintent);

		}
	}

	@Override
	protected void onResume() {
		super.onResume();

	}

	private int group1Id = 1;

	int all_data = Menu.FIRST;
	int send_apk = 2;
	int exit = 3;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		menu.add(group1Id, all_data, all_data, "").setTitle("All Patients Information");
		menu.add(group1Id, send_apk, send_apk, "").setTitle("Send APK");
		menu.add(group1Id, exit, exit, "").setTitle("Exit");

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case 1:
			app.initPatientInfo();
			app.setpAge_years("");
			app.setpAge_months("");
			app.setpDateOfBirth("");
			// set recording to flase
			app.setRecording(false);
			Toast msg = Toast.makeText(HomeActivity.this, "Show All Data", Toast.LENGTH_LONG);
			msg.show();
			Intent to_data = new Intent(this, AllDataActivity.class);
			startActivity(to_data);
			return true;
		case 2:
			sendApk();
			return true;
		case 3:
			Intent exit = new Intent(this, HomeActivity.class);
			exit.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			exit.putExtra("EXIT", true);
			startActivity(exit);
			return true;

		default:
			break;

		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * This method creates a new activity to send local Apk file to a target
	 * bluetooth device
	 * 
	 * @author Hanlin Chen
	 */
	public void sendApk() {
		// file path to send
		String filePath = "/data/app/edu.ni.unimelb.cdss-1.apk";

		PackageManager pm = getPackageManager();

		// Get the real file path
		for (ApplicationInfo app : pm.getInstalledApplications(0)) {
			if (app.packageName.equals("edu.ni.unimelb.cdss")) {
				filePath = app.sourceDir;
			}
		}

		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("application/vnd.android.package-archive");
		i.setPackage("com.android.bluetooth");
		i.putExtra(Intent.EXTRA_STREAM, Uri.parse(filePath));

		startActivity(Intent.createChooser(i, "Send Apk via"));

	}

}
