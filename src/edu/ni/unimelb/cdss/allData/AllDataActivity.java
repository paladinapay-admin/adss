package edu.ni.unimelb.cdss.allData;

import java.util.ArrayList;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;
import edu.ni.unimelb.cdss.R;
import edu.ni.unimelb.cdss.control.ApplicationController;
import edu.ni.unimelb.cdss.control.HomeActivity;
import edu.ni.unimelb.cdss.patientInfo.PatientInfo;
import edu.ni.unimelb.cdss.patientInfo.PatientInfoListAdapter;
import edu.ni.unimelb.cdss.patientInfo.PatientInfoPreviewActivity;

/**
 * @author: Eman Bin Khunayn Date: 24/09/13 Time: 9:23 PM
 */
public class AllDataActivity extends ListActivity {

	private PatientInfo patient;
	private ArrayList<PatientInfo> results;
	private ApplicationController app;
	ImageButton homebutton;
	ImageButton backbutton;

	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.all_data_view);
		app = ((ApplicationController) getApplicationContext());
		patient = app.getPatient();
		results = new ArrayList<PatientInfo>();
		results = patient.retrieveAllPatients(app);
		PatientInfoListAdapter adapter = new PatientInfoListAdapter(this, R.layout.patient_raw_view, results);
		setListAdapter(adapter);

		// Home button and back button
		// Added by Patrick
		// This method is applied in every activity by copying and pasting the
		// code
		homebutton = (ImageButton) findViewById(R.id.homeButtonAlldata);
		homebutton.setOnClickListener(new OnClickListener() {
			@Override
			// On click function
			public void onClick(View view) {
				// Create the intent to start another activity
				Intent intent = new Intent(view.getContext(), HomeActivity.class);
				startActivity(intent);
			}
		});

		backbutton = (ImageButton) findViewById(R.id.backButtonAlldata);
		backbutton.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				finish();
			}
		});

		// End by Patrick

	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		PatientInfo item = (PatientInfo) getListAdapter().getItem(position);
		Toast.makeText(this, item.getF_name() + "  " + item.getL_name() + " \nselected", Toast.LENGTH_LONG).show();
		app.setPatient(item);
		Intent i = new Intent(this, PatientInfoPreviewActivity.class);
		startActivity(i);
	}

	private int group1Id = 1;

	int exit = 1;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		menu.add(group1Id, exit, exit, "").setTitle("Exit");

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case 1:
			Intent exit = new Intent(this, HomeActivity.class);
			exit.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			exit.putExtra("EXIT", true);
			startActivity(exit);
			return true;

		default:
			break;

		}
		return super.onOptionsItemSelected(item);
	}

}