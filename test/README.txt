# Automated CDSS Test Script

# Tools required:
## Ant
## Android Tools
## Robotium

# Go to your command line interface, browse to project

######### 
# For running Robotium tests in an emulator: 
## android list avd
## emulator -avd <device name>

# Wait for it to fully load.

# Browse to CDSS-Test directory <CDSS\test>
## ant clean debug

# Build Successful should appear at the end.

# Install it into the device
## ant clean debug install test

##########
# For running Robotium tests in a connected phone:
# http://controlingquality.blogspot.com.au/2013/05/running-android-test-project-using-ant.html

# Install the APK using Eclipse

# Go to your command line interface
# Browse to CDSS-Test directory <CDSS\test>
## ant clean debug

# Build Successful should appear at the end.

# Install it into the device
## ant clean debug install test

# Installing it using Eclipse is important in order for them to share the same
# signature. A custom keystore can be used if preferred.