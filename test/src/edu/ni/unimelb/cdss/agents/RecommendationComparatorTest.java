/**
 * @author Yossef Mostafa
 * @version 0.1
 * An automated test script to test the recommendation comparator as found 
 * in RecommendationComparator.java. This script tests all the 8 (2^3) 
 * combinations of inputs to the compare() method in the recommendation comparator. 
 * 
 */
package edu.ni.unimelb.cdss.agents;

// imports
import junit.framework.TestCase;

import org.junit.BeforeClass;
import org.junit.Test;

import edu.ni.unimelb.cdss.diagnosis.recommendation.Recommendation;
import edu.ni.unimelb.cdss.diagnosis.recommendation.Recommendation.Urgency;
import edu.ni.unimelb.cdss.diagnosis.recommendation.RecommendationComparator;
import edu.ni.unimelb.cdss.diagnosis.recommendation.UnknownRecommendation;

public class RecommendationComparatorTest extends TestCase {

	// Setting up variables
	RecommendationComparator rc;
	Recommendation rec1;
	Recommendation rec2;

	// Instantiating variables before test cases are executed
	@BeforeClass
	public void setUp() throws Exception {
		System.out.println("SETTING UP TEST CASES FOR AGENT RECOMMENDATION"
				+ " COMPARATOR....");
		rc = new RecommendationComparator();
		rec1 = new UnknownRecommendation("R1");
		rec2 = new UnknownRecommendation("R2");

	}

	/**********************************************************
	 * Test cases. The test cases are named as follows: test(Recommendation1
	 * urgency)(Recommendation2 urgency) So, testHighHigh tests when rec1 is
	 * HIGH and rec 2 is also HIGH.
	 * 
	 * Output of compare(): -1 means that rec1 > rec2 1 means that rec1 < rec2
	 **********************************************************/

	@Test
	public void testHighHigh() {
		rec1.setUrgency(Urgency.HIGH);
		rec2.setUrgency(Urgency.HIGH);
		assertTrue(rc.compare(rec1, rec2) == -1);
	}

	@Test
	public void testHighMed() {
		rec1.setUrgency(Urgency.HIGH);
		rec2.setUrgency(Urgency.MEDIUM);
		assertTrue(rc.compare(rec1, rec2) == -1);
	}

	@Test
	public void testHighLow() {
		rec1.setUrgency(Urgency.HIGH);
		rec2.setUrgency(Urgency.LOW);
		assertTrue(rc.compare(rec1, rec2) == -1);
	}

	@Test
	public void testMedHigh() {
		rec1.setUrgency(Urgency.MEDIUM);
		rec2.setUrgency(Urgency.HIGH);
		assertTrue(rc.compare(rec1, rec2) == 1);
	}

	@Test
	public void testMedMed() {
		rec1.setUrgency(Urgency.MEDIUM);
		rec2.setUrgency(Urgency.MEDIUM);
		assertTrue(rc.compare(rec1, rec2) == -1);
	}

	@Test
	public void testMedLow() {
		rec1.setUrgency(Urgency.MEDIUM);
		rec2.setUrgency(Urgency.LOW);
		assertTrue(rc.compare(rec1, rec2) == -1);
	}

	@Test
	public void testLowHigh() {
		rec1.setUrgency(Urgency.LOW);
		rec2.setUrgency(Urgency.HIGH);
		assertTrue(rc.compare(rec1, rec2) == 1);
	}

	@Test
	public void testLowMed() {
		rec1.setUrgency(Urgency.LOW);
		rec2.setUrgency(Urgency.MEDIUM);
		assertTrue(rc.compare(rec1, rec2) == 1);
	}

	@Test
	public void testLowLow() {
		rec1.setUrgency(Urgency.LOW);
		rec2.setUrgency(Urgency.LOW);
		assertTrue(rc.compare(rec1, rec2) == -1);
	}

}
