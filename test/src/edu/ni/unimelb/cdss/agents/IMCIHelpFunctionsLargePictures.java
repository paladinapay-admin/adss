package edu.ni.unimelb.cdss.agents;

import android.app.Instrumentation;
import android.content.Context;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

import com.jayway.android.robotium.solo.Solo;

import edu.ni.unimelb.cdss.R;
import edu.ni.unimelb.cdss.control.ApplicationController;
import edu.ni.unimelb.cdss.diagnosis.DangerSignsActivity;
import edu.ni.unimelb.cdss.diagnosis.KeySymptomsActivity;
import edu.ni.unimelb.cdss.diagnosis.QuestionsActivity;
import edu.ni.unimelb.cdss.diagnosis.RecommendationsActivity;
import edu.ni.unimelb.cdss.diagnosis.RecommendationsDialog;
import edu.ni.unimelb.cdss.diagnosis.stc.Static;
import edu.ni.unimelb.cdss.patientInfo.PTempPAgeActivity;
import edu.ni.unimelb.cdss.patientInfo.PatientInfo;

/**
 * @author Erick Gaspar
 * @date 10/23/2013
 * @version 0.2
 * 
 *          Tests Help Function Images, loading large versions if necessary.
 *          Unreliable due to Robotium emulator randomly delaying responses.
 * 
 * 
 */
public class IMCIHelpFunctionsLargePictures extends ActivityInstrumentationTestCase2<PTempPAgeActivity> {

	private Solo solo;

	public IMCIHelpFunctionsLargePictures() {

		super(PTempPAgeActivity.class);
	}

	@Override
	public void setUp() throws Exception {
		// setUp() is run before a test case is started.
		// This is where the solo object is created.
		solo = new Solo(getInstrumentation(), getActivity());
	}

	@Override
	public void tearDown() throws Exception {
		// tearDown() is run after a test case has finished.
		// finishOpenedActivities() will finish all the activities that have
		// been opened during the test execution.

		solo.finishOpenedActivities();
	}

	public void testHelpFunctions() throws Exception {

		ApplicationController app;
		PatientInfo patient;
		Context context;

		Instrumentation instrumentation = getInstrumentation();
		context = instrumentation.getTargetContext();
		app = (ApplicationController) context.getApplicationContext();

		// set age
		solo.clickOnView(solo.getView(R.id.date_of_birth_button_ptemp_page));
		solo.setDatePicker(0, 2012, 7, 24);
		solo.clickOnText("Set");

		solo.clickOnButton("Next");

		solo.waitForActivity(DangerSignsActivity.class);

		// ///////////////////
		// Assert that TemperatureActivity -> DangerSignsActivity
		solo.assertCurrentActivity("Expected DangerSignsActivity", "DangerSignsActivity");

		solo.clickOnView(solo.getView(R.id.switchDangerDrink));
		solo.clickOnView(solo.getView(R.id.switchDangerVomit));
		solo.clickOnView(solo.getView(R.id.switchDangerConvulsed));
		solo.clickOnView(solo.getView(R.id.switchDangerLethUncon));
		solo.clickOnView(solo.getView(R.id.switchDangerConvulsing));

		solo.clickOnButton("Next");

		solo.waitForActivity(KeySymptomsActivity.class);

		// ///////////////////
		// Assert that DangerSignsActivity -> KeySymptomsActivity
		solo.assertCurrentActivity("Expected KeySymptomsActivity", "KeySymptomsActivity");

		solo.clickOnView(solo.getView(R.id.switchKeyCough));
		solo.clickOnView(solo.getView(R.id.switchKeyBreath));
		solo.clickOnView(solo.getView(R.id.switchKeyDiarrhoea));
		solo.clickOnView(solo.getView(R.id.switchKeyFever));
		solo.clickOnView(solo.getView(R.id.switchKeyEar));

		solo.clickOnButton("Next");

		// Random random = new Random();

		// random.setSeed((new Date()).getTime());
		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.waitForActivity(RecommendationsDialog.class);

		solo.assertCurrentActivity("Expected RecommendationsDialog", "RecommendationsDialog");

		solo.clickOnButton("Continue");

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.waitForActivity(RecommendationsDialog.class);

		solo.assertCurrentActivity("Expected RecommendationsDialog", "RecommendationsDialog");

		solo.clickOnButton("Continue");

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.waitForActivity(RecommendationsDialog.class);

		solo.assertCurrentActivity("Expected RecommendationsDialog", "RecommendationsDialog");

		solo.clickOnButton("Continue");

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.waitForActivity(RecommendationsDialog.class);

		solo.assertCurrentActivity("Expected RecommendationsDialog", "RecommendationsDialog");

		solo.clickOnButton("Continue");

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.waitForActivity(RecommendationsDialog.class);

		solo.assertCurrentActivity("Expected RecommendationsDialog", "RecommendationsDialog");

		solo.clickOnButton("Continue");

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.waitForActivity(RecommendationsDialog.class);

		// 2
		solo.assertCurrentActivity("Expected RecommendationsDialog", "RecommendationsDialog");

		solo.clickOnButton("Continue");

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.waitForActivity(RecommendationsDialog.class);

		solo.assertCurrentActivity("Expected RecommendationsDialog", "RecommendationsDialog");

		solo.clickOnButton("Continue");

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(RecommendationsActivity.class);
		// ////////////////////
		// Assert that QuestionsActivity -> RecommendationsActivity
		solo.assertCurrentActivity("Expected RecommendationsActivity", "RecommendationsActivity");

		solo.goBack();

		// ///////////////////
		// Assert that RecommendationsActivity -> PTempPAgeActivity
		solo.assertCurrentActivity("Expected PTempPAgeActivity", "PTempPAgeActivity");

		// Repeat for No Key Symptoms and Danger Signs
		solo.clickOnButton("Next");

		solo.waitForActivity(DangerSignsActivity.class);

		// ///////////////////
		// Assert that TemperatureActivity -> DangerSignsActivity
		solo.assertCurrentActivity("Expected DangerSignsActivity", "DangerSignsActivity");

		solo.clickOnButton("Next");

		solo.waitForActivity(KeySymptomsActivity.class);

		// ///////////////////
		// Assert that DangerSignsActivity -> KeySymptomsActivity
		solo.assertCurrentActivity("Expected KeySymptomsActivity", "KeySymptomsActivity");

		solo.clickOnView(solo.getView(R.id.switchKeyCough));
		solo.clickOnView(solo.getView(R.id.switchKeyBreath));
		solo.clickOnView(solo.getView(R.id.switchKeyDiarrhoea));
		solo.clickOnView(solo.getView(R.id.switchKeyFever));
		solo.clickOnView(solo.getView(R.id.switchKeyEar));

		solo.clickOnButton("Next");

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnButton("Skip");

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnButton("Skip");

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnButton("Skip");

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnButton("Skip");

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnButton("Skip");

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnButton("Skip");

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnButton("Skip");

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnButton("Skip");

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnButton("Skip");

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnButton("Next");

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnButton("Skip");

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnButton("Skip");

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnButton("Skip");

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnButton("Skip");

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnButton("Skip");

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnButton("Skip");

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnButton("Skip");

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnButton("Skip");

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnButton("Skip");

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnButton("Skip");

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnButton("Skip");

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnButton("Skip");

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnButton("Skip");

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnButton("Skip");

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnButton("Skip");

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnButton("Skip");

		solo.waitForActivity(RecommendationsActivity.class);
		// ////////////////////
		// Assert that QuestionsActivity -> RecommendationsActivity
		solo.assertCurrentActivity("Expected RecommendationsActivity", "RecommendationsActivity");

		solo.goBack();

		// ///////////////////
		// Assert that RecommendationsActivity -> PTempPAgeActivity
		solo.assertCurrentActivity("Expected PTempPAgeActivity", "PTempPAgeActivity");

		// Repeat for No Key Symptoms and Danger Signs
		solo.clickOnButton("Next");

		solo.waitForActivity(DangerSignsActivity.class);

		// ///////////////////
		// Assert that TemperatureActivity -> DangerSignsActivity
		solo.assertCurrentActivity("Expected DangerSignsActivity", "DangerSignsActivity");

		solo.clickOnButton("Next");

		solo.waitForActivity(KeySymptomsActivity.class);

		// ///////////////////
		// Assert that DangerSignsActivity -> KeySymptomsActivity
		solo.assertCurrentActivity("Expected KeySymptomsActivity", "KeySymptomsActivity");

		solo.clickOnView(solo.getView(R.id.switchKeyCough));
		solo.clickOnView(solo.getView(R.id.switchKeyBreath));
		solo.clickOnView(solo.getView(R.id.switchKeyDiarrhoea));
		solo.clickOnView(solo.getView(R.id.switchKeyFever));
		solo.clickOnView(solo.getView(R.id.switchKeyEar));

		solo.clickOnButton("Next");

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnButton("Next");

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnImageButton(2);

		if (context.getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
				context.getPackageName()) != 0) {
			solo.clickOnView(solo.getView(R.id.helpImg));
			solo.clickOnView(solo.getView(R.id.closeBtn));
		}

		solo.clickOnView(solo.getView(R.id.closeBtn));
		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(RecommendationsActivity.class);
		// ////////////////////
		// Assert that QuestionsActivity -> RecommendationsActivity
		solo.assertCurrentActivity("Expected RecommendationsActivity", "RecommendationsActivity");

		solo.goBack();

		// ///////////////////
		// Assert that RecommendationsActivity -> PTempPAgeActivity
		solo.assertCurrentActivity("Expected PTempPAgeActivity", "PTempPAgeActivity");

		Log.d("ROBOTIUM", "Current activity at: " + solo.getCurrentActivity().toString());
	}

}
