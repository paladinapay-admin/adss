package edu.ni.unimelb.cdss.system;

import org.junit.Before;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.EditText;
import android.widget.RadioButton;

import com.jayway.android.robotium.solo.Solo;

import edu.ni.unimelb.cdss.R;
import edu.ni.unimelb.cdss.config.Config;
import edu.ni.unimelb.cdss.control.HomeActivity;
import edu.ni.unimelb.cdss.diagnosis.DangerSignsActivity;
import edu.ni.unimelb.cdss.diagnosis.KeySymptomsActivity;
import edu.ni.unimelb.cdss.diagnosis.QuestionsActivity;
import edu.ni.unimelb.cdss.diagnosis.RecommendationsActivity;
import edu.ni.unimelb.cdss.patientInfo.PTempPAgeActivity;
import edu.ni.unimelb.cdss.patientInfo.PatientInfoFormActivity;
import edu.ni.unimelb.cdss.patientInfo.PatientSearchActivity;
import edu.ni.unimelb.cdss.qrcode.CameraActivity;

/**
 * Made in Eclipse
 * 
 * @Created date 26-8-2013 @ time 2:02:42 a.m
 * @author Patrick Zhao
 * @version 0.1
 * @Logs: @ 26-8-2013 2:02:42 a.m -- V1 It is a sample system test for sub-team
 *        leads to follow the scenario
 * 
 *        @ 30/09/2013 3:05:21 p.m -- System Test scenarios for release 2,
 *        release 1 st still keep
 * 
 */
public class SystemTest extends ActivityInstrumentationTestCase2<HomeActivity> {

	private Solo solo;
	Config config = new Config();
	private int st = config.SLEEPDURATION;

	/**
	 * @Constructor
	 */
	public SystemTest() {
		super("edu.ni.unimelb.cdss.control", HomeActivity.class);
	}

	@Before
	protected void setUp() throws Exception {
		super.setUp();
		solo = new Solo(getInstrumentation(), getActivity());
	}

	/**
	 * This scenario test the all data button focusing on the reliability
	 * 
	 * @author Patrick
	 */
	public void testScenario1() {
		long startTime = System.nanoTime();

		solo.assertCurrentActivity("Check on CDSS entry", HomeActivity.class);
		solo.sleep(config.SLEEPDURATION);

		config.successTestLog("Entering Home Page round 1");

		// for (int i = 2; i < 3; i++) {
		// config.successTestLog("  Entering Home Page round  " + i);

		solo.pressMenuItem(0);
		config.successTestLog("Expand all data");

		solo.goBack();

		// }

		long endTime = System.nanoTime();
		long duration = endTime - startTime;

		config.successTestLog("Time spent for all data is " + duration);
	}

	/**
	 * This scenario test the recording button focusing on input validation and
	 * performance
	 * 
	 * @author Patrick
	 */
	public void testScenario2() {
		long startTime = System.nanoTime();

		solo.assertCurrentActivity("Check on CDSS entry", HomeActivity.class);
		solo.sleep(config.SLEEPDURATION);

		config.successTestLog("Entering Home Page ");

		solo.clickOnView((solo.getView(R.id.btnStartPatientInfo)));
		config.successTestLog("Begin recording");
		solo.sleep(st);

		solo.assertCurrentActivity("In Camera Activity", CameraActivity.class);

		solo.clickOnView((solo.getView(R.id.NextButtonCammera)));
		config.successTestLog("Skipped QR Code");
		solo.sleep(st);

		solo.assertCurrentActivity("In Patient Information Activity", PatientSearchActivity.class);

		solo.enterText((EditText) solo.getView(R.id.edit_fname1), "Patrick");
		config.successTestLog("Enter search first name");
		solo.sleep(st);

		solo.clickOnView(solo.getView(R.id.edit_lname1));

		solo.enterText((EditText) solo.getView(R.id.edit_lname1), "Zhao");
		config.successTestLog("Enter search last name");
		solo.sleep(st);

		solo.clickOnView((solo.getView(R.id.search_button)));
		config.successTestLog("Enter search button");
		solo.sleep(st);

		solo.assertCurrentActivity("Expected PatientInformationActivity", PatientInfoFormActivity.class);

		solo.clickOnView(solo.getView(R.id.date_of_birth_button));
		solo.setDatePicker(0, 2012, 4, 24);
		solo.clickOnText("Set");
		config.successTestLog("Date chosen");
		solo.sleep(st);

		solo.enterText((EditText) solo.getView(R.id.phone), "87001234");
		config.successTestLog("Phone Number entered");
		solo.sleep(st);
		solo.enterText((EditText) solo.getView(R.id.addressEditText), "344 Swanston Melbourne");
		config.successTestLog("Address Entered");
		solo.sleep(st);
		solo.clickOnView((RadioButton) solo.getView(R.id.male));

		solo.clickOnView(solo.getView(R.id.add_button));

		// solo.assertCurrentActivity("Expected TemperatureActivity",
		// PTempPAgeActivity.class);

		// solo.getCurrentActivity();
		// config.successTestLog("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"+
		// solo.getCurrentActivity());
		solo.assertCurrentActivity("Expected TemperatureActivity ", PTempPAgeActivity.class);

		solo.clickOnView(solo.getView(R.id.Picker1));
		// solo.enterText((EditText) solo.getView(R.id.Picker1), "38");

		// solo.sendKey(KeyEvent.KEYCODE_3);
		// solo.sendKey(KeyEvent.KEYCODE_8);

		solo.clickOnView((solo.getView(R.id.btn_temp_age)));
		solo.sleep(st);

		// solo.assertCurrentActivity("Expected DangerSignsActivity",
		// DangerSignsActivity.class);
		// solo.clickOnView((solo.getView(R.id.switchDangerDrink)));
		// solo.sleep(st);

		solo.assertCurrentActivity("Expected DangerSignsActivity", DangerSignsActivity.class);
		solo.clickOnView((solo.getView(R.id.next3)));
		solo.sleep(st);

		// solo.assertCurrentActivity("Expected KeySymptomsActivity",
		// KeySymptomsActivity.class);
		// solo.clickOnView((solo.getView(R.id.switchKeyCough)));
		// solo.sleep(st);

		solo.assertCurrentActivity("Expected KeySymptomsActivity", KeySymptomsActivity.class);
		solo.clickOnView((solo.getView(R.id.next4)));
		solo.sleep(st);

		// Begin the question
		// Question 1
		solo.assertCurrentActivity("Expected QuestionsActivity", QuestionsActivity.class);
		solo.clickOnView((solo.getView(R.id.skip)));
		solo.sleep(st);

		// Question 2
		solo.assertCurrentActivity("Expected QuestionsActivity", QuestionsActivity.class);
		solo.clickOnView((solo.getView(R.id.skip)));
		solo.sleep(st);

		// Question 3
		solo.assertCurrentActivity("Expected QuestionsActivity", QuestionsActivity.class);
		solo.clickOnView((solo.getView(R.id.skip)));
		solo.sleep(st);

		// Question 4
		solo.assertCurrentActivity("Expected QuestionsActivity", QuestionsActivity.class);
		solo.clickOnView((solo.getView(R.id.skip)));
		solo.sleep(st);

		// // Question 5
		// solo.assertCurrentActivity("Expected QuestionsActivity",
		// QuestionsActivity.class);
		// solo.clickOnView((solo.getView(R.id.skip)));
		// solo.sleep(st);
		//
		// // Question 6
		// solo.assertCurrentActivity("Expected QuestionsActivity",
		// QuestionsActivity.class);
		// solo.clickOnView((solo.getView(R.id.skip)));
		// solo.sleep(st);
		//
		// // Question 7
		// solo.assertCurrentActivity("Expected QuestionsActivity",
		// QuestionsActivity.class);
		// solo.clickOnView((solo.getView(R.id.skip)));
		// solo.sleep(st);
		// // Question 8
		// solo.assertCurrentActivity("Expected QuestionsActivity",
		// QuestionsActivity.class);
		// solo.clickOnView((solo.getView(R.id.skip)));
		// solo.sleep(st);
		// // Question 9
		// solo.assertCurrentActivity("Expected QuestionsActivity",
		// QuestionsActivity.class);
		// solo.clickOnView((solo.getView(R.id.skip)));
		// solo.sleep(st);

		solo.assertCurrentActivity("Expected RecommendationsActivity", RecommendationsActivity.class);

		solo.clickOnView((solo.getView(R.id.saveButton)));
		solo.sleep(st);

		long endTime = System.nanoTime();
		long duration = endTime - startTime;

		config.successTestLog("Time spent for all data is " + duration);
	}

	// /**
	// * This scenario is used for CDSS project Release 1 This scenario goes
	// * through all the patient information confirmation and the diagnose
	// process
	// * which can be used as a system test scenario
	// *
	// * @author Patrick
	// *
	// * This scenario skips the QR Code and the smart cable process
	// *
	// */
	// @Test
	// public void testScenario1() {
	// solo.assertCurrentActivity("Check on CDSS entry", HomeActivity.class);
	// config.successTestLog("Entering Home Page");
	// solo.sleep(st);
	//
	// solo.clickOnButton("Begin Assessment");
	// config.successTestLog("Begin assessment");
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("In Patient Information Activity",
	// PatientSearchActivity.class);
	//
	// solo.enterText((EditText) solo.getView(R.id.edit_fname1), "Patrick");
	// config.successTestLog("Enter search first name");
	// solo.sleep(st);
	//
	// solo.clickOnView(solo.getView(R.id.edit_lname1));
	//
	// solo.enterText((EditText) solo.getView(R.id.edit_lname1), "Zhao");
	// config.successTestLog("Enter search last name");
	// solo.sleep(st);
	//
	// solo.clickOnView((solo.getView(R.id.search_button)));
	// config.successTestLog("Enter search button");
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("Expected PatientInformationActivity",
	// PatientInfoFormActivity.class);
	//
	// solo.clickOnView((CheckBox) solo.getView(R.id.regisited));
	// solo.enterText((EditText) solo.getView(R.id.edit_fname), "Ilhamy");
	// config.successTestLog("First Name entered");
	// solo.sleep(st);
	// solo.enterText((EditText) solo.getView(R.id.edit_lname), "The Strong");
	// config.successTestLog("Last Name Entered");
	// solo.sleep(st);
	//
	// solo.enterText((EditText) solo.getView(R.id.birth_date_form), " ");
	// solo.setDatePicker(0, 2008, 4, 24);
	// solo.clickOnText("Set");
	// config.successTestLog("Date chosen");
	// solo.sleep(st);
	//
	// solo.enterText((EditText) solo.getView(R.id.phone), "87001234");
	// config.successTestLog("Phone Number entered");
	// solo.sleep(st);
	// solo.enterText((EditText) solo.getView(R.id.patientAge_form), "32");
	// config.successTestLog("Age Entered");
	// solo.sleep(st);
	// solo.enterText((EditText) solo.getView(R.id.addressEditText),
	// "344 Swanston Melbourne");
	// config.successTestLog("Address Entered");
	// solo.sleep(st);
	// solo.clickOnView((RadioButton) solo.getView(R.id.male));
	//
	// solo.clickOnView(solo.getView(R.id.add_button));
	//
	// solo.assertCurrentActivity("Expected TemperatureActivity",
	// PTempPAgeActivity.class);
	//
	// solo.enterText((EditText) solo.getView(R.id.Picker1), "38");
	// solo.clickOnView((solo.getView(R.id.btn_temp_age)));
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("Expected DangerSignsActivity",
	// DangerSignsActivity.class);
	// solo.clickOnView((solo.getView(R.id.switchDangerDrink)));
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("Expected DangerSignsActivity",
	// DangerSignsActivity.class);
	// solo.clickOnView((solo.getView(R.id.next3)));
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("Expected KeySymptomsActivity",
	// KeySymptomsActivity.class);
	// solo.clickOnView((solo.getView(R.id.switchKeyCough)));
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("Expected KeySymptomsActivity",
	// KeySymptomsActivity.class);
	// solo.clickOnView((solo.getView(R.id.next4)));
	// solo.sleep(st);
	//
	// // Begin the question
	// // Question 1
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.skip)));
	// solo.sleep(st);
	//
	// // Question 2
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.skip)));
	// solo.sleep(st);
	//
	// // Question 3
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.skip)));
	// solo.sleep(st);
	//
	// // Question 4
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.skip)));
	// solo.sleep(st);
	//
	// // Question 5
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.skip)));
	// solo.sleep(st);
	//
	// // Question 6
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.skip)));
	// solo.sleep(st);
	//
	// // Question 7
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.skip)));
	// solo.sleep(st);
	// // Question 8
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.skip)));
	// solo.sleep(st);
	// // Question 9
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.skip)));
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("Expected RecommendationsActivity",
	// RecommendationsActivity.class);
	// solo.clickOnView((solo.getView(R.id.doneButton)));
	// solo.sleep(st);
	//
	// }
	//
	// /**
	// * This scenario is used for CDSS project Release 1 This scenario goes
	// * through all the patient information confirmation and the diagnose
	// process
	// * which can be used as a system test scenario
	// *
	// * @author Patrick
	// *
	// * This scenario skips the QR Code and the smart cable process
	// *
	// */
	// @Test
	// public void testScenario2() {
	// solo.assertCurrentActivity("Check on CDSS entry", HomeActivity.class);
	// config.successTestLog("Entering Home Page");
	// solo.sleep(st);
	//
	// solo.clickOnButton("Begin Assessment");
	// config.successTestLog("Begin assessment");
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("In Patient Information Activity",
	// PatientSearchActivity.class);
	//
	// solo.enterText((EditText) solo.getView(R.id.edit_fname1), "Patrick");
	// config.successTestLog("Enter search first name");
	// solo.sleep(st);
	//
	// solo.clickOnView(solo.getView(R.id.edit_lname1));
	//
	// solo.enterText((EditText) solo.getView(R.id.edit_lname1), "Zhao");
	// config.successTestLog("Enter search last name");
	// solo.sleep(st);
	//
	// solo.clickOnView((solo.getView(R.id.search_button)));
	// config.successTestLog("Enter search button");
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("Expected PatientInformationActivity",
	// PatientInfoFormActivity.class);
	//
	// solo.clickOnView((CheckBox) solo.getView(R.id.regisited));
	// solo.enterText((EditText) solo.getView(R.id.edit_fname), "Ilhamy");
	// config.successTestLog("First Name entered");
	// solo.sleep(st);
	// solo.enterText((EditText) solo.getView(R.id.edit_lname), "The Strong");
	// config.successTestLog("Last Name Entered");
	// solo.sleep(st);
	//
	// solo.enterText((EditText) solo.getView(R.id.birth_date_form), " ");
	// solo.setDatePicker(0, 2008, 4, 24);
	// solo.clickOnText("Set");
	// config.successTestLog("Date chosen");
	// solo.sleep(st);
	//
	// solo.enterText((EditText) solo.getView(R.id.phone), "87001234");
	// config.successTestLog("Phone Number entered");
	// solo.sleep(st);
	// solo.enterText((EditText) solo.getView(R.id.patientAge_form), "32");
	// config.successTestLog("Age Entered");
	// solo.sleep(st);
	// solo.enterText((EditText) solo.getView(R.id.addressEditText),
	// "344 Swanston Melbourne");
	// config.successTestLog("Address Entered");
	// solo.sleep(st);
	// solo.clickOnView((RadioButton) solo.getView(R.id.male));
	//
	// solo.clickOnView(solo.getView(R.id.add_button));
	//
	// solo.assertCurrentActivity("Expected TemperatureActivity",
	// PTempPAgeActivity.class);
	//
	// solo.enterText((EditText) solo.getView(R.id.Picker1), "38");
	// solo.clickOnView((solo.getView(R.id.btn_temp_age)));
	// solo.sleep(st);
	//
	// // Key danger tick all
	// solo.assertCurrentActivity("Expected DangerSignsActivity",
	// DangerSignsActivity.class);
	// solo.clickOnView((solo.getView(R.id.switchDangerDrink)));
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("Expected DangerSignsActivity",
	// DangerSignsActivity.class);
	// solo.clickOnView((solo.getView(R.id.switchDangerDrink)));
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("Expected DangerSignsActivity",
	// DangerSignsActivity.class);
	// solo.clickOnView((solo.getView(R.id.switchDangerVomit)));
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("Expected DangerSignsActivity",
	// DangerSignsActivity.class);
	// solo.clickOnView((solo.getView(R.id.switchDangerConvulsed)));
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("Expected DangerSignsActivity",
	// DangerSignsActivity.class);
	// solo.clickOnView((solo.getView(R.id.switchDangerLethUncon)));
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("Expected DangerSignsActivity",
	// DangerSignsActivity.class);
	// solo.clickOnView((solo.getView(R.id.switchDangerConvulsing)));
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("Expected DangerSignsActivity",
	// DangerSignsActivity.class);
	// solo.clickOnView((solo.getView(R.id.next3)));
	// solo.sleep(st);
	//
	// // Tick all key symptoms
	// solo.assertCurrentActivity("Expected KeySymptomsActivity",
	// KeySymptomsActivity.class);
	// solo.clickOnView((solo.getView(R.id.switchKeyCough)));
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("Expected KeySymptomsActivity",
	// KeySymptomsActivity.class);
	// solo.clickOnView((solo.getView(R.id.switchKeyBreath)));
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("Expected KeySymptomsActivity",
	// KeySymptomsActivity.class);
	// solo.clickOnView((solo.getView(R.id.switchKeyDiarrhoea)));
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("Expected KeySymptomsActivity",
	// KeySymptomsActivity.class);
	// solo.clickOnView((solo.getView(R.id.switchKeyFever)));
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("Expected KeySymptomsActivity",
	// KeySymptomsActivity.class);
	// solo.clickOnView((solo.getView(R.id.switchKeyEar)));
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("Expected KeySymptomsActivity",
	// KeySymptomsActivity.class);
	// solo.clickOnView((solo.getView(R.id.next4)));
	// solo.sleep(st);
	//
	// // Begin the question
	// // Question 1
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.skip)));
	// solo.sleep(st);
	//
	// // Question 2
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.skip)));
	// solo.sleep(st);
	//
	// // Question 3
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.skip)));
	// solo.sleep(st);
	//
	// // Question 4
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.skip)));
	// solo.sleep(st);
	//
	// // Question 5
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.skip)));
	// solo.sleep(st);
	//
	// // Question 6
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.skip)));
	// solo.sleep(st);
	//
	// // Question 7
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.skip)));
	// solo.sleep(st);
	// // Question 8
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.skip)));
	// solo.sleep(st);
	// // Question 9
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.skip)));
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("Expected RecommendationsActivity",
	// RecommendationsActivity.class);
	// solo.clickOnView((solo.getView(R.id.doneButton)));
	// solo.sleep(st);
	//
	// }
	//
	// @Test
	// public void testScenario3() {
	// solo.assertCurrentActivity("Check on CDSS entry", HomeActivity.class);
	// config.successTestLog("Entering Home Page");
	// solo.sleep(st);
	//
	// solo.clickOnButton("Begin Assessment");
	// config.successTestLog("Begin assessment");
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("In Patient Information Activity",
	// PatientSearchActivity.class);
	//
	// solo.enterText((EditText) solo.getView(R.id.edit_fname1), "Patrick");
	// config.successTestLog("Enter search first name");
	// solo.sleep(st);
	//
	// solo.clickOnView(solo.getView(R.id.edit_lname1));
	//
	// solo.enterText((EditText) solo.getView(R.id.edit_lname1), "Zhao");
	// config.successTestLog("Enter search last name");
	// solo.sleep(st);
	//
	// solo.clickOnView((solo.getView(R.id.search_button)));
	// config.successTestLog("Enter search button");
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("Expected PatientInformationActivity",
	// PatientInfoFormActivity.class);
	//
	// solo.clickOnView((CheckBox) solo.getView(R.id.regisited));
	// solo.enterText((EditText) solo.getView(R.id.edit_fname), "Ilhamy");
	// config.successTestLog("First Name entered");
	// solo.sleep(st);
	// solo.enterText((EditText) solo.getView(R.id.edit_lname), "The Strong");
	// config.successTestLog("Last Name Entered");
	// solo.sleep(st);
	//
	// solo.enterText((EditText) solo.getView(R.id.birth_date_form), " ");
	// solo.setDatePicker(0, 2008, 4, 24);
	// solo.clickOnText("Set");
	// config.successTestLog("Date chosen");
	// solo.sleep(st);
	//
	// solo.enterText((EditText) solo.getView(R.id.phone), "87001234");
	// config.successTestLog("Phone Number entered");
	// solo.sleep(st);
	// solo.enterText((EditText) solo.getView(R.id.patientAge_form), "32");
	// config.successTestLog("Age Entered");
	// solo.sleep(st);
	// solo.enterText((EditText) solo.getView(R.id.addressEditText),
	// "344 Swanston Melbourne");
	// config.successTestLog("Address Entered");
	// solo.sleep(st);
	// solo.clickOnView((RadioButton) solo.getView(R.id.male));
	//
	// solo.clickOnView(solo.getView(R.id.add_button));
	//
	// solo.assertCurrentActivity("Expected TemperatureActivity",
	// PTempPAgeActivity.class);
	//
	// solo.enterText((EditText) solo.getView(R.id.Picker1), "38");
	// solo.clickOnView((solo.getView(R.id.btn_temp_age)));
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("Expected DangerSignsActivity",
	// DangerSignsActivity.class);
	// solo.clickOnView((solo.getView(R.id.switchDangerDrink)));
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("Expected DangerSignsActivity",
	// DangerSignsActivity.class);
	// solo.clickOnView((solo.getView(R.id.next3)));
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("Expected KeySymptomsActivity",
	// KeySymptomsActivity.class);
	// solo.clickOnView((solo.getView(R.id.switchKeyCough)));
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("Expected KeySymptomsActivity",
	// KeySymptomsActivity.class);
	// solo.clickOnView((solo.getView(R.id.next4)));
	// solo.sleep(st);
	//
	// // Begin the question
	// // Question 1
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.yes)));
	// solo.sleep(st);
	//
	// // Question 2
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.yes)));
	// solo.sleep(st);
	//
	// // Question 3
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.yes)));
	// solo.sleep(st);
	//
	// // Question 4
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.yes)));
	// solo.sleep(st);
	//
	// // Question 5
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.yes)));
	// solo.sleep(st);
	//
	// // Question 6
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.yes)));
	// solo.sleep(st);
	//
	// // Question 7
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.yes)));
	// solo.sleep(st);
	// // Question 8
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.yes)));
	// solo.sleep(st);
	// // Question 9
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.yes)));
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("Expected RecommendationsActivity",
	// RecommendationsActivity.class);
	// solo.clickOnView((solo.getView(R.id.doneButton)));
	// solo.sleep(st);
	//
	// }
	//
	// @Test
	// public void testScenario4() {
	//
	// solo.assertCurrentActivity("Check on CDSS entry", HomeActivity.class);
	// config.successTestLog("Entering Home Page");
	// solo.sleep(st);
	//
	// solo.clickOnButton("Begin Assessment");
	// config.successTestLog("Begin assessment");
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("In Patient Information Activity",
	// PatientSearchActivity.class);
	//
	// solo.enterText((EditText) solo.getView(R.id.edit_fname1), "Ilhamy");
	// config.successTestLog("Enter search first name");
	// solo.sleep(st);
	//
	// solo.enterText((EditText) solo.getView(R.id.edit_lname1), "The Strong");
	// config.successTestLog("Enter search first name");
	// solo.sleep(st);
	//
	// solo.clickOnView((solo.getView(R.id.search_button)));
	// config.successTestLog("Enter search button");
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("Expected PatientInformationShowActivity",
	// PatientInfoPreviewActivity.class);
	// solo.clickOnView((solo.getView(R.id.confirm_button)));
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("Expected TemperatureActivity",
	// PTempPAgeActivity.class);
	//
	// solo.enterText((EditText) solo.getView(R.id.Picker1), "38");
	// solo.clickOnView((solo.getView(R.id.btn_temp_age)));
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("Expected DangerSignsActivity",
	// DangerSignsActivity.class);
	// solo.clickOnView((solo.getView(R.id.switchDangerDrink)));
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("Expected DangerSignsActivity",
	// DangerSignsActivity.class);
	// solo.clickOnView((solo.getView(R.id.next3)));
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("Expected KeySymptomsActivity",
	// KeySymptomsActivity.class);
	// solo.clickOnView((solo.getView(R.id.switchKeyCough)));
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("Expected KeySymptomsActivity",
	// KeySymptomsActivity.class);
	// solo.clickOnView((solo.getView(R.id.next4)));
	// solo.sleep(st);
	//
	// // Begin the question
	// // Question 1
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.no)));
	// solo.sleep(st);
	//
	// // Question 2
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.no)));
	// solo.sleep(st);
	//
	// // Question 3
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.no)));
	// solo.sleep(st);
	//
	// // Question 4
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.no)));
	// solo.sleep(st);
	//
	// // Question 5
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.no)));
	// solo.sleep(st);
	//
	// // Question 6
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.no)));
	// solo.sleep(st);
	//
	// // Question 7
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.no)));
	// solo.sleep(st);
	// // Question 8
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.no)));
	// solo.sleep(st);
	// // Question 9
	// solo.assertCurrentActivity("Expected QuestionsActivity",
	// QuestionsActivity.class);
	// solo.clickOnView((solo.getView(R.id.no)));
	// solo.sleep(st);
	//
	// solo.assertCurrentActivity("Expected RecommendationsActivity",
	// RecommendationsActivity.class);
	// solo.clickOnView((solo.getView(R.id.doneButton)));
	// solo.sleep(st);
	//
	// }

}
