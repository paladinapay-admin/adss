/**
 * 
 */
package edu.ni.unimelb.cdss.patientInfo;

import android.app.Instrumentation;
import android.content.Context;
import android.test.ActivityInstrumentationTestCase2;
import edu.ni.unimelb.cdss.control.HomeActivity;

import java.util.ArrayList;

/**
 * @author Eman K
 *
 */
public class PatientInfoTest extends ActivityInstrumentationTestCase2<HomeActivity> {
	
	PatientInfo patient;

    public PatientInfoTest() {
        super(HomeActivity.class);
    }


    /**
	 * @throws java.lang.Exception
	 */
	protected static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	protected static void tearDownAfterClass() throws Exception {
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		System.out.println("Setting up ...");
		patient = new PatientInfo();


    }

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();

		System.out.println("Tearing down ...");
	}
	/*
	 * String P_ID, String F_name, String L_name, String phone, 
			String date_of_birth, String age, String sex, String address, String last_update,
			String registered
	 */
	PatientInfo p1=new PatientInfo("1","emma","abc","04498978","1995-01-15","17","Female","Australia melbourne 3002","2013-10-12","0");
	PatientInfo p2=new PatientInfo("12","ilhamy","putra","044465433","1988-01-15","27","Male","Australia melbourne 3002","2013-10-12","0");
	PatientInfo p3=new PatientInfo("123","Chi","Wang","0442323","1987-01-15","25","Male","Australia melbourne 3002","2013-10-12","0");
	PatientInfo p4=new PatientInfo("1234","eman","alatawi","043232444","1988-01-15","25","Female","Australia melbourne 3002","2013-10-12","0");
	PatientInfo p5=new PatientInfo("12345","eman","kh","023246444","1985-01-15","27","Female","Australia melbourne 3002","2013-10-12","0");
	PatientInfo p6=new PatientInfo("123456","Erick","Gaspar","044449994","1988-01-15","25","Male","Australia melbourne 3002","2013-10-12","0");
	PatientInfo p7=new PatientInfo("1234567","Patrick","Zhao","0498437544","1988-01-15","25","Male","Australia melbourne 3002","2013-10-12","0");
	PatientInfo p8=new PatientInfo("12345678","Henry","C","0444444533","1989-01-15","23","Male","Australia melbourne 3002","2013-10-12","0");
	PatientInfo p9=new PatientInfo("123456789","Lacy","hhh","044447774","1989-01-15","23","Female","Australia melbourne 3002","2013-10-12","0");
	PatientInfo p10=new PatientInfo("10","eman","kh","044445353","1985-01-15","27","Female","Australia melbourne 3002","2013-10-12","0");
	PatientInfo p11=new PatientInfo("11","eman","kh","044443333","1985-01-15","27","Female","Australia melbourne 3002","2013-10-12","0");


    // Test 1: getter and setter of patientInfo


    public void testSetterAndGetter() {
        // Test setter functions
        patient.setP_ID("123");
        patient.setF_name("Carlos");
        patient.setL_name("Rep");
        patient.setAddress("USA");
        patient.setAge("22");
        patient.setDate_of_birth("1990-01-23");
        patient.setPhone("+1297655");
        patient.setSex("Male");
        patient.setLatst_update("2013-10-14");
        patient.setRegistered("0");


        assertEquals("123", patient.getP_ID());
        assertEquals("Carlos", patient.getF_name());
        assertEquals("Rep", patient.getL_name());
        assertEquals("+1297655", patient.getPhone());
        assertEquals("1990-01-23", patient.getDate_of_birth());
        assertEquals("22", patient.getAge());
        assertEquals("Male", patient.getSex());
        assertEquals("USA", patient.getAddress());
        assertEquals("0",patient.isRegistered());
    }
    public void testInsertPatient() throws Exception {
        Instrumentation mInstrumentation = getInstrumentation();
        Context context= mInstrumentation.getTargetContext();

        assertTrue(p1.insertPatient(context, p1));
        assertTrue(p2.insertPatient(context, p2));
        assertTrue(p3.insertPatient(context, p3));
        assertTrue(p4.insertPatient(context, p4));
        assertTrue(p5.insertPatient(context, p5));
        assertTrue(p9.insertPatient(context, p9));

    }
    /**
	 * Test method for {@link PatientInfo#insertPatient(android.content.Context, PatientInfo)}.
	 */

    public void testInsertAndRetrievePatient() throws Exception {
        Instrumentation mInstrumentation = getInstrumentation();
        Context context= mInstrumentation.getTargetContext();


        ArrayList<PatientInfo> result;
        result = p9.retrievePatients(context,p9);
        assertFalse(result.isEmpty());
        result = p6.retrievePatients(context,p6);
        assertTrue(result.isEmpty());
        assertEquals(0,result.size());

	}


	/**
	 * Test method for {@link edu.ni.unimelb.cdss.patientInfo.PatientInfo#updatePatient(android.content.Context, edu.ni.unimelb.cdss.patientInfo.PatientInfo)}.
	 */
	public void testUpdateAndRetrivePatient() {
		//fail("Not yet implemented");
	}

	/**
	 * Test method for {@link edu.ni.unimelb.cdss.patientInfo.PatientInfo#retrieveAllPatients(android.content.Context)}.
	 */
	public void testRetrieveAllPatients() {
        Instrumentation mInstrumentation = getInstrumentation();
        Context context= mInstrumentation.getTargetContext();


        ArrayList<PatientInfo> result2;
        result2 = p4.retrieveAllPatients(context);
        assertFalse(result2.isEmpty());


        //fail("Not yet implemented");
	}



}
