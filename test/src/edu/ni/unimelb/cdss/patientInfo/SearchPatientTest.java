/**
 * 
 */
package edu.ni.unimelb.cdss.patientInfo;

import android.app.Instrumentation;
import android.content.Context;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.EditText;
import edu.ni.unimelb.cdss.R;

/**
 * This unit tests will test the "SearchPatientActivity" functions
 * 
 * @author Eman K
 * @version 0.1
 */
public class SearchPatientTest extends ActivityInstrumentationTestCase2<PatientSearchActivity>{
    PatientInfo patient;
    PatientSearchActivity activity;
    public SearchPatientTest() {
        super(PatientSearchActivity.class);
	}

    /**
     * @throws java.lang.Exception
     */
    protected static void setUpBeforeClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    protected static void tearDownAfterClass() throws Exception {
    }

    /* (non-Javadoc)
     * @see junit.framework.TestCase#setUp()
     */
    protected void setUp() throws Exception {
        super.setUp();
        System.out.println("Setting up ...");
        patient = new PatientInfo();
        setActivityInitialTouchMode(false);
        activity = getActivity();
    }




    /* (non-Javadoc)
     * @see junit.framework.TestCase#tearDown()
     */
    protected void tearDown() throws Exception {
        super.tearDown();
        System.out.println("Tearing down ...");
    }

    PatientInfo p1=new PatientInfo("1","emma","abc","04498978","1995-01-15","17","Female","Australia melbourne 3002","2013-10-12","0");
    PatientInfo p2=new PatientInfo("12","ilhamy","putra","044465433","1988-01-15","27","Male","Australia melbourne 3002","2013-10-12","0");
    PatientInfo p3=new PatientInfo("123","Chi","Wang","0442323","1987-01-15","25","Male","Australia melbourne 3002","2013-10-12","0");
    PatientInfo p4=new PatientInfo("1234","eman","alatawi","043232444","1988-01-15","25","Female","Australia melbourne 3002","2013-10-12","0");
    PatientInfo p5=new PatientInfo("12345","eman","kh","023246444","1985-01-15","27","Female","Australia melbourne 3002","2013-10-12","0");
    PatientInfo p6=new PatientInfo("123456","Erick","Gaspar","044449994","1988-01-15","25","Male","Australia melbourne 3002","2013-10-12","0");
    PatientInfo p7=new PatientInfo("1234567","Patrick","Zhao","0498437544","1988-01-15","25","Male","Australia melbourne 3002","2013-10-12","0");
    PatientInfo p8=new PatientInfo("12345678","Henry","C","0444444533","1989-01-15","23","Male","Australia melbourne 3002","2013-10-12","0");
    PatientInfo p9=new PatientInfo("123456789","Lacy","hhh","044447774","1989-01-15","23","Female","Australia melbourne 3002","2013-10-12","0");
    PatientInfo p10=new PatientInfo("10","eman","kh","044445353","1985-01-15","27","Female","Australia melbourne 3002","2013-10-12","0");
    PatientInfo p11=new PatientInfo("11","eman","kh","044443333","1985-01-15","27","Female","Australia melbourne 3002","2013-10-12","0");

    public void testInsertPatient() throws Exception {
        Instrumentation mInstrumentation = getInstrumentation();
        Context context= mInstrumentation.getTargetContext();

        assertTrue(p1.insertPatient(context, p1));
        assertTrue(p2.insertPatient(context, p2));
        assertTrue(p3.insertPatient(context, p3));
        assertTrue(p4.insertPatient(context, p4));
        assertTrue(p5.insertPatient(context, p5));
        assertTrue(p9.insertPatient(context, p9));

    }
    public void testCollectData1() {

        // search for the textView
       final EditText phone = (EditText) activity.findViewById(R.id.phone_search);
       final EditText firstName = (EditText) activity.findViewById(R.id.edit_fname1);
       final EditText lastName = (EditText) activity.findViewById(R.id.edit_lname1);

        // set text
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                phone.setText("006888558");
                firstName.setText("eman");
                lastName.setText("kh");
            }
        });

        getInstrumentation().waitForIdleSync();
        assertEquals("phone incorrect", "006888558", phone.getText().toString());

        assertEquals("fname incorrect", "eman", firstName.getText().toString());
        assertEquals("lname incorrect", "kh", lastName.getText().toString());

    }
    public void testCollectData2() {

        // search for the textView
        final EditText phone = (EditText) activity.findViewById(R.id.phone_search);
        final EditText firstName = (EditText) activity.findViewById(R.id.edit_fname1);
        final EditText lastName = (EditText) activity.findViewById(R.id.edit_lname1);

        // set text
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                phone.setText("");
                firstName.setText("eman");
                lastName.setText("kh");
            }
        });

        getInstrumentation().waitForIdleSync();
        assertEquals("phone incorrect", "", phone.getText().toString());

        assertEquals("fname incorrect", "eman", firstName.getText().toString());
        assertEquals("lname incorrect", "kh", lastName.getText().toString());

    }
    public void testCollectData3() {

        // search for the textView
        final EditText phone = (EditText) activity.findViewById(R.id.phone_search);
        final EditText firstName = (EditText) activity.findViewById(R.id.edit_fname1);
        final EditText lastName = (EditText) activity.findViewById(R.id.edit_lname1);

        // set text
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                phone.setText("006888558");
                firstName.setText("");
                lastName.setText("");
            }
        });

        getInstrumentation().waitForIdleSync();
        assertEquals("phone incorrect", "006888558", phone.getText().toString());

        assertEquals("fname incorrect", "", firstName.getText().toString());
        assertEquals("lname incorrect", "", lastName.getText().toString());

    }
    public void testCollectData4() {

        // search for the textView
        final EditText phone = (EditText) activity.findViewById(R.id.phone_search);
        final EditText firstName = (EditText) activity.findViewById(R.id.edit_fname1);
        final EditText lastName = (EditText) activity.findViewById(R.id.edit_lname1);
        final Button next=(Button)activity.findViewById(R.id.search_button);

        // set text
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                phone.setText("");
                firstName.setText("");
                lastName.setText("kh");
            }
        });

        getInstrumentation().waitForIdleSync();
        assertEquals("phone incorrect", "", phone.getText().toString());

        assertEquals("fname incorrect", "", firstName.getText().toString());
        assertEquals("lname incorrect", "kh", lastName.getText().toString());
        patient.setL_name(lastName.getText().toString());
        patient.setPhone(phone.getText().toString());
        patient.setF_name(firstName.getText().toString());


    }

}
