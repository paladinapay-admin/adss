package edu.ni.unimelb.cdss.patientInfo;

import android.app.Instrumentation;
import android.content.Context;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.EditText;
import android.widget.RadioButton;

import com.jayway.android.robotium.solo.Solo;

import edu.ni.unimelb.cdss.R;
import edu.ni.unimelb.cdss.control.ApplicationController;
import edu.ni.unimelb.cdss.control.HomeActivity;
import edu.ni.unimelb.cdss.diagnosis.DangerSignsActivity;
import edu.ni.unimelb.cdss.diagnosis.QuestionsActivity;
import edu.ni.unimelb.cdss.diagnosis.RecommendationsActivity;

/**
 * @author Erick Gaspar
 * @date 10/23/2013
 * @version 0.3
 * 
 *          Integration test for message passing to and from the patient
 *          information gathering phase. Ensures that the Application Class
 *          retains information before moving forward to the diagnosis phase.
 * 
 *          Tests for Patient Information entry at the end of the diagnosis.
 * 
 * 
 */
public class INT_R2_PatientInfoTest extends ActivityInstrumentationTestCase2<HomeActivity> {

	private Solo solo;
	private final int REPEAT = 1;

	public INT_R2_PatientInfoTest() {
		super(HomeActivity.class);
	}

	@Override
	public void setUp() throws Exception {
		// setUp() is run before a test case is started.
		// This is where the solo object is created.
		solo = new Solo(getInstrumentation(), getActivity());
	}

	@Override
	public void tearDown() throws Exception {
		// tearDown() is run after a test case has finished.
		// finishOpenedActivities() will finish all the activities that have
		// been opened during the test execution.

		solo.finishOpenedActivities();
	}

	/*
	 * USE IF YOU NEED TO ITERATE TESTS
	 * 
	 * public void testRepeater() throws Exception {
	 * 
	 * for (int i = 0; i < REPEAT; i++) { Log.v("REPEAT",
	 * "Test iteration number " + i);
	 * 
	 * 
	 * } }
	 */

	public void testPatientState() throws Exception {

		Instrumentation instrumentation = getInstrumentation();
		Context context = instrumentation.getTargetContext();
		ApplicationController app = (ApplicationController) context.getApplicationContext();

		solo.sleep(5000);
		solo.clickOnView((solo.getView(R.id.btnStartDia)));

		context = instrumentation.getTargetContext();
		app = (ApplicationController) context.getApplicationContext();

		// Temperature and Age Activity
		solo.assertCurrentActivity("Expected PTempPAgeActivity", "PTempPAgeActivity");
		solo.sendKey(Solo.ENTER);

		solo.clearEditText((EditText) solo.getView(R.id.patientAge_ptemp_page));
		solo.enterText((EditText) solo.getView(R.id.patientAge_ptemp_page), "2");

		solo.clickOnButton("Next");

		// ----------------------------- //
		// Test 1: Ensure that Patient Information gets stored in Application
		// Class - DangerSignsActivity
		context = instrumentation.getTargetContext();
		app = (ApplicationController) context.getApplicationContext();

		solo.waitForActivity(DangerSignsActivity.class);

		assertEquals("2", app.getpAge_years());

		assertEquals("37.0", app.getPTemp());

		solo.clickOnView((solo.getView(R.id.next3)));

		// ----------------------------- //
		// Test 3: Ensure that Patient Information gets stored in Application
		// Class - KeySymptomsActivity
		context = instrumentation.getTargetContext();
		app = (ApplicationController) context.getApplicationContext();

		solo.assertCurrentActivity("Expected KeySymptomsActivity", "KeySymptomsActivity");

		assertEquals("2", app.getpAge_years());

		assertEquals("37.0", app.getPTemp());

		solo.clickOnView((solo.getView(R.id.next4)));
		solo.waitForActivity(QuestionsActivity.class);

		// ----------------------------- //
		// Test 4: Ensure that Patient Information gets stored in Application
		// Class - QuestionsActivity
		context = instrumentation.getTargetContext();
		app = (ApplicationController) context.getApplicationContext();

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		assertEquals("2", app.getpAge_years());

		assertEquals("37.0", app.getPTemp());

		solo.sleep(1000);
		solo.clickOnView((solo.getView(R.id.skip)));
		solo.waitForActivity(QuestionsActivity.class);
		solo.sleep(1000);
		solo.clickOnView((solo.getView(R.id.skip)));
		solo.waitForActivity(QuestionsActivity.class);
		solo.sleep(1000);
		solo.clickOnView((solo.getView(R.id.skip)));
		solo.waitForActivity(QuestionsActivity.class);
		solo.sleep(1000);
		solo.clickOnView((solo.getView(R.id.skip)));
		solo.waitForActivity(RecommendationsActivity.class);

		// ----------------------------- //
		// Test 5: Ensure that Patient Information gets stored in Application
		// Class - RecommendationsActivity
		context = instrumentation.getTargetContext();
		app = (ApplicationController) context.getApplicationContext();

		solo.assertCurrentActivity("Expected RecommendationsActivity", "RecommendationsActivity");

		assertEquals("2", app.getpAge_years());

		assertEquals("37.0", app.getPTemp());

		solo.clickOnView((solo.getView(R.id.saveButton)));
		solo.clickOnView((solo.getView(R.id.NextButtonCammera)));

		// Test unsaved ID
		solo.enterText((EditText) solo.getView(R.id.phone_search), "12345678");
		solo.clickOnView((solo.getView(R.id.search_button)));

		// Enter information at PatientInformationActivity
		solo.assertCurrentActivity("Expected PatientInfoFormActivity", "PatientInfoFormActivity");

		solo.enterText((EditText) solo.getView(R.id.edit_fname), "Mister");
		solo.enterText((EditText) solo.getView(R.id.edit_lname), "Robotium");

		solo.clearEditText((EditText) solo.getView(R.id.patientAge_form));
		solo.enterText((EditText) solo.getView(R.id.patientAge_form), "2");
		solo.clearEditText((EditText) solo.getView(R.id.phone));
		solo.enterText((EditText) solo.getView(R.id.phone), "87001234");
		solo.enterText((EditText) solo.getView(R.id.addressEditText), "123 Melbourne");
		solo.clickOnView((RadioButton) solo.getView(R.id.male));

		solo.clickOnView(solo.getView(R.id.add_button));

		// ----------------------------- //
		// Test 6: Return to the beginning and query the DB if it was stored
		solo.assertCurrentActivity("Expected HomeActivity", "HomeActivity");

		solo.sleep(7000);
		solo.clickOnView((solo.getView(R.id.btnStartPatientInfo)));
		solo.clickOnView((solo.getView(R.id.NextButtonCammera)));

		context = instrumentation.getTargetContext();

		// Test saved ID, select first result in list
		solo.enterText((EditText) solo.getView(R.id.phone_search), "87001234");
		solo.clickOnView((solo.getView(R.id.search_button)));

		solo.sleep(3000);
	}
}
