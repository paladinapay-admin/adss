package edu.ni.unimelb.cdss.patientInfo;

import java.util.ArrayList;

import android.app.Instrumentation;
import android.content.Context;
import android.test.ActivityInstrumentationTestCase2;
import android.text.format.Time;
import android.widget.EditText;
import android.widget.RadioButton;

import com.jayway.android.robotium.solo.Solo;

import edu.ni.unimelb.cdss.R;
import edu.ni.unimelb.cdss.control.ApplicationController;
import edu.ni.unimelb.cdss.control.HomeActivity;
import edu.ni.unimelb.cdss.diagnosis.DangerSignsActivity;
import edu.ni.unimelb.cdss.diagnosis.QuestionsActivity;
import edu.ni.unimelb.cdss.diagnosis.RecommendationsActivity;

/**
 * @author Erick Gaspar
 * @date 10/23/2013
 * @version 0.4
 * 
 *          Integration test for message passing to and from the patient
 *          information gathering phase. Ensures that the Application Class
 *          retains information before moving forward to the diagnosis phase.
 * 
 * 
 */
public class INT_R1_PatientInfoTest extends ActivityInstrumentationTestCase2<HomeActivity> {

	private Solo solo;
	private final int REPEAT = 1;

	public INT_R1_PatientInfoTest() {
		super(HomeActivity.class);
	}

	@Override
	public void setUp() throws Exception {
		// setUp() is run before a test case is started.
		// This is where the solo object is created.
		solo = new Solo(getInstrumentation(), getActivity());
	}

	@Override
	public void tearDown() throws Exception {
		// tearDown() is run after a test case has finished.
		// finishOpenedActivities() will finish all the activities that have
		// been opened during the test execution.

		solo.finishOpenedActivities();
	}

	/*
	 * USE IF YOU NEED TO ITERATE TESTS
	 * 
	 * public void testRepeater() throws Exception {
	 * 
	 * for (int i = 0; i < REPEAT; i++) { Log.v("REPEAT",
	 * "Test iteration number " + i);
	 * 
	 * 
	 * } }
	 */

	public void testPatientState() throws Exception {

		solo.sleep(5000);
		solo.clickOnView((solo.getView(R.id.btnStartPatientInfo)));

		ApplicationController app;
		PatientInfo patient;
		Context context;

		Instrumentation instrumentation = getInstrumentation();
		context = instrumentation.getTargetContext();
		app = (ApplicationController) context.getApplicationContext();
		patient = app.getPatient();

		// ----------------------------- //
		// Test 1: Initial Patient state must be empty

		solo.clickOnView((solo.getView(R.id.NextButtonCammera)));
		solo.assertCurrentActivity("Expected PatientSearchActivity", "PatientSearchActivity");

		// other fields used by database were not used

		// Test unsaved ID
		solo.enterText((EditText) solo.getView(R.id.phone_search), "12345678");
		solo.clickOnView((solo.getView(R.id.search_button)));

		// Enter information at PatientInformationActivity

		// // ////solo.clickOnView((CheckBox) solo.getView(R.id.regisited));
		// solo.enterText((EditText) solo.getView(R.id.edit_fname1), "Mister");
		// solo.enterText((EditText) solo.getView(R.id.edit_lname1),
		// "Robotium");
		// solo.clickOnView(solo.getView(R.id.search_button));

		solo.enterText((EditText) solo.getView(R.id.edit_fname), "Mister");
		solo.enterText((EditText) solo.getView(R.id.edit_lname), "Robotium");
		solo.clickOnView(solo.getView(R.id.date_of_birth_button));
		solo.setDatePicker(0, 2012, 7, 24);
		solo.clickOnText("Set");

		solo.clearEditText((EditText) solo.getView(R.id.phone));
		solo.enterText((EditText) solo.getView(R.id.phone), "87001234");
		solo.enterText((EditText) solo.getView(R.id.addressEditText), "123 Melbourne");
		solo.clickOnView((RadioButton) solo.getView(R.id.male));

		solo.clickOnView(solo.getView(R.id.add_button));

		// Temperature Activity
		solo.assertCurrentActivity("Expected PTempPAgeActivity", "PTempPAgeActivity");
		solo.sendKey(Solo.ENTER);
		solo.clickOnButton("Next");

		// ----------------------------- //
		// Test 2: Ensure that Patient Information gets stored in Application
		// Class - DangerSignsActivity

		context = instrumentation.getTargetContext();
		app = (ApplicationController) context.getApplicationContext();
		patient = app.getPatient();

		solo.waitForActivity(DangerSignsActivity.class);

		assertNotNull("patient is null", patient);
		assertEquals("Mister87001234", patient.getP_ID());
		assertEquals("Mister", patient.getF_name());
		assertEquals("Robotium", patient.getL_name());
		assertEquals("87001234", patient.getPhone());
		assertEquals("24 - 8 - 2012", patient.getDate_of_birth());
		assertEquals("Male", patient.getSex());
		assertEquals("123 Melbourne", patient.getAddress());

		Time now = new Time();
		now.setToNow();

		int day = now.monthDay;
		int month = now.month;
		int year = now.year;

		assertEquals(day + "-" + month + "-" + year, patient.getLast_update());
		assertEquals("37.0", app.getPTemp());

		solo.clickOnView((solo.getView(R.id.next3)));

		// ----------------------------- //
		// Test 3: Ensure that Patient Information gets stored in Application
		// Class - KeySymptomsActivity
		context = instrumentation.getTargetContext();
		app = (ApplicationController) context.getApplicationContext();
		patient = app.getPatient();

		solo.assertCurrentActivity("Expected KeySymptomsActivity", "KeySymptomsActivity");

		assertNotNull("patient is null", patient);
		assertEquals("Mister87001234", patient.getP_ID());
		assertEquals("Mister", patient.getF_name());
		assertEquals("Robotium", patient.getL_name());
		assertEquals("87001234", patient.getPhone());
		assertEquals("24 - 8 - 2012", patient.getDate_of_birth());
		assertEquals("Male", patient.getSex());
		assertEquals("123 Melbourne", patient.getAddress());

		day = now.monthDay;
		month = now.month;
		year = now.year;

		assertEquals(day + "-" + month + "-" + year, patient.getLast_update());
		assertEquals("37.0", app.getPTemp());

		solo.clickOnView((solo.getView(R.id.next4)));
		solo.waitForActivity(QuestionsActivity.class);

		// ----------------------------- //
		// Test 4: Ensure that Patient Information gets stored in Application
		// Class - QuestionsActivity
		context = instrumentation.getTargetContext();
		app = (ApplicationController) context.getApplicationContext();
		patient = app.getPatient();

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		assertNotNull("patient is null", patient);
		assertEquals("Mister87001234", patient.getP_ID());
		assertEquals("Mister", patient.getF_name());
		assertEquals("Robotium", patient.getL_name());
		assertEquals("87001234", patient.getPhone());
		assertEquals("24 - 8 - 2012", patient.getDate_of_birth());
		assertEquals("Male", patient.getSex());
		assertEquals("123 Melbourne", patient.getAddress());

		day = now.monthDay;
		month = now.month;
		year = now.year;

		assertEquals(day + "-" + month + "-" + year, patient.getLast_update());
		assertEquals("37.0", app.getPTemp());

		solo.sleep(1000);
		solo.clickOnView((solo.getView(R.id.skip)));
		solo.waitForActivity(QuestionsActivity.class);
		solo.sleep(1000);
		solo.clickOnView((solo.getView(R.id.skip)));
		solo.waitForActivity(QuestionsActivity.class);
		solo.sleep(1000);
		solo.clickOnView((solo.getView(R.id.skip)));
		solo.waitForActivity(QuestionsActivity.class);
		solo.sleep(1000);
		solo.clickOnView((solo.getView(R.id.skip)));
		solo.waitForActivity(RecommendationsActivity.class);

		// ----------------------------- //
		// Test 5: Ensure that Patient Information gets stored in Application
		// Class - RecommendationsActivity
		context = instrumentation.getTargetContext();
		app = (ApplicationController) context.getApplicationContext();
		patient = app.getPatient();

		solo.assertCurrentActivity("Expected RecommendationsActivity", "RecommendationsActivity");

		assertNotNull("patient is null", patient);
		assertEquals("Mister87001234", patient.getP_ID());
		assertEquals("Mister", patient.getF_name());
		assertEquals("Robotium", patient.getL_name());
		assertEquals("87001234", patient.getPhone());
		assertEquals("24 - 8 - 2012", patient.getDate_of_birth());
		assertEquals("Male", patient.getSex());
		assertEquals("123 Melbourne", patient.getAddress());

		day = now.monthDay;
		month = now.month;
		year = now.year;

		assertEquals(day + "-" + month + "-" + year, patient.getLast_update());
		assertEquals("37.0", app.getPTemp());

		solo.clickOnView((solo.getView(R.id.homeButtonReco)));

		// ----------------------------- //
		// Test 6: Return to the beginning and query the DB if it was stored
		solo.assertCurrentActivity("Expected HomeActivity", "HomeActivity");

		solo.sleep(5000);
		solo.clickOnView((solo.getView(R.id.btnStartPatientInfo)));

		context = instrumentation.getTargetContext();
		PatientInfo patientInfo = new PatientInfo();
		ArrayList<PatientInfo> results = new ArrayList<PatientInfo>();

		results = patientInfo.retrievePatients(context, patient);

		assertFalse(results.isEmpty());

		solo.clickOnView((solo.getView(R.id.NextButtonCammera)));
		// Test saved ID, select first result in list
		solo.enterText((EditText) solo.getView(R.id.phone_search), "87001234");
		solo.clickOnView((solo.getView(R.id.search_button)));

		solo.sleep(3000);
	}
}
