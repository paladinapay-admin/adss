package edu.ni.unimelb.cdss.qrcode;

/**
 * @author Chi Wang
 * @version 0.1
 * 
 * Tests bundle information retrieved from the QR Code reading
 * 
 */
import junit.framework.Assert;
import junit.framework.TestCase;

public class QRCodeTest extends TestCase {

	QRCode qRCode;

	public void setUp() throws Exception {
		System.out.println("Setting up ...");
		qRCode = new QRCode();
	}

	public static void main(String[] a) {
		junit.textui.TestRunner.run(QRCodeTest.class);
	}

	public void teanDown() throws Exception {
		System.out.println("Tearing down ...");
	}

	private String dessembleT1 = "CDSSQRCODE6 abc1236 Jackie6 Potter14 (855) 433-310510 09/01/19922 "
			+ "4469 2908 Easy Mews, Madrid, West Virginia, 25399-3369, US, (681) 970-04634 Male";
	private String dessembleT2 = "CDSSQRCODE7 68475828 Dominick6 Knight14 (899) 687-307210 "
			+ "01/03/20042 1274 848 Amber Embers Circuit, Poorman, Montana, "
			+ "59363-5242, US, (406) 061-26114 Male";
	private String dessembleT3 = "CDSSQRCODE7 12766937 Stanley7 Leonard14 (811) "
			+ "559-404410 10/07/19812 5668 5275 Honey Towers, Bee Tree, Wyoming, "
			+ "83048-2514, US, (307) 510-33044 Male";
	private String dessembleT4 = "CDSSQRCODE7 18249394 Ruth9 Singleton14 (844) 828-156310 07/18/19832 1172 9920 Cotton Common, Pole Garden, Wyoming, 82177-8638, US, (307) 470-72434 Male";
	private String dessembleT5 = "CDSSQRCODE7 44958655 David6 Kelley14 (811) 963-068110 07/03/19802 3382 7467 Sunny Barn Plaza, Dead Mans Crossing, Florida, 33889-2455, US, (352) 784-28984 Male";
	private String dessembleT6 = "CDSSQRCODE7 88029755 Edgar6 Murray14 (844) 787-869710 05/13/20012 5568 4655 Dusty Abbey, Carbonear, Montana, 59326-7584, US, (406) 221-12416 Female";
	private String dessembleT7 = "CDSSQRCODE7 23509815 Tanya7 Walters14 (899) 475-519910 10/17/20012 6670 2990 Cinder Farm, Forget, Pennsylvania, 17971-3287, US, (878) 811-45596 Female";
	private String dessembleT8 = "CDSSQRCODE7 38279338 Winifred7 Kennedy14 (899) 172-645610 09/08/19832 5574 4115 Quiet Quail Dell, Bug Tussle, Florida, 33330-3934, US, (954) 800-75206 Female";
	private String dessembleT9 = "CDSSQRCODE7 51489765 Debra7 Stanley14 (844) 295-986210 02/03/20052 1675 7092 Lazy Elk Glen, Coleville, Pennsylvania, 16726-3385, US, (878) 863-52466 Female";
	private String dessembleT10 = "CDSSQRCODE7 11005267 Ignacio4 Lane14 (833) 494-091210 01/21/19992 6674 770 Lost Meadow, Peeled Chestnut, Maryland, 21324-0814, US, (301) 101-62126 Female";

	private String dessembleT11 = "";
	private String dessembleT12 = "7 11005267 Ignacio4 Lane14 (833) 494-091210 01/21/19992 6674 770 Lost Meadow, Peeled Chestnut, Maryland, 21324-0814, US, (301) 101-62126 Female";

	public void testAssemble1() {

		String[] result = qRCode.Dessemble(dessembleT1);

		Assert.assertEquals("abc123", result[0]);
		Assert.assertEquals("Jackie", result[1]);
		Assert.assertEquals("Potter", result[2]);
		Assert.assertEquals("(855) 433-3105", result[3]);
		Assert.assertEquals("09/01/1992", result[4]);
		Assert.assertEquals("44", result[5]);
		Assert.assertEquals(
				"2908 Easy Mews, Madrid, West Virginia, 25399-3369, US, (681) 970-0463",
				result[6]);
		Assert.assertEquals("Male", result[7]);

	}

	public void testAssemble2() {

		String[] result = qRCode.Dessemble(dessembleT2);

		Assert.assertEquals("6847582", result[0]);
		Assert.assertEquals("Dominick", result[1]);
		Assert.assertEquals("Knight", result[2]);
		Assert.assertEquals("(899) 687-3072", result[3]);
		Assert.assertEquals("01/03/2004", result[4]);
		Assert.assertEquals("12", result[5]);
		Assert.assertEquals(
				"848 Amber Embers Circuit, Poorman, Montana, 59363-5242, US, (406) 061-2611",
				result[6]);
		Assert.assertEquals("Male", result[7]);

	}

	public void testAssemble3() {

		String[] result = qRCode.Dessemble(dessembleT3);

		Assert.assertEquals("1276693", result[0]);
		Assert.assertEquals("Stanley", result[1]);
		Assert.assertEquals("Leonard", result[2]);
		Assert.assertEquals("(811) 559-4044", result[3]);
		Assert.assertEquals("10/07/1981", result[4]);
		Assert.assertEquals("56", result[5]);
		Assert.assertEquals(
				"5275 Honey Towers, Bee Tree, Wyoming, 83048-2514, US, (307) 510-3304",
				result[6]);
		Assert.assertEquals("Male", result[7]);

	}

	public void testAssemble4() {

		String[] result = qRCode.Dessemble(dessembleT4);

		Assert.assertEquals("1824939", result[0]);
		Assert.assertEquals("Ruth", result[1]);
		Assert.assertEquals("Singleton", result[2]);
		Assert.assertEquals("(844) 828-1563", result[3]);
		Assert.assertEquals("07/18/1983", result[4]);
		Assert.assertEquals("11", result[5]);
		Assert.assertEquals(
				"9920 Cotton Common, Pole Garden, Wyoming, 82177-8638, US, (307) 470-7243",
				result[6]);
		Assert.assertEquals("Male", result[7]);

	}

	public void testAssemble5() {

		String[] result = qRCode.Dessemble(dessembleT5);

		Assert.assertEquals("4495865", result[0]);
		Assert.assertEquals("David", result[1]);
		Assert.assertEquals("Kelley", result[2]);
		Assert.assertEquals("(811) 963-0681", result[3]);
		Assert.assertEquals("07/03/1980", result[4]);
		Assert.assertEquals("33", result[5]);
		Assert.assertEquals(
				"7467 Sunny Barn Plaza, Dead Mans Crossing, Florida, 33889-2455, US, (352) 784-2898",
				result[6]);
		Assert.assertEquals("Male", result[7]);

	}

	public void testAssemble6() {

		String[] result = qRCode.Dessemble(dessembleT6);

		Assert.assertEquals("8802975", result[0]);
		Assert.assertEquals("Edgar", result[1]);
		Assert.assertEquals("Murray", result[2]);
		Assert.assertEquals("(844) 787-8697", result[3]);
		Assert.assertEquals("05/13/2001", result[4]);
		Assert.assertEquals("55", result[5]);
		Assert.assertEquals(
				"4655 Dusty Abbey, Carbonear, Montana, 59326-7584, US, (406) 221-1241",
				result[6]);
		Assert.assertEquals("Female", result[7]);

	}

	public void testAssemble7() {

		String[] result = qRCode.Dessemble(dessembleT7);

		Assert.assertEquals("2350981", result[0]);
		Assert.assertEquals("Tanya", result[1]);
		Assert.assertEquals("Walters", result[2]);
		Assert.assertEquals("(899) 475-5199", result[3]);
		Assert.assertEquals("10/17/2001", result[4]);
		Assert.assertEquals("66", result[5]);
		Assert.assertEquals(
				"2990 Cinder Farm, Forget, Pennsylvania, 17971-3287, US, (878) 811-4559",
				result[6]);
		Assert.assertEquals("Female", result[7]);

	}

	public void testAssemble8() {

		String[] result = qRCode.Dessemble(dessembleT8);

		Assert.assertEquals("3827933", result[0]);
		Assert.assertEquals("Winifred", result[1]);
		Assert.assertEquals("Kennedy", result[2]);
		Assert.assertEquals("(899) 172-6456", result[3]);
		Assert.assertEquals("09/08/1983", result[4]);
		Assert.assertEquals("55", result[5]);
		Assert.assertEquals(
				"4115 Quiet Quail Dell, Bug Tussle, Florida, 33330-3934, US, (954) 800-7520",
				result[6]);
		Assert.assertEquals("Female", result[7]);

	}

	public void testAssemble9() {

		String[] result = qRCode.Dessemble(dessembleT9);

		Assert.assertEquals("5148976", result[0]);
		Assert.assertEquals("Debra", result[1]);
		Assert.assertEquals("Stanley", result[2]);
		Assert.assertEquals("(844) 295-9862", result[3]);
		Assert.assertEquals("02/03/2005", result[4]);
		Assert.assertEquals("16", result[5]);
		Assert.assertEquals(
				"7092 Lazy Elk Glen, Coleville, Pennsylvania, 16726-3385, US, (878) 863-5246",
				result[6]);
		Assert.assertEquals("Female", result[7]);

	}

	public void testAssemble10() {

		String[] result = qRCode.Dessemble(dessembleT10);

		Assert.assertEquals("1100526", result[0]);
		Assert.assertEquals("Ignacio", result[1]);
		Assert.assertEquals("Lane", result[2]);
		Assert.assertEquals("(833) 494-0912", result[3]);
		Assert.assertEquals("01/21/1999", result[4]);
		Assert.assertEquals("66", result[5]);
		Assert.assertEquals(
				"770 Lost Meadow, Peeled Chestnut, Maryland, 21324-0814, US, (301) 101-6212",
				result[6]);
		Assert.assertEquals("Female", result[7]);

	}

	public void testAssemble11() {

		String[] result = qRCode.Dessemble(dessembleT11);

		Assert.assertEquals("CDSSQRCODEINFORMATIONERROR", result[0]);
		Assert.assertEquals("CDSSQRCODEINFORMATIONERROR", result[1]);
		Assert.assertEquals("CDSSQRCODEINFORMATIONERROR", result[2]);
		Assert.assertEquals("CDSSQRCODEINFORMATIONERROR", result[3]);
		Assert.assertEquals("CDSSQRCODEINFORMATIONERROR", result[4]);
		Assert.assertEquals("CDSSQRCODEINFORMATIONERROR", result[5]);
		Assert.assertEquals("CDSSQRCODEINFORMATIONERROR", result[6]);
		Assert.assertEquals("CDSSQRCODEINFORMATIONERROR", result[7]);

	}

	public void testAssemble12() {

		String[] result = qRCode.Dessemble(dessembleT12);

		Assert.assertEquals("CDSSQRCODEINFORMATIONERROR", result[0]);
		Assert.assertEquals("CDSSQRCODEINFORMATIONERROR", result[1]);
		Assert.assertEquals("CDSSQRCODEINFORMATIONERROR", result[2]);
		Assert.assertEquals("CDSSQRCODEINFORMATIONERROR", result[3]);
		Assert.assertEquals("CDSSQRCODEINFORMATIONERROR", result[4]);
		Assert.assertEquals("CDSSQRCODEINFORMATIONERROR", result[5]);
		Assert.assertEquals("CDSSQRCODEINFORMATIONERROR", result[6]);
		Assert.assertEquals("CDSSQRCODEINFORMATIONERROR", result[7]);

	}

}
