
/* Initial beliefs and rules */


/* INDIVIDUAL KEY FEATURES SPECIFICATION */


// ---- main features ---------------------------
	

feature(diarrhoea,
		kf_diarrhoea_at_least_14_days,
	 	"Has the child had diarrhoea for at least 14 days?",
	 	"Diarrhoea occurs when stools contain more water than normal. Diarrhoea is also called loose or watery stools. Frequent passing of normal stools is not diarrhoea.",
	 	"", 13).	 	


/*FEATURE REQUIRED */
featureRequired(severe_persistent_diarrhoea, kf_diarrhoea_at_least_14_days).
outcomeDone(DIAGNOSIS)
:- .count(featureRequired(DIAGNOSIS,_), M)
 & .count((featureRequired(DIAGNOSIS,FEATURE)
   		  &answer(FEATURE,_)),N)  
 & M = N 	
.


/* INDIVIDUAL OUTCOMES SPECIFICATION */

//  severe_persistent_diarrhoea

// - Two of the following signs:
// - Lethargic or unconscious
// - Sunken eyes
// - Not able to drink or drinking poorly SEVERE
// - Skin pinch goes back very slowly.
// or 
// Two of the following 
// - Restless or irritable
// - Sunken eyes 
// - Drinks eagerly, thirsty 
// - Skin pinch goes back slowly
outcomeSpecification(severe_persistent_diarrhoea, "Severe Persistent Diarrhoea") 
:- (outcomeDehydration(some_dehydration)
 |  outcomeDehydration(severe_dehydration))
 & answer(kf_diarrhoea_at_least_14_days, yes)
 & keysymptom(diarrhoea, yes)
 & outcomeDone(severe_persistent_diarrhoea)
.


//persistent diarrhoea

//diarrhoea
//diarrhoea for 14 days or more
outcomeSpecification(persistent_diarrhoea, "Persistent Diarrhoea") 
:- outcomeDehydration(no_dehydration) 
 & answer(kf_diarrhoea_at_least_14_days, yes)
 & keysymptom(diarrhoea, yes)
 & outcomeDone(severe_persistent_diarrhoea)
.

outcome(
	["Severe Persistent Diarrhoea","HIGH"],
		treatments([treatment(referral,referral_parameters("Hospital",duration(0,0,0,0,0,0),"", true),"", high,""),
					treatment(advice,advice_parameters("Treat dehydration before referral to hospital"),"If child has no other severe classification", high,"")
					])
	):- outcomeSpecification(severe_persistent_diarrhoea,_).
	
outcome(
	["Persistent Diarrhoea","MEDIUM"],
		treatments([treatment(prescribed_supplement,prescribed_supplement_parameters("multivitamins and minerals (including zinc)","" ,duration(0,0,14,0,0,0)),"", medium,"p20_b_diag"),
					treatment(advice,advice_parameters("Advise the mother on feeding a child"),"", medium,""),
					treatment(follow_up,follow_up_parameters(duration(0,0,5,0,0,0)),"", medium,"p16_bl_diag")
					])
	):- outcomeSpecification(persistent_diarrhoea,_).
	
	
/* Initial goals */

!start.


/* Plans */

+!start <-

	// Register key feature data.
	for (feature(AGENT, FEATURE, QUESTION, HELP, IMAGE, PRIORITY)) {
		.send(feature,tell,question(AGENT, FEATURE, QUESTION, HELP, IMAGE, PRIORITY));
	}
	
	for (feature(AGENT, FEATURE, QUESTION, HELP, IMAGE, PRIORITY, DEPENDENCY, ANSWER)) {		
		.send(feature,tell,question(AGENT, FEATURE, QUESTION, HELP, IMAGE, PRIORITY, DEPENDENCY, ANSWER));
	}

.


+!checkOutcome : not response(yes) & keysymptom(diarrhoea, yes)
		<-
		.perceive;
		if(keysymptom(diarrhoea, yes) & .count(response(yes),M) & M = 0){
			if(outcomeSpecification(DIAGNOSIS, _)){
				for(outcome(DIAGNOSE, TREATMENT)){
		 			.send(outcome, tell, outcome(DIAGNOSE, TREATMENT));
				}
				 +response(yes);
				.send(feature, tell, response(diarrhoea, yes));
			}else{
				.send(feature, tell, response(diarrhoea, no));
			}		
		}
.
