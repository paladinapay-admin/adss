

/* Initial goals: */

!initialise.

/* Plan definitions: */

/* - Internal plans: */

// Action:	Initialisation and setup of the agent.
+!initialise <-
	// Tell the DisplayEnvironment that the agents are ready.
	.print("Initialised.");
	agentsIntialised;.


+dangersigns(A) : true  <- 
?dangersigns(A);
?answer(dangersigns,A);
.print(A);
gotokeysymptoms;
.

+ask : true <- 
	 		   removeAsk;
	 		   if (ask) {
					.wait({-ask});
			   }
			   .send(feature,tell,ask_feature);	
.

+!question(PRIORITY, AGENT, FEATURE, QUESTION, HELP, IMAGE, OUTCOME) : true <- 
askGUI(AGENT, FEATURE, QUESTION, HELP, IMAGE);
if(OUTCOME = yes){
	!!alertGUI;
}
.

+!alertGUI : true <-
while(.count(alertRecommendation(_),N) & N = 0){}
for(alertRecommendation(RECOMMENDATION)){
	.abolish(alertRecommendation(RECOMMENDATION));	
	if(not (RECOMMENDATION = no)){
		sendAlertRecommendation(RECOMMENDATION);
	}
}
.

+!returnRecommendation(RECOMMENDATION) :true <-
sendRecommendation(RECOMMENDATION);
.stopMAS
.

+!checkOutcome : true <-
	true;
.