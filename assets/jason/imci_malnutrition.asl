
/* Initial beliefs and rules */


/* INDIVIDUAL KEY FEATURES SPECIFICATION */


// ---- main features ---------------------------

feature(malnutrition,
		kf_very_underweight,
	 	"Is the child very underweight?",
	 	"Calculate the child's age in months. The child should wear light clothing when he is weighed. Ask the mother to help remove any coat, sweater, or shoes. Refer to diagram for estimation.",
		"underweight_help", 16).
	 	
feature(malnutrition,
		kf_visible_severe_wasting,
	 	"Does the child have visible severe wasting?",
	 	"Wasting is a form of severe malnutrition. Look for severe wasting of the muscles of the shoulders, arms, buttocks and legs. Look to see if the outline of the child's ribs is easily seen. Look at the child's hips. Look at the child from the side to see if the fat of the buttocks is missing.",
	 	"severe_wasting_help", 15).
	 		 	
feature(malnutrition,
		kf_oedema_of_both_feet,
	 	"Does the child have oedema of both feet?",
	 	"Use your thumb to press gently for a few seconds on the top side of each foot. The child has oedema if a dent remains in the child's foot when you lift your thumb.",
	 	"oedema_help", 15).
		
	
/*FEATURE REQUIRED */
featureRequired(severe_malnutrition, kf_visible_severe_wasting).
featureRequired(severe_malnutrition, kf_oedema_of_both_feet).
featureRequired(very_low_weight, kf_very_underweight).
outcomeDone(DIAGNOSIS)
:- .count(featureRequired(DIAGNOSIS,_), M)
 & .count((featureRequired(DIAGNOSIS,FEATURE)
   		  &answer(FEATURE,_)),N)  
 & M = N 	
.

/* INDIVIDUAL OUTCOMES SPECIFICATION */

//  severe_malnutrition

// - Visible severe wasting
// - Oedema of both feet
outcomeSpecification(severe_malnutrition, "Severe Malnutrition") 
:- answer(kf_visible_severe_wasting, yes)
 & answer(kf_oedema_of_both_feet, yes)
 & outcomeDone(severe_malnutrition)
.

//very low weight
//very low weight for age
outcomeSpecification(very_low_weight, "Very Low Weight") 
:- answer(kf_very_underweight, yes)
 & not outcomeSpecification(severe_malnutrition, _) 
 & outcomeDone(severe_malnutrition)
 & outcomeDone(very_low_weight)
.

//not very low weight
outcomeSpecification(not_very_low_Weight, "Not Very Low Weight") 
:- not outcomeSpecification(severe_malnutrition, _) 
 & not outcomeSpecification(very_low_weight, _)
 & outcomeDone(severe_malnutrition)
 & outcomeDone(very_low_weight)
.

outcome(
	["Severe Malnutrition","HIGH"],
		treatments([treatment(referral,referral_parameters("Hospital",duration(0,0,0,0,0,0),"", true),"", high,""),
					treatment(advice,advice_parameters("Treat the child to prevent low sugar"),"", high,"p13_diag")
					])
	):- outcomeSpecification(severe_malnutrition,_).

outcome(
	["Very Low Weight","MEDIUM"],
		treatments([treatment(advice,advice_parameters("Counsel the mother on feeding according to the feeding recommendations"),"", medium,"p19p20p21"),
					treatment(advice,advice_parameters("Advise the mother on feeding a child"),"", medium,"p23_br_tabl"),
					treatment(follow_up,follow_up_parameters(duration(0,0,30,0,0,0)),"", medium, "p18_br_diag")
					])
	):- outcomeSpecification(very_low_weight,_).
	
outcome(
	["Not Very Low Weight","LOW"],
		treatments([
			treatment(advice,advice_parameters("Assess the child's feeding and counsel the mother on feeding"),"If child is less than 2 years old", low, ""),
			treatment(follow_up,follow_up_parameters(duration(0,0,5,0,0,0)),"If feeding problems", low, ""),
			treatment(advice,advice_parameters("Advise mother when to return immediately"),"", low, "p23_br_tabl")
		])
	):- outcomeSpecification(not_very_low_Weight,_).
	
	
/* Initial goals */

!start.

/* Plans */

+!start <-
	
	registerSymptom(malnutrition);
	
	// Register key feature data.
	for (feature(AGENT, FEATURE, QUESTION, HELP, IMAGE, PRIORITY)) {
		.send(feature,tell,question(AGENT, FEATURE, QUESTION, HELP, IMAGE, PRIORITY));
	}
.

+!checkOutcome : not response(yes) & keysymptom(malnutrition, yes)
		<-
		.perceive;
		if(keysymptom(malnutrition, yes) & .count(response(yes),M) & M = 0){
			if(outcomeSpecification(DIAGNOSIS, _)){
				for (outcome(DIAGNOSE, TREATMENT)){
		 			.send(outcome, tell, outcome(DIAGNOSE, TREATMENT));
				}
				 +response(yes);
				.send(feature, tell, response(malnutrition, yes));
			}else{
				.send(feature, tell, response(malnutrition, no));
			}		
		}
.
