
/* Initial beliefs and rules */

/* INDIVIDUAL KEY FEATURES SPECIFICATION */

// ---- main features ---------------------------

feature(coughing_or_difficult_breathing,
		kf_breathing_problems_fast_breathing,
	 	"Does the child have fast breathing?",
	 	"Count the number of breaths of a calm or sleeping child. Fast depends on the age. 2-12 months: 50 breaths or more per minute, 1-5 years: 40 breaths or more per minute.",
	 	"", 11).
	 	
feature(coughing_or_difficult_breathing,
		kf_chest_indrawn,
	 	"Does the child have chest indrawing?",
	 	"Look for chest indrawing when the child breathes IN. Look at the lower chest wall (lower ribs). The child has chest indrawing if the lower chest wall goes IN when the child breathes IN.",
	 	"chest_indrawing_help", 10).
	 	
feature(coughing_or_difficult_breathing,
		kf_stridor,
	 	"Does the child have stridor?",
	 	"Stridor is a harsh noise made when the child breathes IN. To look and listen for stridor, look to see when the child breathes IN. Put your ear near the child's mouth because stridor can be difficult to hear. Sometimes you will hear a wet noise if the child's nose is blocked. Clear the nose, and listen again. Be sure to look and listen for stridor when the child is calm. You may hear a wheezing noise when the child breathes OUT. This is not stridor.",
	 	"", 10).

feature(coughing_or_difficult_breathing,
		kf_drinking_problems,
	 	"Does the child have drinking problems?",
	 	"Ask the mother to offer the child some water in a cup or spoon. Watch the child drink. A child is not able to drink if he is not able to take fluid in his mouth and swallow it. A child is drinking poorly if the child is weak and cannot drink without help. A child has the sign drinking eagerly, thirsty if it is clear that the child wants to drink.",
	 	"", 10).	
	 	
feature(coughing_or_difficult_breathing,
		kf_vomits_everything,
	 	"Does the child vomit everything?",
	 	"Ask the mother how often the child vomits. Also ask if each time the child swallows food or fluids, does the child vomit? If you are not sure of the mother's answers, ask her to offer the child a drink. See if the child vomits.",
	 	"", 10).
	 	
feature(coughing_or_difficult_breathing,
		kf_had_convulsions,
	 	"Has the child had convulsions?",
	 	"During a convulsion, the child's arms and legs stiffen because the muscles are contracting. The child may lose consciousness or not be able to respond to spoken directions. Ask the mother if the child has had convulsions during this current illness. Use words the mother understands. For example, the mother may know convulsions as 'fits' or 'spasms'.",
	 	"", 10).
	 	
feature(coughing_or_difficult_breathing,
		kf_lethargic_or_unconscious,
	 	"Is the child lethargic or unconscious?",
	 	"Ask the mother if the child seems unusually sleepy or if she cannot wake the child. Look to see if the child wakens when the mother talks or shakes the child or when you clap your hands. Often the lethargic child does not look at his mother or watch your face when you talk. The child may stare blankly and appear not to notice what is going on around him.",
	 	"", 10).
	 	
feature(coughing_or_difficult_breathing,
		kf_convulsing,
	 	"Is the child convulsing?",
	 	"During a convulsion, the child's arms and legs stiffen because the muscles are contracting. The child may lose consciousness or not be able to respond to spoken directions. Ask the mother if the child has had convulsions during this current illness. Use words the mother understands. For example, the mother may know convulsions as 'fits' or 'spasms'.",
	 	"", 10).


/*FEATURE REQUIRED */
featureRequired(severe_pneumonia_or_very_severe_disease, kf_chest_indrawn).
featureRequired(severe_pneumonia_or_very_severe_disease, kf_stridor).
featureRequired(severe_pneumonia_or_very_severe_disease, dangersigns).
featureRequired(pneumonia, kf_breathing_problems_fast_breathing).
outcomeDone(DIAGNOSIS)
:- .count(featureRequired(DIAGNOSIS,_), M)
 & .count((featureRequired(DIAGNOSIS,FEATURE)
   		  &answer(FEATURE,_)),N)  
 & M = N 	
.

/* INDIVIDUAL OUTCOMES SPECIFICATION */

// severe_pneumonia_or_very_severe_disease
// - Any general danger sign
// OR
// - Chest indrawing or
// OR 
// - Stridor in a calm child
outcomeSpecification(severe_pneumonia_or_very_severe_disease, "Severe Pneumonia or Very Severe Disease") 
:-.count((answer(FEATURE, yes) 
		& .member(FEATURE,
		 	       [kf_chest_indrawn,
 					kf_stridor,
 					dangersigns
 					])),N)
 & N > 0
 & keysymptom(coughing_or_difficult_breathing, yes)
 & outcomeDone(severe_pneumonia_or_very_severe_disease)
.


// PNEUMONIA
//   - Coughing 
// - OR
//   - Difficult Breathing
// - AND
//   - Fast breathing
outcomeSpecification(pneumonia, "Pneumonia") 
:- answer(kf_breathing_problems_fast_breathing, yes) 
 & keysymptom(coughing_or_difficult_breathing, yes)
 //add
 & not outcomeSpecification(severe_pneumonia_or_very_severe_disease,_)
 & outcomeDone(severe_pneumonia_or_very_severe_disease)
 & outcomeDone(pneumonia)
.

// COUGH OR COLD
// - No signs of pneumonia 
// OR
// - No signs of a very severe disease
outcomeSpecification(cough_or_cold, "Cough or Cold") 
:- (not outcomeSpecification(pneumonia,_) 
 & not outcomeSpecification(severe_pneumonia_or_very_severe_disease,_))
 //& not answer(kf_pneumonia, yes)
// & not answer(kf_severe_disease, yes))
 & not answer(kf_chest_indrawn, yes)
 & not answer(kf_breathing_problems_fast_breathing, yes)
 & not answer(kf_stridor, yes) 
 & keysymptom(coughing_or_difficult_breathing, yes)
 & outcomeDone(severe_pneumonia_or_very_severe_disease)
 & outcomeDone(pneumonia)
// & outcomeDone(cough_or_cold)
.

outcome(
	["Severe Pneumonia or Very Severe Disease","HIGH"],
		treatments([
		treatment(referral,referral_parameters("Hospital",duration(0,0,0,0,0,0),"", true),"", high, ""),
		treatment(prescribed_drug,prescribed_drug_parameters("Antibiotic","First dose" ,duration(0,0,0,0,0,0)),"", high,"p8_tr_tabl")
		])
	):- outcomeSpecification(severe_pneumonia_or_very_severe_disease,_).

outcome(
	["Pneumonia","MEDIUM"],
		treatments([treatment(referral,referral_parameters("Hospital",duration(0,0,0,0,0,0),"Assessment for TB or asthma", true),"If coughing for more than 3 weeks or if having recurrent wheezing", medium,""),
					treatment(prescribed_drug,prescribed_drug_parameters("Oral Antibiotic","" ,duration(0,0,3,0,0,0)),"", medium,"p8_tr_tabl"),
					treatment(prescribed_drug,prescribed_drug_parameters("Inhaled bronchodilator","" ,duration(0,0,5,0,0,0)),"If wheezing (even if it disappeared after rapidly acting bronchodilator)", medium,"p9_l_diag"),
					treatment(prescribed_drug,prescribed_drug_parameters("Oral salbutamol","" ,duration(0,0,5,0,0,0)),"In settings where inhaled bronchodilator is not available, this is the second choice", medium,""),
					treatment(advice,advice_parameters("Advise mother when to return immediately"),"", medium,"p23_br_tabl"),
					treatment(advice,advice_parameters("Soothe the throat and relieve the cough with a safe remedy"),"", medium,"p10_bl_diag"),
					treatment(follow_up,follow_up_parameters(duration(0,0,2,0,0,0)),"", medium,"p16_tl_diag")		
					])
	):- outcomeSpecification(pneumonia,_).
	
outcome(
	["Cough or Cold","LOW"],
		treatments([treatment(referral,referral_parameters("",duration(0,0,0,0,0,0),"Assessment for TB or asthma", false),"If coughing for more than 3 weeks or if having recurrent wheezing", low, ""),
					treatment(prescribed_drug,prescribed_drug_parameters("Inhaled bronchodilator","" ,duration(0,0,5,0,0,0)),"If wheezing (even if it disappeared after rapidly acting bronchodilator)", low,"p9_l_diag"),
					treatment(prescribed_drug,prescribed_drug_parameters("Oral salbutamol","" ,duration(0,0,5,0,0,0)),"In settings where inhaled bronchodilator is not available, This is the second choice", low,""),
					treatment(advice,advice_parameters("Advise mother when to return immediately"),"", low, "p23_br_tabl"),
					treatment(advice,advice_parameters("Soothe the throat and relieve the cough with a safe remedy"),"", low,"p10_bl_diag"),
					treatment(follow_up,follow_up_parameters(duration(0,0,5,0,0,0)),"If not improving", low, "")
					])			
	):- outcomeSpecification(cough_or_cold,_).
	
/* Initial goals */

!start.

/* Plans */

+!start <-

	// Register key feature data.
	for (feature(AGENT, FEATURE, QUESTION, HELP, IMAGE, PRIORITY)) {
		.send(feature,tell,question(AGENT, FEATURE, QUESTION, HELP, IMAGE, PRIORITY));
	}
.


+!checkOutcome : not response(yes) & keysymptom(coughing_or_difficult_breathing, yes)
		<-
		.perceive;
		if(keysymptom(coughing_or_difficult_breathing, yes) & .count(response(yes),M) & M = 0){
			if(outcomeSpecification(DIAGNOSIS, _)){
				for (outcome(DIAGNOSE, TREATMENT)){
		 			.send(outcome, tell, outcome(DIAGNOSE, TREATMENT));
				}
				 +response(yes);
				.send(feature, tell, response(coughing_or_difficult_breathing, yes));
			}else{
				.send(feature, tell, response(coughing_or_difficult_breathing, no));
			}		
		}
.
