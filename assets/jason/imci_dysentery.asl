
/* Initial beliefs and rules */


/* INDIVIDUAL KEY FEATURES SPECIFICATION */


// ---- main features ---------------------------
	

feature(dysentery,
		kf_diarrhoea_at_least_14_days,
	 	"Has the child had diarrhoea for at least 14 days?",
	 	"Diarrhoea occurs when stools contain more water than normal. Diarrhoea is also called loose or watery stools. Frequent passing of normal stools is not diarrhoea.",
	 	"", 13).
	 	
feature(dysentery,
		kf_blood_in_stool,
	 	"Is there any blood in child's stool?",
	 	"Diarrhoea with blood in the stool, with or without mucus, is called dysentery.",
	 	"", 14).


/*FEATURE REQUIRED */
featureRequired(dysentery, kf_blood_in_stool).
featureRequired(dysentery, kf_diarrhoea_at_least_14_days).
outcomeDone(DIAGNOSIS)
:- .count(featureRequired(DIAGNOSIS,_), M)
 & .count((featureRequired(DIAGNOSIS,FEATURE)
   		  &answer(FEATURE,_)),N)  
 & M = N 	
.


/* INDIVIDUAL OUTCOMES SPECIFICATION */

//dysentery

//diarrhoea
//diarrhoea for 14 days or more
//blood in stool

outcomeSpecification(dysentery, "Dysentery") 
:- answer(kf_blood_in_stool, yes)
 & answer(kf_diarrhoea_at_least_14_days, yes)
 & keysymptom(dysentery, yes)
 & outcomeDone(dysentery)
.
	
outcome(
	["Dysentery","MEDIUM"],
		treatments([treatment(prescribed_drug,prescribed_drug_parameters("Ciprofloxacin","" ,duration(0,0,3,0,0,0)),"", medium,"p8_bl_tabl"),
					treatment(follow_up,follow_up_parameters(duration(0,0,2,0,0,0)),"", medium, "p16_r_diag")
				  ])
	):- outcomeSpecification(dysentery,_).
	
	
	
/* Initial goals */

!start.


/* Plans */

+!start <-

	// Register key feature data.
	for (feature(AGENT, FEATURE, QUESTION, HELP, IMAGE, PRIORITY)) {
		.send(feature,tell,question(AGENT, FEATURE, QUESTION, HELP, IMAGE, PRIORITY));
	}
	
	for (feature(AGENT, FEATURE, QUESTION, HELP, IMAGE, PRIORITY, DEPENDENCY, ANSWER)) {		
		.send(feature,tell,question(AGENT, FEATURE, QUESTION, HELP, IMAGE, PRIORITY, DEPENDENCY, ANSWER));
	}

.

+!checkOutcome : not response(yes) & keysymptom(dysentery, yes)
		<-
		.perceive;
		if(keysymptom(dysentery, yes) & .count(response(yes),M) & M = 0){
			if(outcomeSpecification(DIAGNOSIS, _)){
				for (outcome(DIAGNOSE, TREATMENT)){
		 			.send(outcome, tell, outcome(DIAGNOSE, TREATMENT));
				}
				 +response(yes);
				.send(feature, tell, response(dysentery, yes));
			}else{
				.send(feature, tell, response(dysentery, no));
			}		
		}
.
