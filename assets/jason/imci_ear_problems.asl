
/* Initial beliefs and rules */

/* INDIVIDUAL KEY FEATURES SPECIFICATION */

// Main Features

feature(ear_problems,
		kf_ear_swelling,
	 	"Is there tender swelling behind the ear?",
	 	"Feel behind both ears. Compare them and decide if there is tender swelling of the mastoid bone. In infants, the swelling may be above the ear. Both tenderness and swelling must be present to classify mastoiditis, a deep infection in the mastoid bone. Do not confuse this swelling of the bone with swollen lymph nodes.",
	 	"", 10).
	 	
feature(ear_problems,
		kf_ear_pain,
	 	"Does the child have ear pain?",
	 	"Ear pain can mean that the child has an ear infection. If the mother is not sure that the child has ear pain, ask if the child has been irritable and rubbing his ear.",
	 	"", 11).

feature(ear_problems,
		kf_ear_discharge,
	 	"Is there ear discharge?",
	 	"If the child has had ear discharge, ask for how long. You will classify and treat the ear problem  depending on how long the ear discharge has been present. Ear discharge reported for 2 weeks or more (with pus seen draining from the ear) is treated as a chronic ear infection. Ear discharge reported for less than 2 weeks (with pus seen draining from the ear) is treated as an acute ear infection.",
	 	"", 11).
	 	
feature(ear_problems,
		kf_ear_discharge_at_least_14_days,
	 	"Have the child had ear discharge for at least 14 days?",
	 	"You will classify and treat the ear problem  depending on how long the ear discharge has been present. Ear discharge reported for 2 weeks or more (with pus seen draining from the ear) is treated as a chronic ear infection.",
	 	"", 11, kf_ear_discharge, yes).

feature(ear_problems,
		kf_ear_discharge_pus,
	 	"Is pus draining from the child's ear?",
	 	"Pus draining from the ear is a sign of infection, even if the child no longer has any pain. Look inside the child's ear to see if pus is draining from the ear.",
	 	"", 11).
	 	
/*FEATURE REQUIRED */
featureRequired(mastoiditis, kf_ear_swelling).
featureRequired(acute_ear_infection, kf_ear_discharge_pus).
featureRequired(acute_ear_infection, kf_ear_pain).
featureRequired(acute_ear_infection, kf_ear_discharge_at_least_14_days)
:- answer(kf_ear_discharge, yes).
outcomeDone(DIAGNOSIS)
:- .count(featureRequired(DIAGNOSIS,_), M)
 & .count((featureRequired(DIAGNOSIS,FEATURE)
   		  &answer(FEATURE,_)),N)  
 & M = N 	
.

/* INDIVIDUAL OUTCOMES SPECIFICATION */

// MASTOIDIITIS
// - Tender swelling behind the ear
outcomeSpecification(mastoiditis, "Mastoiditis") 
:- answer(kf_ear_swelling, yes)
 & keysymptom(ear_problems, yes)
 & outcomeDone(mastoiditis)
.


// ACUTE EAR INFECTION
// - Pus is seen draining from the ear and discharge is reported for less than 14 days
// OR
// - Ear pain
outcomeSpecification(acute_ear_infection, "Acute Ear Infection") 
:-.count((answer(FEATURE, yes) 
		& .member(FEATURE,
		 	       [kf_ear_discharge_pus,
 					kf_ear_pain
 					])),N)
 & N > 0
 &.count(answer(kf_ear_pain, yes),M)
 &.count(answer(kf_ear_discharge_at_least_14_days, no),O)
 & M + O > 0
 //add
 & not outcomeSpecification(mastoiditis, _) 
 & keysymptom(ear_problems, yes)
 & outcomeDone(mastoiditis)
 & outcomeDone(acute_ear_infection)
.


// CHRONIC EAR INFECTION
// - Pus is seen draining from the ear and discharge is reported for greater than 14 days
outcomeSpecification(chronic_ear_infection, "Chronic Ear Infection") 
:- answer(kf_ear_discharge_pus, yes)
 & answer(kf_ear_discharge_at_least_14_days, yes)
 & answer(kf_ear_pain, no)
 //add
 & not outcomeSpecification(mastoiditis, _) 
 & keysymptom(ear_problems, yes)
 & outcomeDone(mastoiditis)
 & outcomeDone(acute_ear_infection)
.


// NO EAR INFECTION
// - No ear pain
// AND
// - No pus seen draining from the ear
outcomeSpecification(no_ear_infection, "No Ear Infection") 
:- answer(kf_ear_discharge_pus, no)
 & answer(kf_ear_pain, no)
 //add
 & not outcomeSpecification(mastoiditis, _) 
 & keysymptom(ear_problems, yes)
 & outcomeDone(mastoiditis)
 & outcomeDone(acute_ear_infection)
.

outcome(
	["Mastoiditis","HIGH"],
		treatments([
			treatment(referral, referral_parameters("Hospital", duration(0,0,0,0,0,0), "", true), "", high, ""),
			treatment(prescribed_drug, prescribed_drug_parameters("Paracetamol", "First dose" , duration(0,0,0,0,0,0)), "", high, ""),
			treatment(prescribed_drug, prescribed_drug_parameters("Antibiotic", "First dose" , duration(0,0,0,0,0,0)), "", high, "p8_tr_tabl")
		])
	):- outcomeSpecification(mastoiditis,_).

outcome(
	["Acute Ear Infection","MEDIUM"],
		treatments([	
			treatment(prescribed_drug, prescribed_drug_parameters("Antibiotic", "", duration(0,0,5,0,0,0)), "", medium,"p8_tr_tabl"),
			treatment(prescribed_drug, prescribed_drug_parameters("Paracetamol", "", duration(0,0,0,0,0,0)), "", medium,""),					
			treatment(advice, advice_parameters("Dry the ear by wicking"), "", medium,"p10_tl_diag"),
			treatment(follow_up, follow_up_parameters(duration(0,0,5,0,0,0)), "", medium,"p18_tl_diag")
		])
	):- outcomeSpecification(acute_ear_infection,_).
	
outcome(
	["Chronic Ear Infection","MEDIUM"],
		treatments([
			treatment(prescribed_drug, prescribed_drug_parameters("Topical quinolone eardrops", "", duration(0,0,14,0,0,0)), "", medium, ""),
			treatment(advice, advice_parameters("Dry the ear by wicking"), "", medium, ""),
			treatment(follow_up, follow_up_parameters(duration(0,0,5,0,0,0)), "", medium, "")
		])
	):- outcomeSpecification(chronic_ear_infection,_).
	
outcome(
	["No Ear Infection","LOW"],
		treatments([])
	):- outcomeSpecification(no_ear_infection,_).
	
	
	
/* Initial goals */

!start.


/* Plans */

+!start <-

	// Register key feature data.
	for (feature(AGENT, FEATURE, QUESTION, HELP, IMAGE, PRIORITY)) {
		.send(feature,tell,question(AGENT, FEATURE, QUESTION, HELP, IMAGE, PRIORITY));
	}
	
	for (feature(AGENT, FEATURE, QUESTION, HELP, IMAGE, PRIORITY, DEPENDENCY, ANSWER)) {		
		.send(feature,tell,question(AGENT, FEATURE, QUESTION, HELP, IMAGE, PRIORITY, DEPENDENCY, ANSWER));
	}
	
.

+!checkOutcome : not response(yes) & keysymptom(ear_problems, yes)
		<-
		.perceive;
		if(keysymptom(ear_problems, yes) & .count(response(yes),M) & M = 0){
			if(outcomeSpecification(DIAGNOSIS, _)){
				for (outcome(DIAGNOSE, TREATMENT)){
		 			.send(outcome, tell, outcome(DIAGNOSE, TREATMENT));
				}
				 +response(yes);
				.send(feature, tell, response(ear_problems, yes));
			}else{
				.send(feature, tell, response(ear_problems, no));
			}		
		}
.
