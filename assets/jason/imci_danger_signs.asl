

/* Initial beliefs and rules */


outcomeSpecification(refer_urgently_to_the_hospital, "Danger Signs presented") 
:-.count((answer(FEATURE, yes) 
		& .member(FEATURE,
		 	       [
				   dangersigns
 					])),M)
 & M > 0
& keysymptom(fever_measles,no)
& keysymptom(fever_malaria, no)
& keysymptom(diarrhoea, no)
& keysymptom(coughing_or_difficult_breathing, no)
& keysymptom(ear_problems, no)
& keysymptom(dehydration, no)
& keysymptom(dysentery, no)
.	

outcome(
	["Danger Signs presented","HIGH"],
		treatments([treatment(referral,referral_parameters("Hospital",duration(0,0,0,0,0,0),"", true),"", high, "")
])
):- outcomeSpecification(refer_urgently_to_the_hospital,_).

/* Initial goals */

!start.

/* Plans */

+!start <-
	// Register key feature data.
	true;
.


+!checkOutcome : not response(yes) & keysymptom(dangersigns, yes)
		<-
		.perceive;
		if(keysymptom(dangersigns, yes) & .count(response(yes),M) & M = 0){
			if(outcomeSpecification(DIAGNOSIS, _)){
				 +response(yes);
				.send(feature, tell, response(dangersigns, yes));
				for (outcome(DIAGNOSE, TREATMENT)){
		 			.send(outcome, tell, outcome(DIAGNOSE, TREATMENT));
				}
			}else{
				.send(feature, tell, response(dangersigns, no));
			}	
		}
.
