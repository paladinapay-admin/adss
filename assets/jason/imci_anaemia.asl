
/* Initial beliefs and rules */

/* INDIVIDUAL KEY FEATURES SPECIFICATION */


// ---- main features ---------------------------
	
feature(anaemia,
		kf_palmar_pallor,
	 	"Does the child have palmar pallor?",
	 	"Pallor is unusual paleness of the skin. It is a sign of anaemia. To see if the child has palmar pallor, look at the skin of the child's palm. Hold the child's palm open by grasping it gently from the side. Do not stretch the fingers backwards. If the skin of the child's palm is pale, the child has some palmar pallor.",
	 	"", 15).

feature(anaemia,
		kf_palmar_pallor_severe,
	 	"Does the child have severe palmar pallor?",
	 	"If the skin of the palm is very pale or so pale that it looks white, the child has severe palmar pallor.",
	 	"", 15, kf_palmar_pallor, yes).	 	

/*FEATURE REQUIRED */
featureRequired(severe_anemia, kf_palmar_pallor_severe).
featureRequired(anemia, kf_palmar_pallor).
outcomeDone(DIAGNOSIS)
:- .count(featureRequired(DIAGNOSIS,_), M)
 & .count((featureRequired(DIAGNOSIS,FEATURE)
   		  &answer(FEATURE,_)),N)  
 & M = N 	
.

/* INDIVIDUAL OUTCOMES SPECIFICATION */

//  Severe Anaemia
// - severe palmar pallor
outcomeSpecification(severe_anemia, "Severe Anaemia") 
:- answer(kf_palmar_pallor_severe, yes) 
& outcomeDone(severe_anemia)
.


// Anaemia
// - some palmar pallor
outcomeSpecification(anemia, "Anaemia") 
:- answer(kf_palmar_pallor, yes)
 & not answer(kf_palmar_pallor_severe, yes) 
 & outcomeDone(severe_anemia)
 & outcomeDone(anemia)
.


// No anaemia
outcomeSpecification(no_anaemia, "No Anaemia") 
:- not answer(kf_palmar_pallor, yes) 
 & outcomeDone(anemia)
.

outcome(
	["Severe Anaemia","HIGH"],
		treatments([treatment(referral,referral_parameters("Hospital",duration(0,0,0,0,0,0),"", true),"", high,"")])
	) :- outcomeSpecification(severe_anemia, _).

outcome(
	["Anaemia","MEDIUM"],
		treatments([treatment(prescribed_supplement,prescribed_supplement_parameters("Iron","" ,duration(0,0,0,0,0,0)),"", medium,"p9_tr_tabl"),
					treatment(prescribed_drug,prescribed_drug_parameters("Oral Antimalarial","" ,duration(0,0,0,0,0,0)),"If high malaria risk", medium,""),
					treatment(prescribed_drug,prescribed_drug_parameters("Mebendazole","" ,duration(0,0,0,0,0,0)),"If child is one year or older and has not had a dose in the previous six months", medium,"p11_r_diag"),
					treatment(advice,advice_parameters("Advise the mother when to return immediately"),"", medium,"p23_br_tabl"),
					treatment(follow_up,follow_up_parameters(duration(0,0,14,0,0,0)),"", medium,"p18_tr_diag")
					])
	) :- outcomeSpecification(anemia, _).
	
outcome(
	["No Anaemia","LOW"],
		treatments([
			treatment(advice,advice_parameters("Assess the child's feeding and counsel the mother on feeding"),"If child is less than 2 years old", low, ""),
			treatment(follow_up,follow_up_parameters(duration(0,0,5,0,0,0)),"If feeding problems", low, "")
		])
	) :- outcomeSpecification(no_anaemia, _).
	
/* Initial goals */

!start.


/* Plans */

+!start <-
	
	registerSymptom(anaemia);
	
	// Register key feature data.
	for (feature(AGENT, FEATURE, QUESTION, HELP, IMAGE, PRIORITY)) {
		.send(feature,tell,question(AGENT, FEATURE, QUESTION, HELP, IMAGE, PRIORITY));
	}
	
	for (feature(AGENT, FEATURE, QUESTION, HELP, IMAGE, PRIORITY, DEPENDENCY, ANSWER)) {		
		.send(feature,tell,question(AGENT, FEATURE, QUESTION, HELP, IMAGE, PRIORITY, DEPENDENCY, ANSWER));
	}
.


+!checkOutcome : not response(yes) & keysymptom(anaemia, yes)
		<-
		.perceive;
		if(keysymptom(anaemia, yes) & .count(response(yes),M) & M = 0){
			if(outcomeSpecification(DIAGNOSIS, _)){
				for (outcome(DIAGNOSE, TREATMENT)){
		 			.send(outcome, tell, outcome(DIAGNOSE, TREATMENT));
				}
				+response(yes);
				.send(feature, tell, response(anaemia, yes));
			}else{
				.send(feature, tell, response(anaemia, no));
			}		
		}
.

