
/* Initial beliefs and rules: */

// concatenatedTreatments(TREATMENTS_LIST, TREATMENTS)
// Parameters:
// - TREATMENTS_LIST{in, [TREATMENTS]}
// - TREATMENTS{out, =}
concatenatedTreatments([], treatments([])).
concatenatedTreatments([treatments(TREATMENT_LIST)|TREATMENTS_LIST], treatments(MERGED_TREATMENT_LIST)) :-
	concatenatedTreatments(TREATMENTS_LIST, treatments(TEMP_TREATMENT_LIST)) &
	.union(TREATMENT_LIST, TEMP_TREATMENT_LIST, MERGED_TREATMENT_LIST).
	// TODO: Account for similar but not exactly equal treatments being merged
	//       into the same one. Example:
	//       Return in 5 days + Return in 2 days -> Return in 2 days. 


!initialise.

/* Plan definitions: */

/* - Internal plans: */

// Action:	Initialisation and setup of the agent.
+!initialise <-
	.print("Initialised.");
	agentsIntialised;.

+!checkOutcome : true <-
	true;
.

+outcome([DIAGNOSIS,PRIORITY], TREATMENT) : not notifyOutcome(DIAGNOSIS,PRIORITY)
	<- +notifyOutcome(DIAGNOSIS,PRIORITY);
	   if(PRIORITY = "HIGH"){
	   		!alert;
	   }
	   else{
	   		.send(user_interaction, tell, alertRecommendation(no));
	   }	   
.

+!alert : not requestOutcome
	<- .print("Alert");
	   .findall(outcome(DIAGNOSIS, TREATMENT),outcome(DIAGNOSIS, TREATMENT),OUTCOME_LIST);
	   .findall(OUTCOME_NAME, .member(outcome(OUTCOME_NAME, _), OUTCOME_LIST), OUTCOME_NAME_LIST);
	   .findall(TREATMENTS, .member(outcome(_, TREATMENTS), OUTCOME_LIST), TREATMENTS_LIST);
	   ?concatenatedTreatments(TREATMENTS_LIST, TREATMENTS);
	   RECOMMENDATION = recommendation(outcome_names(OUTCOME_NAME_LIST), TREATMENTS);
	   .send(user_interaction, tell, alertRecommendation(RECOMMENDATION));
.
	
+requestOutcome : true
	<- .print("Ready");
	   .findall(outcome(DIAGNOSIS, TREATMENT),outcome(DIAGNOSIS, TREATMENT),OUTCOME_LIST);
	   .findall(OUTCOME_NAME, .member(outcome(OUTCOME_NAME, _), OUTCOME_LIST), OUTCOME_NAME_LIST);
	   .findall(TREATMENTS, .member(outcome(_, TREATMENTS), OUTCOME_LIST), TREATMENTS_LIST);
	   ?concatenatedTreatments(TREATMENTS_LIST, TREATMENTS);
	   RECOMMENDATION = recommendation(outcome_names(OUTCOME_NAME_LIST), TREATMENTS);
	   .send(user_interaction, achieve, returnRecommendation(RECOMMENDATION));
.

