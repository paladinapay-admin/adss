
/* Initial beliefs and rules */


/* INDIVIDUAL KEY FEATURES SPECIFICATION */


// ---- main features ---------------------------	
	 	
feature(dehydration,
		kf_slow_skin_pinch,
	 	"Pinch the child's abdomen skin. Does the skin pinch go back slowly?",
	 	"Lay child flat with arms at sides. Locate the area on the child's abdomen halfway between the umbilicus and the side of the abdomen. To do the skin pinch, use your thumb and first finger. Do not use your fingertips because this will cause pain. Place your hand so that when you pinch the skin, the fold of skin will be in a line up and down the child's body and not across the child's body. Firmly pick up all of the layers of skin and the tissue under them. Pinch the skin for one second and then release it. When you release the skin, look to see if the skin pinch goes back: very slowly (longer than 2 seconds), slowly (skin stays up even for a brief instant), or immediately. If the skin stays up for even a brief time after you release it, decide that the skin pinch goes back slowly.",
	 	"pinch_help", 11).
	 	
feature(dehydration,
		kf_very_slow_skin_pinch,
	 	"Pinch the child's abdomen skin. Does the skin pinch go back very slowly?",
	 	"the skin pinch goes back very slowly (longer than 2 seconds)",
	 	"pinch_help", 11, kf_slow_skin_pinch, yes).

feature(dehydration,
		kf_sunken_eyes,
	 	"Does the child have sunken eyes?",
	 	"Decide if you think the eyes are sunken. Then ask the mother if she thinks her child's eyes look unusual. Her opinion helps you confirm that the child's eyes are sunken.",
	 	"", 11).
	 		 	
feature(dehydration,
		kf_lethargic_or_unconscious,
	 	"Is the child lethargic or unconscious?",
	 	"Ask the mother if the child seems unusually sleepy or if she cannot wake the child. Look to see if the child wakens when the mother talks or shakes the child or when you clap your hands. Often the lethargic child does not look at his mother or watch your face when you talk. The child may stare blankly and appear not to notice what is going on around him.",
	 	"", 11).
	 	
feature(dehydration,
		kf_drinking_problems,
	 	"Does the child have drinking problems?",
	 	"Ask the mother to offer the child some water in a cup or spoon. Watch the child drink. A child is not able to drink if he is not able to take fluid in his mouth and swallow it. A child is drinking poorly if the child is weak and cannot drink without help. A child has the sign drinking eagerly, thirsty if it is clear that the child wants to drink.", 
	 	"", 11).
	 	
feature(dehydration,
		kf_restless_and_irritable,
	 	"Is the child restless and irritable?",
	 	"If an infant or child is calm when breastfeeding but again restless and irritable when he stops breastfeeding, he has the sign 'restless and irritable'. Many children are upset just because they are in the clinic. Usually these children can be consoled and calmed. They do not have the sign 'restless and irritable'.",
	 	"", 12).
		
feature(dehydration,
		kf_thirsty,
	 	"Is the child thirsty?",
	 	"Look to see if the child reaches out for the cup or spoon when you offer him water. When the water is taken away, see if the child is unhappy because he wants to drink more. If the child takes a drink only with encouragement and does not want to drink more, he does not have the sign 'drinking eagerly, thirsty'.",
	 	"", 12).


/*FEATURE REQUIRED */
featureRequired(severe_dehydration, kf_lethargic_or_unconscious).
featureRequired(severe_dehydration, kf_drinking_problems).
featureRequired(severe_dehydration, kf_sunken_eyes).
featureRequired(severe_dehydration, kf_very_slow_skin_pinch)
:-answer(kf_slow_skin_pinch,yes).
featureRequired(some_dehydration, kf_sunken_eyes).
featureRequired(some_dehydration, kf_restless_and_irritable).
featureRequired(some_dehydration, kf_thirsty).
featureRequired(some_dehydration, kf_slow_skin_pinch).
outcomeDone(DIAGNOSIS)
:- .count(featureRequired(DIAGNOSIS,_), M)
 & .count((featureRequired(DIAGNOSIS,FEATURE)
   		  &answer(FEATURE,_)),N)  
 & M = N 	
.


/* INDIVIDUAL OUTCOMES SPECIFICATION */

//  severe_dehydration

// - Two of the following signs:
// - Lethargic or unconscious
// - Sunken eyes
// - Not able to drink or drinking poorly SEVERE
// - Skin pinch goes back very slowly.
outcomeSpecification(severe_dehydration, "Severe Dehydration") 
:-.count((answer(FEATURE, yes) 
		& .member(FEATURE,
		 	       [kf_lethargic_or_unconscious,
		 	       	kf_very_slow_skin_pinch,
		 	       	kf_drinking_problems,
		 	       	kf_sunken_eyes])),N)
 & N > 1		
 & keysymptom(dehydration, yes)
 & outcomeDone(severe_dehydration)
.


// Some dehydration

// Two of the following 
// - Restless or irritable
// - Sunken eyes 
// - Drinks eagerly, thirsty 
// - Skin pinch goes back slowly
outcomeSpecification(some_dehydration, "Some Dehydration") 
:-.count((answer(FEATURE, yes) 
		& .member(FEATURE,
		 	       [kf_sunken_eyes,
 					kf_restless_and_irritable,
 					kf_thirsty,
 					kf_slow_skin_pinch])),N)
 & N > 1		
 & not answer(kf_very_slow_skin_pinch, yes)
 //add
 & not outcomeSpecification(severe_dehydration, _) 
 & keysymptom(dehydration, yes)
 & outcomeDone(severe_dehydration)
 & outcomeDone(some_dehydration)
.

// No Dehydration
// Not enough signs to classify as some NO or severe dehydration
outcomeSpecification(no_dehydration, "No Dehydration") 
:- not outcomeSpecification(some_dehydration, _) 
 & not outcomeSpecification(severe_dehydration, _) 
 & keysymptom(dehydration, yes)
 & outcomeDone(severe_dehydration)
 & outcomeDone(some_dehydration)
.

outcome(
	["Severe Dehydration","HIGH"],					
		treatments([treatment(referral,referral_parameters("Hospital",duration(0,0,0,0,0,0),"", true),"If child also has another severe classification other than dehydration", high,""),
					treatment(prescribed_supplement,prescribed_supplement_parameters("Oral Rehydration Salts (ORS)","Given frequently by mother on the way to the hospital", duration(0,0,0,0,0,0)),"If child also has another severe classification", high,""),
					treatment(advice,advice_parameters("Advise the mother to continue breastfeeding"),"If child also has another severe classification", high,""),
					treatment(prescribed_drug,prescribed_drug_parameters("Antibiotic for cholera","" ,duration(0,0,0,0,0,0)),"If child is 2 years or older and there is cholera in your area", high,"p8_br_tabl"),
					treatment(prescribed_supplement,prescribed_supplement_parameters("Fluid for severe dehydration (Plan C)","" ,duration(0,0,0,0,0,0)),"If child also has no other severe classification", high,"p15_diag")
					])	
	):- outcomeSpecification(severe_dehydration,_).
	
outcome(
	["Some Dehydration","MEDIUM"],
		treatments([treatment(referral,referral_parameters("Hospital",duration(0,0,0,0,0,0),"Severe disease classification present", true),"If child also has a severe classification", medium,""),
					treatment(prescribed_supplement,prescribed_supplement_parameters("Oral Rehydration Salts ORS","Given frequently by mother" ,duration(0,0,0,0,0,0)),"On The way to the hospital,If child also has a severe classification", medium,""),
					treatment(advice,advice_parameters("Advise the mother to continue breastfeeding"),"If child also has a severe classification", medium,""),
					treatment(prescribed_supplement,prescribed_supplement_parameters("Fluid, zinc supplements and food for severe dehydration(Plan B)","" ,duration(0,0,0,0,0,0)),"", medium,"p14_r_diag"),	
					treatment(advice,advice_parameters("Advise mother when to return immediately"),"", medium,"p23_br_tabl"),
					treatment(follow_up,follow_up_parameters(duration(0,0,5,0,0,0)),"if not improving", medium,"")
					])
	):- outcomeSpecification(some_dehydration,_).
	
outcome(
	["No Dehydration","LOW"],
		treatments([
					treatment(prescribed_supplement,prescribed_supplement_parameters("Fluid, zinc supplements and food to treat diarrhoea ","" ,duration(0,0,0,0,0,0)),"If child also has no other severe classification", low,"p14_l_diag"),					
					treatment(advice,advice_parameters("Advise mother when to return immediately"),"",low,""),
					treatment(follow_up,follow_up_parameters(duration(0,0,5,0,0,0)),"if not improving", low,"")
					])			
	):- outcomeSpecification(no_dehydration,_).
	
	
	
/* Initial goals */

!start.


/* Plans */

+!start <-

	// Register key feature data.
	for (feature(AGENT, FEATURE, QUESTION, HELP, IMAGE, PRIORITY)) {
		.send(feature,tell,question(AGENT, FEATURE, QUESTION, HELP, IMAGE, PRIORITY));
	}
	
	for (feature(AGENT, FEATURE, QUESTION, HELP, IMAGE, PRIORITY, DEPENDENCY, ANSWER)) {		
		.send(feature,tell,question(AGENT, FEATURE, QUESTION, HELP, IMAGE, PRIORITY, DEPENDENCY, ANSWER));
	}

.


+!checkOutcome : not response(yes) & keysymptom(dehydration, yes)
		<-
		.perceive;
		if(keysymptom(dehydration, yes) & .count(response(yes),M) & M = 0){
			if(outcomeSpecification(DIAGNOSIS, _)){
				
				.send(imci_diarrhoea, tell, outcomeDehydration(DIAGNOSIS));
				for (outcome(DIAGNOSE, TREATMENT)){				
		 			.send(outcome, tell, outcome(DIAGNOSE, TREATMENT));
				}
				+response(yes);
				.send(feature, tell, response(dehydration, yes));			
			}else{
				.send(feature, tell, response(dehydration, no));
			}
		}
.

