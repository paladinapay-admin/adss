My position in this project is the testing manager and developer.

This project was an upgrade of the existing system which was originally developed for Nossal Institute. We developed an application which can run on basic Android system mobile devices. The significance of this project is that in less-developed countries there is a shortage of experienced doctors. By the promotion of this application, the training of health workers could improve in terms of efficiency and effectiveness.

This project won the award for "Best Project in the Department of Computing and Information Systems" upon successful completion and showcase at the University of Melbourne's Endeavor Engineering Expo 2013.